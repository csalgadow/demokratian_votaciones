<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('../inc_web/seguri.php');
include('../inc_web/version.php');
ini_set('memory_limit', '3064M'); //cantidad de memoria
ini_set('max_execution_time', 900); //900 seconds = 15 minutes
ob_start();
$array_datos_res = "";
$array_datos_f = "";
?>

<!DOCTYPE html>
<html lang="es"><head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../modulos/morris.js-0.5.1/morris.css">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>


        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">


                <div class="col-md-12">
                    <!--Comiezo--><h1><?php echo "$nombre_votacion"; ?></h1>
                    <?php echo "$resumen"; ?>

                    <div>


                        <?php
                        // $id_pro = $_GET['id_pro'];


                        $sql = "SELECT ID FROM $tbn2  WHERE id_votacion = '$idvot' ";
                        $result_num = mysqli_query($con, $sql);
                        $numero_votantes = mysqli_num_rows($result_num); // obtenemos el número de filas
                        // Votos en blanco
                        $sql = "select distinct vote_id from $tbn10 WHERE id_votacion = '$idvot' and otros=2";
                        $result = mysqli_query($con, $sql);
                        $blancos = mysqli_num_rows($result); // obtenemos el número de filas
                        // Votos en nulos
                        $sql = "select distinct vote_id from $tbn10 WHERE id_votacion = '$idvot' and otros=1";
                        $result = mysqli_query($con, $sql);
                        $nulos = mysqli_num_rows($result); // obtenemos el número de filas
                        // Votos en urna
                        $sql = "select distinct vote_id from $tbn10 WHERE id_votacion = '$idvot' and especial=1";
                        $result = mysqli_query($con, $sql);
                        $urna = mysqli_num_rows($result); // obtenemos el número de filas


                        if ($row = mysqli_fetch_array($result_num)) {
                            ?>
                            <div class="jumbotron">
                                <p class="lead"><?= _('Numero de votantes'); ?> <?php echo "$numero_votantes" ?></p>
                                <p class="lead"><?= _("Votos en blanco") ?>: <?php echo "$blancos" ?></p>
                                <p class="lead"><?= _("Votos nulos") ?>: <?php echo "$nulos" ?></p>
                                <?php if ($urna != 0) { ?>
                                    <p class="lead"><?= _("Votos introducidos de urna") ?>: <?php echo "$urna" ?></p>
                                <?php } ?>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        /* nuevo */

                        $timeStart = microtime(true);
                        $i = 1;
                        $sql = "SELECT  id_candidato FROM $tbn10 WHERE id_votacion = '$idvot'  ";
                        $result = mysqli_query($con, $sql);
                        if ($row = mysqli_fetch_array($result)) {
                            mysqli_field_seek($result, 0);

                            do { //sacamos todas las filas de la base de datos y creamos un array de dos dimensiones
                                $arr = explode(',', $row[0]);
                                foreach ($arr as $val) {
                                    $arr2 = explode('-', $val);
                                    foreach ($arr2 as $val2) {
                                        //echo "<br/>";
                                    }
                                    $equipo[] = array('candidato' => $arr2[0], 'valor' => $arr2[1]);
                                    //$$arr2[1] = $arr2[0];  //asignamos el valor del voto a la variable con el numero de cada candidato
                                    //echo $arr2[1]."=". $$arr2[1]."<br/>";
                                }
                            } while ($row = mysqli_fetch_array($result));
                        }

                        function qd_sd($array, $campo, $campo2) {
                            $suma = 0;
                            $nuevo = array();
                            foreach ($array as $parte) {
                                $clave[] = $parte[$campo];
                            }
                            $unico = array_unique($clave);
                            foreach ($unico as $un) {
                                foreach ($array as $original) {
                                    if ($un == $original[$campo]) {
                                        $suma = $suma + $original[$campo2];
                                    }
                                }
                                $ele['id'] = $un;
                                $ele['total'] = $suma;
                                array_push($nuevo, $ele);
                                $suma = 0;
                            }
                            return $nuevo;
                        }

                        $chido = qd_sd($equipo, 'candidato', 'valor');


                        foreach ($chido as $row) {
                            foreach ($row as $key => $value) {
                                ${$key}[] = $value;
                            }
                        }
                        array_multisort($total, SORT_DESC, $id, SORT_ASC, $chido);
                        //// fin de ordenar el array
                        ?>


                        <div class="row">
                            <div class="col-md-6" >
                                <h2><?= _("Resultado") ?> </h2>

                                <table class="table table-striped">
                                    <tr>
                                        <th>P</th>
                                        <th><?= _("Sexo") ?></th>
                                        <th><?= _("Candidata") ?></th>
                                        <th><?= _("Puntuación") ?></th>
                                    </tr>
                                    <?php
// sacamos los datos del array
                                    for ($a = 0, $total = count($chido); $a < $total; $a++) {
                                        if ($chido[$a]['id'] == "BLANCO" || $chido[$a]['id'] == "NULO") {
                                            $row[0] = $chido[$a]['id'];
                                            $row[1] = "-";
                                        } else {
                                            $sql = "SELECT nombre_usuario,sexo  FROM $tbn7 WHERE ID=" . $chido[$a]['id'] . " ";
                                            $result = mysqli_query($con, $sql);
                                            $row = mysqli_fetch_row($result);
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $i++ ?></td>
                                            <td><?php echo $row[1]; ?></td>
                                            <td><?php echo $chido[$a]['id'] . " | " . $row[0]; ?></td>
                                            <td><?php echo $chido[$a]['total']; ?></td>

                                        </tr>

                                        <?php
                                        $array_datos_res .= "{label: '$row[0]', value:" . $chido[$a]['total'] . " },";
                                        $array_datos_f .= "{label: '$row[0]', value:" . $chido[$a]['total'] . " },";
                                    }
                                    ?>





                                    <?php ?>
                                </table>
                            </div>

                            <div class="col-md-6" >
                                <div> <h3>Grafica</h3>
                                    <div id="donut_resultado_f"  class="resultadosDonut"></div>
                                    <div id="tabla_resultado_f"  class="resultadosGrafica"></div>

                                </div>
                            </div>
                        </div>



                    </div>


                    <!--fin nuevo bloque-->

                </div>

                <!--Final-->


            </div>



            <div id="footer" class="row">

                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>


        <script src="../js/jquery-1.9.0.min.js"></script>
        <script type="text/javascript">
            jQuery.noConflict();
        </script>

        <script src="../modulos/raphael_2.2.1/raphael.min.js"></script>
        <script src="../modulos/morris.js-0.5.1/morris.min.js"></script>
        <script type="text/javascript">
            var array_colores = new Array();
            array_colores = [
                '#0066CC',
                '#FF8000',
                '#FDF512',
                '#F912FD',
                '#BBD03F',
                '#12DEFD',
                '#9102C6',
                '#39FF08',
                '#0BA462',
                '#990000'
            ];

        </script>

        <script type="text/javascript">
            var array_js = new Array();
            array_f = [
<?php echo "$array_datos_f"; ?>
            ];
        </script>

        <script type="text/javascript">
            new Morris.Bar({
                element: 'tabla_resultado_f',
                data: array_f, //array de los datos
                xkey: 'label',
                ykeys: ['value'],
                labels: ['Votos: '],
                backgroundColor: '#9D9D9D',
                /* barFillColors: [
                 '#39FF08 #555',      // from light gray to dark gray (top to bottom)
                 '#555 #aaa black' // from dark day, through light gray, to black
                 ]*/
                /* */
                barColors:
                        function (row, series, type) {
                            if (type === 'bar') {
                                var blue = Math.ceil(255 * row.y / this.ymax);
                                return 'rgb(43,200,' + blue + ')';
                            } else {
                                return '#000';
                            }
                        }
            });





            /*
             * Play with this code and it'll update in the panel opposite.
             *
             * Why not try some of the options above?
             */
            new Morris.Donut({
                element: 'donut_resultado_f',
                data: array_f, //array de los datos
                backgroundColor: '#9D9D9D',
                labelColor: '#060',
                colors: array_colores
                        /*formatter: function (x) { return x + "%"} // da la funcion en porcentajes y no en absolutos
                         */
            });



        </script>
    </body>
</html>
<?php
$contenido = ob_get_contents();
ob_end_flush();
$archivo = $FileRec . $idvot . ".html";
$crear = fopen($archivo, "w");
$grabar = fwrite($crear, $contenido);
fclose($crear);
?>
<?php
$timeEnd = microtime(true);
$timeElapsed = $timeEnd - $timeStart;
echo "<br/>";

printf("Memory used: %s kB\n", memory_get_peak_usage() / 1024);
printf("Total time: %s s\n", $timeElapsed);
/* fin nuevo */
?>
