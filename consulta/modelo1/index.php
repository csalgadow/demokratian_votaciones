
<div class="row" id="fondo">
    <?php
    $numcolumnas = $aux; //demomento el dato aux solo lleva el dato del numero de columnas, pero en el futuro sera un array de datos,
    // $numcolumnas = 4; 
    $n_letras = 50;
    if ($numcolumnas == 2) {
        $class_columnas = "col-xs-12 col-sm-6 col-md-6";
    } else if ($numcolumnas == 3) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-4";
    } else if ($numcolumnas == 4) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-3";
    } else if ($numcolumnas == 6) {
        $class_columnas = "col-xs-12 col-sm-4 col-md-2"; //		
    } else if ($numcolumnas == 12) {
        $class_columnas = "col-xs-6 col-sm-3 col-md-1";
    }

    if ($total_resultados > 0) {
        $i = 1;
        while ($row = mysqli_fetch_array($result)) {
            $resto = ($i % $numcolumnas);
            if ($resto == 1) { /* si es el primer elemento creamos una nueva fila */
                ?>
                <div class="row" >
                <?php }
                ?>      

                <div class="<?php echo $class_columnas; ?>">
                    <div class="profile-header-container">   
                        <div class="profile-header-img">
                            <?php if ($row['imagen_pequena'] == "") { ?><img class="img-circle" src="../temas/<?php echo "$tema_web"; ?>/imagenes/avatar_sin_imagen.jpg" /><?php } else { ?><img class="img-circle" src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> "  /> <?php } ?>
                            <!-- badge -->
                            <div class="rank-label-container">
                                <span class="label label-default rank-label">
                                    <?php echo $row['nombre_usuario']; ?> </span> <br/>
                                <span class="label label-default rank-label"><a data-toggle="modal"  href="../votacion/perfil.php?idgr=<?php echo $row[0]; ?>" data-target="#ayuda_contacta" title="<?php echo $row['nombre_usuario']; ?>"  ><?= _("más información") ?></a>
                                </span>
                            </div>
                            <p class="description">
                                <?php
                                $txt = strip_tags($row['texto']);
                                $txt = mb_substr($txt, 0, $n_letras, 'UTF-8');
                                echo $txt;
                                ?></p>
                        </div>
                    </div>
                </div> 


                <?php
                if ($resto == 0) {
                    /* cerramos la fila */
                    ?>
                </div>
                <?php
            }
            $i++;
        }

        if ($resto != 0) {
            /* Si en la &uacute;ltima fila sobran columnas, creamos celdas vac&iacute;as */
            for ($j = 0; $j < ($numcolumnas - $resto); $j++) {
                echo "<div class=\"col-md-3\"></div>";
            }
            echo "";
        }
    }
    ?>  
</div>

