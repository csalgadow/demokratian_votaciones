<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
require_once("../inc_web/version.php");
require_once("../basicos_php/lang.php");

include ('../basicos_php/basico.php');
if ($_GET['d'] != "") {
    $d = fn_filtro_numerico($con, $_GET['d']); //variable que nos llega con la votacion
    $activa = "si"; //Si la votacion esta activa o no
    $sql_vot = "SELECT a.id_provincia,a.activa,a.tipo,a.tipo_votante,a.nombre_votacion,a.texto,a.resumen, a.fecha_com,a.fecha_fin, b.presentacion,b.cabecera,b.imagen_cab,b.aux,b.diseno FROM $tbn1 a,$tbn22 b  WHERE (a.ID= b.id_votacion) and a.ID='$d' and a.activa='$activa' and b.activo=0 ";
    $res_votacion = mysqli_query($con, $sql_vot);

    $row_vot = mysqli_fetch_row($res_votacion);
    if (is_null($row_vot)) {
        $no_hay_info = _("No hay datos de esta votación, quizas haya terminado ya o no existe");
    } else {
        $id_provincia = $row_vot[0];
        $activa = $row_vot[1];
        $tipo = $row_vot[2];
        $tipo_votante = $row_vot[3];
        $nombre_votacion = $row_vot[4];
        $texto = $row_vot[5];
        $resumen = $row_vot[6];
        $fecha_com = $row_vot[7];
        $fecha_fin = $row_vot[8];
        $presentacion = $row_vot[9];
        $cabecera = $row_vot[10];
        $imagen_cab = $row_vot[11];
        $aux = $row_vot[12];
        $diseno = $row_vot[13];
        $hay_datos = true;
    }
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?> | <?php echo $nombre_votacion; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">
        <!--  mirar si se puede poner al final-->

        <!-- FIN  mirar si se puede poner al final-->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../modulos/themes-jquery-iu/base/jquery.ui.all.css">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">
        <?php if ($presentacion == 1) { ?>
            <link href="modelo1/estilos.css" rel="stylesheet" type="text/css">
        <?php } else if ($presentacion == 2) { ?>
            <link href="modelo2/estilos.css" rel="stylesheet" type="text/css">
        <?php } else if ($presentacion == 3) { ?>
            <link href="modelo3/estilos.css" rel="stylesheet" type="text/css">
        <?php } else if ($presentacion == 4 or $presentacion == 5 or $presentacion == 6) { ?>
            <link rel="stylesheet" type="text/css" href="modelo4/css/normalize.css" />
            <link rel="stylesheet" type="text/css" href="modelo4/css/estilos.css" />
            <link rel="stylesheet" type="text/css" href="modelo4/css/component.css" />
            <script src="modelo4/js/snap.svg-min.js"></script>
            <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <?php } ?>
        <?php if ($diseno != "") { ?>
            <style type="text/css">
    <?php echo $diseno; ?>
            </style>
        <?php } ?>
    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->


        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <?php if ($cabecera == 0) { ?>
                    <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
                <?php } else if ($cabecera == 1) { ?>

                <?php } else if ($cabecera == 2) { ?>
                    <img src="<?php echo $upload_cat; ?>/<?php echo $imagen_cab; ?>" class="img-responsive" alt="<?php echo $nombre_votacion; ?>">
                <?php } ?>
            </div>

            <!-- END cabecera
            ================================================== -->


            <div class="row">
                <div class="col-lg-12">
                    <?php if ($hay_datos == true) { ?>
                        <!---->

                        <h1><?php echo "$nombre_votacion"; ?></h1>

                        <?php echo "$resumen"; ?>


                        <!-- Contenedor general -->

                        <?php
                        $sql = "SELECT * FROM $tbn7 WHERE id_votacion = '$d'  ORDER BY rand(" . time() . " * " . time() . ")  ";
                        $result = mysqli_query($con, $sql);
                        $total_resultados = mysqli_num_rows($result); // obtenemos el número de filas
                        ?>
                        <?php if ($presentacion == 1) { ?>
                            <?php include ('modelo1/index.php'); ?>
                        <?php } else if ($presentacion == 2) { ?>
                            <?php include ('modelo2/index.php'); ?>
                        <?php } else if ($presentacion == 3) { ?>
                            <?php include ('modelo3/index.php'); ?>
                        <?php } else if ($presentacion == 4) { ?>
                            <?php include ('modelo4/indexa.php'); ?>
                        <?php } else if ($presentacion == 5) { ?>
                            <?php include ('modelo4/indexb.php'); ?>
                        <?php } else if ($presentacion == 6) { ?>
                            <?php include ('modelo4/indexc.php'); ?>
                        <?php } ?>


                    <?php } else { ?>
                        <div class="contenedor">
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><?php echo $no_hay_info; ?></div>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        </div>
                    <?php } ?>
                </div>
                <!---->
            </div>
            <p>&nbsp;</p>

            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/ui/jquery-ui.custom.js"></script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#ayuda_contacta').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });

        </script>

    </body>
</html>
