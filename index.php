<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
$nombre_fichero = 'config/config.inc.php';

//require ("basicos_php/lang.php");

if (!file_exists($nombre_fichero)) {
    header('Location: install/index.php');
    echo "<p>El fichero de configuracion no existe</p><p>si es la primera vez que instala la aplicación debe de ir a la carpeta de instalación (SuSitio.org/install)</p>";
} else {
// Inicializar la sesión.
// Si está usando session_name("algo"), ¡no lo olvide ahora!
//require_once("config/config.inc.php");
    require_once("config/config.inc.php");

    session_name($usuarios_sesion);


// session_name($usuarios_sesion);
// Destruir todas las variables de sesión.
    $_SESSION = array();

// Si se desea destruir la sesión completamente, borre también la cookie de sesión.
// Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name($usuarios_sesion), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
        );
    }

    session_start();
// Finalmente, destruir la sesión.
    session_destroy();

    //// incluimos idioma
    require_once("basicos_php/lang.php");

    $con = @mysqli_connect("$host", "$hostu", "$hostp") or die("no se puede conectar");
    mysqli_set_charset($con, "utf8");
    $db = @mysqli_select_db($con, "$dbn") or die("no se puede acceder a la tabla");
    ?><!DOCTYPE html>
    <html lang="es"><head>
            <?php include("temas/codes/meta.php"); ?>

            <title><?php echo "$nombre_web"; ?></title>

            <meta name="author" content="Carlos Salgado">
            <link rel="icon"  type="image/png"  href="temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">

            <!--[if lt IE 9]>

            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <![endif]-->

            <link rel="stylesheet" href="modulos/themes-jquery-iu/base/jquery.ui.core.css" type="text/css" />
            <link rel="stylesheet" href="modulos/themes-jquery-iu/base/jquery.ui.theme.css" type="text/css" title="ui-theme" />
            <link href="temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
            <link href="temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">
            <!--<link href="temas/<?php echo "$tema_web"; ?>/estilo_login.css" rel="stylesheet">-->

        </head>

        <body>

            <div class="container">

                <!-- cabecera
                ================================================== -->
                <div class="page-header">

                    <img src="temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">

                </div>

                <!-- END cabecera
                ================================================== -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="jumbotron">
                            <h2><?= _("Bienvenido al sistema de votaciones") ?> <?php echo "$nombre_web"; ?></h2>
                            <p><?= _("Si es la primera vez que accedes deberás generar tu clave y usuario usando la dirección de correo electrónico con la que estes registrado en la base de datos.") ?></p>
                            <?php if ($insti == true) { ?>
                                <p><a data-toggle="modal" href="#myModalInsti"  class="btn btn-primary btn-lg"><?= _("Registrarme") ?></a> </p>
                            <?php } else { ?>
                                <p><a data-toggle="modal" href="#myModal"  class="btn btn-primary btn-lg"><?= _("Registrarme") ?></a> </p>

                            <?php } ?>


                        </div>


                    </div>


                    <div class="col-lg-6" id="autenticacion_local">
                        <?php
                        if ($cfg_autenticacion_solo_local == false) {
                            ?>
                            <span><button style="display:inline-block; margin-right:5px"id="flip" style="max-width: 30px; max-height: 18px">
                                    <span class="ui-icon ui-icon-carat-1-s" id="flecha"></span>
                                </button><p style="display:inline-block;"><?= _("Acceso para usuarios locales") ?></p></span>
                        <?php } ?>
                        <div class="well" id="panel_autenticacion_local">

                            <h3 class="form-signin-heading"><?= _("Tienes que identificarte para acceder") ?></h3>


                            <?php
                            include ("inc_web/ms_error.php");
                            if (isset($_GET['error_login'])) {
                                $error = $_GET['error_login'];
                                if (isset($_GET['tx'])) {
                                    $tx = "<br/>" . $_GET['tx'];
                                }
                                ?>
                                <div class="alert alert-danger">
                                    <a class="close" data-dismiss="alert">x</a>

                                    <?= _("Error") ?>: <?php echo $error_login_ms[$error]; ?> <br/>
                                    <a data-toggle="modal" href="#myModal" ><?= _("Si no estas registrado puedes hacerlo") ?></a> <br/>
                                    <a data-toggle="modal" href="#myModal" ><?= _("No recuerdo mi contraseña") ?></a>
                                </div>

                            <?php }
                            ?>
                            <!-- comienza el formulario de acceso -->

                            <form action="votacion/inicio.php" method="post"  class="form-horizontal" role="form" id="login">
                                <div class="form-group">
                                    <label for="user" class="col-sm-4 control-label"><?= _("Usuario") ?> : </label>
                                    <div class="col-sm-6">
                                        <input type="text" id="user"   name="user" class="form-control" placeholder="<?= _("Usuario") ?> " required autofocus/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user" class="col-sm-4 control-label"><?= _("Password") ?> : </label>
                                    <div class="col-sm-6">
                                        <input type="password" id="pass"  name="pass" class="form-control" placeholder="<?= _("Password") ?> " required />
                                    </div>
                                </div>

                                <?php
                                if ($reCaptcha == true) {
                                    ?>
                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-6">
                                            <div class="g-recaptcha" data-sitekey="<?php echo "$reCAPTCHA_site_key"; ?>"></div>
                                        </div>
                                    </div>

                                <?php } ?>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">

                                        <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit"><?= _("Entrar") ?> </button>
                                    </div>
                                </div>

                            </form>
                            <!-- Fin del formulario de acceso -->
                        </div>
                        <?php
                        if ($cfg_autenticacion_solo_local == false) {
                            ?>
                            <div class="well">
                                <form action="votacion/inicio.php" method="post"  class="form-horizontal" role="form" >
                                    <input type=hidden name=federada value=1>
                                    <h3 class="form-signin-heading"><?= _("Autenticación externa") ?></h3>
                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-6">
                                            <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit"><?= _("Entrar") ?> </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <?php
                        }
                        ?>
                        <div class="well">
                            <a data-toggle="modal" href="#myModal" ><?= _("¿Olvidaste tu contraseña?") ?></a> </p>
                        </div>

                        <div class="alert alert-info">
                            <a href="http://demokratian.org/" ><?= _("Que es demokratian") ?></a> </p>
                        </div>
                    </div>

                </div>


                <!--
                ================================= ventana modal
                -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" >x</a>
                                <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                                <h4 class="modal-title"><?= _("Complete para registrarse o recuperar su contraseña") ?></h4>
                            </div>

                            <div class="modal-body">

                                <fieldset>

                                    <form name="contrasenaForm" class="well" id="contrasenaForm" >
                                        <div  id="divcontrasenaForm" >


                                            <!-- <form name="sentMessage" class="well" id="contactForm"  novalidate>-->
                                            <!--<legend>Contact me</legend>-->
                                            <div class="control-group">
                                                <div class="controls">
                                                    <input type="text" class="form-control" placeholder="<?= _("Su nombre") ?>" id="name" name="name" required data-validation-required-message="<?= _("Por favor, ponga su nombre") ?>" />
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <input type="email" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="email" name="email" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                                                </div>
                                            </div>


                                            <?php if ($es_municipal == false) {
                                                ?>
                                                <div class="control-group">
                                                    <span class="help-block"><?= _("Seleccione la provincia donde esta censado") ?>:</span>
                                                    <?php
// listar para meter en una lista del tipo enlace
                                                    $especial = "";
                                                    $activo = 0;
                                                    $lista = "";
                                                    $tbn8 = $extension . "provincia";
                                                    $options = "select DISTINCT ID,provincia from $tbn8 where especial ='$especial' order by id  ";
                                                    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());
                                                    while ($listrows = mysqli_fetch_array($resulta)) {
                                                        $name = $listrows['provincia'];
                                                        $id_pro = $listrows['ID'];
                                                        $lista .= "<option value=\"$id_pro\"> $name</option>";
                                                    }
                                                    ?>


                                                    <div class="form-group">
                                                        <select class="form-control"  name="provincia" id="provincia" >
                                                            <!-- <option value=""> Escoje una provincia</option>-->
                                                            <?php echo "$lista"; ?>
                                                        </select>
                                                    </div>



                                                </div>
                                            <?php } else { ?>
                                                <input type="hidden" name="provincia" id="provincia" value="001"  />
                                            <?php } ?>


                                            <button type="submit" class="btn btn-primary pull-right"><?= _("Enviar") ?></button><br />
                                        </div>
                                    </form>
                                    <div id="success"> </div> <!-- mensajes -->
                                </fieldset>

                            </div>
                        </div>
                    </div>
                </div>
                <!--
                ===========================  fin ventana modal
                -->
                <!--
                ========================== segunda ventana modal
                -->


                <div class="modal fade" id="contacta">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal" >x</a>
                                <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                                <h4 class="modal-title"><?= _("Contactar") ?></h4>
                            </div>

                            <div class="modal-body">
                                <div  id="divcontactaForm" >
                                    <p><?= _("Si quieres contactar, completa este formulario y en breve te contestaremos") ?></p>
                                    <p><?= _("Asegurate de que escribes bien tu direccion de correo") ?> </p>

                                    <form name="formularioContacto" class="well" id="formularioContacto" >
                                        <!--<legend>Contact me</legend>-->
                                        <div class="control-group">
                                            <div class="controls">
                                                <input type="text" class="form-control" placeholder="<?= _("Su nombre") ?>" id="nombre2" name="nombre2" required data-validation-required-message="<?= _("Por favor, ponga su nombre") ?>" />
                                                <p class="help-block"></p>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="controls">
                                                <input type="email2" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="email2" name="email2" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                                            </div>
                                        </div>

                                        <?php if ($es_municipal == false) { ?>
                                            <div class="control-group">
                                                <span class="help-block"><?= _("Soy de la provincia") ?>:</span>


                                                <div class="form-group">
                                                    <select class="form-control"  name="provincia2" id="provincia2" >
                                                        <!-- <option value=""> Escoje una provincia</option>-->
                                                        <?php echo "$lista"; ?>
                                                    </select>
                                                </div>



                                            </div>

                                        <?php } ?>
                                        <div class="control-group">
                                            <div class="controls">
                                                <textarea rows="10" cols="100" class="form-control"
                                                          placeholder="<?= _("Cuentanos el problema") ?>" id="texto" required
                                                          data-validation-required-message="<?= _("Cuentanos el problemaa") ?>" minlength="5"
                                                          data-validation-minlength-message="<?= _("Min 5 characteresa") ?>"
                                                          maxlength="999" style="resize:none"></textarea>
                                            </div>
                                        </div>


                                        <button type="submit" class="btn btn-primary pull-right"><?= _("Enviar") ?></button><br />
                                    </form>
                                </div>
                                <div id="success2"> </div> <!-- mensajes -->

                            </div>



                        </div>
                    </div>
                </div>
                <!--
                ===========================  fin segunda ventana modal
                -->

                <?php if ($insti == true) { ?>
                    <!--
        ================================= ventana modal para cuando se usa correo institucional y hay circunscripcion multiple, el registro inicial
                    -->
                    <div class="modal fade" id="myModalInsti">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal" >x</a>
                                    <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                                    <h4 class="modal-title"><?= _("Complete para registrarse") ?> </h4>
                                </div>

                                <div class="modal-body">

                                    <fieldset>

                                        <form name="contrasenaFormInsti" class="well" id="contrasenaFormInsti" >
                                            <div  id="divcontrasenaFormInsti" >

                                                <!--<legend>Contact me</legend>-->
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <input type="text" class="form-control"
                                                               placeholder="<?= _("Su nombre") ?>" id="nameInsti" name="nameInsti" required data-validation-required-message="<?= _("Por favor, ponga su nombrea") ?>" />
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <input type="email" class="form-control" placeholder="<?= _("Su correo electronico") ?>" id="emailInsti" name="emailInsti" required  data-validation-required-message="<?= _("Por favor, ponga su correo electronico") ?>" />
                                                    </div>
                                                </div>


                                                <?php if ($es_municipal == false) {
                                                    ?>
                                                    <div class="control-group">
                                                        <span class="help-block"><?= _("Seleccione la provincia donde esta censado") ?>:</span>
                                                        <?php
// listar para meter en una lista del tipo enlace
                                                        $especial = "";
                                                        $activo = 0;
                                                        $lista = "";
                                                        $tbn8 = $extension . "provincia";
                                                        $options = "select DISTINCT ID,provincia from $tbn8 where especial ='$especial' order by id  ";
                                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());
                                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                                            $name = $listrows['provincia'];
                                                            $id_pro = $listrows['ID'];
                                                            $lista .= "<option value=\"$id_pro\"> $name</option>";
                                                        }
                                                        ?>


                                                        <div class="form-group">
                                                            <select class="form-control"  name="provinciaInsti" id="provinciaInsti" >

                                                                <?php echo "$lista"; ?>
                                                            </select>
                                                        </div>
                                                        <div class="control-group">
                                                            <span class="help-block">
                                                                <?= _("selecciona municipio") ?> </span>
                                                            <select name="municipioInsti" id="municipioInsti" class="form-control" > </select>

                                                        </div>

                                                    </div>
                                                    <p></p>
                                                <?php } ?>



                                                <button type="submit" class="btn btn-primary pull-right"><?= _("Enviar") ?></button><br />
                                            </div>
                                        </form>
                                        <div id="successInsti"> </div> <!-- mensajes -->
                                    </fieldset>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    ===========================  fin ventana modal
                    -->
                <?php } ?>



                <div id="footer" class="row">
                    <?php include("temas/$tema_web/pie_com.php"); ?>
                </div>




            </div>

                                                                                                                     <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
            <script src="js/jquery-1.9.0.min.js"></script>
            <!--<script src="js/jquery.validate.js"></script>-->
            <script src="modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
            <script src="js/jqBootstrapValidation.js"></script>
            <?php if ($insti == true) { ?>
                <script src="js/recupera_contrasena_insti.js"></script>
                <?php if ($es_municipal == false) { ?>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#provinciaInsti').change(function () {

                                var id_provincia = $('#provinciaInsti').val();
                                $('#municipioInsti').load('basicos_php/genera_select.php?id_provincia=' + id_provincia);
                                $("#municipioInsti").html(data);
                            });
                        });
                    </script>
                <?php } ?>


            <?php } ?>
            <script src="js/recupera_contrasena.js"></script>

            <script src="js/contact_me.js"></script>
            <script type="text/javascript">

                        aut_local_plegado = <?php
        if ($cfg_autenticacion_solo_local == false) {
            echo "true";
        } else {
            echo "false";
        }
        ?>;

                        autenticacion_solo_local = <?php
        if ($cfg_autenticacion_solo_local == false) {
            echo "false";
        } else {
            echo "true";
        }
        ?>;

                        if (aut_local_plegado) {
                            $("#panel_autenticacion_local").slideUp();
                        } else {
                            $("#panel_autenticacion_local").slideDown();
                        }

                        if (!autenticacion_solo_local) {
                            $("#flip").click(function () {
                                $("#panel_autenticacion_local").slideToggle();
                                // $("#flecha").toggleClass("ui-icon ui-icon-carat-1-s", "ui-icon-carat-1-n ui-icon");
                            });
                        }
            </script>

        <?php } ?>

        <?php
        if ($reCaptcha == true) {
            ?>
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <?php
        }
        ?>

    </body>
</html>
