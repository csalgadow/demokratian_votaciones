<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="es"><head>
        <?php include("../temas/codes/meta.php"); ?>
        <title>Sistema de instalación de DEMOKRATIAN</title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/demokratian/imagenes/icono.png">




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/demokratian/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
        <link href="../temas/demokratian/estilo.css" rel="stylesheet">
        <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
    </head>

    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="cabecera_instalacion.png" class="img-responsive" alt="Logo DEMOKRATIAN">

            </div>

            <!-- END cabecera
            ================================================== -->
            <div id="success"> </div> <!-- mensajes -->

            <div class="row" id="1_fase1">
                <div class="col-lg-6">
                    <div class="well">
                        <h2>Bienvenido al instalador del sistema de votaciones DEMOKRATIAN</h2>
                        <p> <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div></p>

                    </div>
                </div>

                <div class="col-lg-6">
                    <div >
                        <p><div class="alert alert-info">Para realizar la instalación necesita conocer los datos de la base de datos de su servidor asi como los datos de configuración del servidor de correo.<br>
                            <p>Si usa una base de datos compartida con otras aplicaciones, es  muy recomendable hacer una copia de seguridad de la misma antes de proceder a  la instalación. Durante la misma se pueden producir errores inesperados y  perder datos. Demokratian no se responsabiliza de posibles bugs o errores que  puedan generar perdida de datos.  </p>
                        </div></p>
                        <p>Vamos a utilizar esta información para configurar el archivo  config.inc.php que esta en la carpeta config. Si por cualquier motivo la creación automática no funcionara, o posteriormente quiere hacer una modificación que no pueda realizar mediante el panel de administración de la aplicación, siempre podrá modificar ese archivo a mano.
                        </p>
                        <p class="pull-right"> <a href="http://demokratian.org/" >Que es demokratian</a>  </p>
                    </div>
                </div>
            </div>
            <?php
            $file = "../config/config.inc.php";
            if (file_exists($file)) {
                ?>

                <div class="row" id="2_fase1">
                    <div class="col-lg-12">
                        <div class="well">
                            <div class="alert alert-danger">  Ya existe un archivo de configuración!!, no se puede continuar con el proceso, proceda manualmente o eliminelo</div>
                        </div>
                    </div>
                </div> 
                <?php
            } else {
                ?>


                <div class="row" id="2_fase1">
                    <div class="col-lg-12">
                        <div class="well">
                            <?php
                            //miramos que este instalada la extension gettext
                            if (false === function_exists('gettext')) {
                                echo "<div class=\"alert alert-danger\" role=\"alert\">No tiene la libreria gettext instalada. Si no habilita esta libreria no podra continuar </div>";
                                exit();
                            } else {
                                ?>

                                <form action="prueba.php" method="post"  class="form-horizontal" role="form" name="FormBBDD" id="FormBBDD" >
                                    <h3 class="form-signin-heading">Primer paso</h3>
                                    <h4 class="form-signin-heading">Datos de configuracion de la base de datos</h4>


                                    <div id="success2"> </div>
                                    <div class="form-group">
                                        <label for="dbname" class="col-sm-4 control-label">Acepto las condiciones de la licencia</label>
                                        <div class="col-sm-4">
                                            <input type="checkbox" required/><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">DEMOKRATIAN</span> por <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Carlos Salgado Werner</span> is licensed under a <a href="license-gpl-3.0-standalone.html" target="_blank" rel="license">GPL 3.0 </a>.<img src="gplv3-127x51.png" width="75" height="30" alt="GPL">
                                            <br />
                                            Mas informacion sobre <a xmlns:dct="http://purl.org/dc/terms/" href="http://demokratian.org" rel="dct:source">http://demokratian.org</a>.<br />
                                        </div>
                                        <div class="col-sm-4">

                                            <br/> La aplicación contiene distintos módulos de programas de terceros que pueden tener distintas licencias, compruébelas antes de su uso.</div>
                                    </div>

                                    <div class="form-group">
                                        <label for="dbname" class="col-sm-4 control-label">Nombre de la base de datos</label>
                                        <div class="col-sm-4">
                                            <input name="dbname" id="dbname" type="text" class="form-control" size="25"  placeholder="demokratian" required  autofocus /></td>
                                        </div>
                                        <div class="col-sm-4">
                                            El nombre de la base de datos donde se ejecutara DEMOKRATIAN.
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="username" class="col-sm-4 control-label">Nombre de Usuario</label>
                                        <div class="col-sm-4">
                                            <input name="username" id="username" type="text" class="form-control" size="25" placeholder="nombre_de_usuario" required  autofocus />
                                        </div>
                                        <div class="col-sm-4">
                                            Tu usuario de MySQL
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="pass" class="col-sm-4 control-label">Contraseña</label>
                                        <div class="col-sm-4">
                                            <input name="pass" id="pass" type="text" class="form-control" size="25" placeholder="contraseña"  autofocus />
                                        </div>
                                        <div class="col-sm-4"> Tu contraseña de MySQL.
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="dbhost" class="col-sm-4 control-label">Servidor de la base de datos</label>
                                        <div class="col-sm-4">
                                            <input name="dbhost" id="dbhost" class="form-control" type="text" size="25" value="localhost" required  autofocus />
                                        </div>
                                        <div class="col-sm-4">
                                            Normalmente sera <strong>localhost</strong>.
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="prefijo" class="col-sm-4 control-label">Prefijo de tabla</label>
                                        <div class="col-sm-4">
                                            <input name="prefijo" id="prefijo" class="form-control" type="text" value="dk_"  size="25" required  autofocus />
                                        </div>
                                        <div class="col-sm-4">
                                            Cambia esto si quieres ejecutar varias instalaciones de DEMOKRATIAN en una sola base de datos. . *Usa minusculas, no uses acentos o caracteres extraños y no dejes espacios en blanco
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-offset-4 col-sm-6">
                                            <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Configurar BBDD</button>
                                        </div>
                                    </div>
                                </form>
                            <?php } ?>
                        </div>

                    </div>

                </div>

                <?php
            }
            ?>

            <div id="cargando" > <img class="cargador" src='../temas/demokratian/imagenes/loading.gif'/> <div  class="cargador2" > <h4>Esto puede tardar unos minutos...</h4></div>
            </div>
            <!--segunda fase de la configuración-->
            <div class="row" id="segunda_fase"> <div class="col-lg-12"> <br/>
                    <p class="bg-success"> Ha terminado con la primera fase de la configuración, enseguida terminamos, puede pasar a la segunda fase &nbsp;&nbsp;<a href="install2.php" class="btn btn-primary btn-lg active" role="button">Segunda fase</a></p>
                    <br/></div>
            </div>
            <!--fin segunda fase-->

            <div id="footer" class="row">
                <div  class="pie_demokratia">

                    <div class="pie_demokratia2">
                        <a href="http://www.demokratian.org" target="_blank"><img src="../temas/demokratian/imagenes/logo_pie.png" class="img-responsive"  alt="DEMOKRATIA | plataforma de votaciones"  /></a>
                    </div>
                    <div class="pie_demokratia1"> </div>

                </div>
            </div>




        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#segunda_fase").hide();
                $("#cargando").hide();
            });

            $("#cargando").on("ajaxStart", function() {
                // this hace referencia a la div con la imagen.
                $(this).show();
            }).on("ajaxStop", function() {
                $(this).hide();
            });
        </script>
   <!--<script src="js/jquery.validate.js"></script>-->
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../js/jqBootstrapValidation.js"></script>
        <script src="configura_BBDD.js"></script>

    </body>
</html>
