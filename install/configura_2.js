// JavaScript Document
/*
 Jquery Validation using jqBootstrapValidation
 example is taken from jqBootstrapValidation docs 
 */
// $('#contenido').on('submit','#formulario',function(event){ 
$(function () {
    $("#FormBBDD").find("input,select").jqBootstrapValidation(// este seria con un formulario con class="form-horizontal"
            //$("input#name,input#email,select#provincia").jqBootstrapValidation(  //  ver que ese texarea sera un select
                    {
                        preventSubmit: true,
                        submitError: function ($form, event, errors) {
                            // something to have when submit produces an error ?
                            // Not decided if I need it yet
                        },
                        submitSuccess: function ($form, event) {
                            event.preventDefault(); // prevent default submit behaviour
                            // get values from FORM
                            $('#cargando').show("slow");  // show the loading message.
                            var datos_formulario = $("#FormBBDD").serialize();

                            $.ajax({
                                url: "configura_2.php",
                                type: "POST",
                                data: datos_formulario,
                                cache: false,
                                success: function (data) {
                                    $('#cargando').hide("slow");  // show the loading message.
                                    var result = data.trim().split("#");
                                    if (result[0] == 'OK') {
                                        $("#FormBBDD").hide("slow");
                                        $("#1_fase1").hide("slow");
                                        $("#2_fase1").hide("slow");
                                        $('#success').html(" <p>&nbsp;</p>" + result[1] + " <p>&nbsp;</p>");
                                        //$('#success').find('div').html(result[1]);
                                        $('#success').show();
                                        $('#tercera_fase').show("slow");
                                        $('#FormBBDD').trigger("reset");
                                    } else {
                                        //$("#confirm").fadeTo("fast", 1);
                                        //$('#confirmvote').prop("disabled", false);
                                        $("#success2").html("<div class=\"alert alert-danger\">Se ha producido un error: " + result[1] + "</div>");
                                        $("#success2").show("slow");

                                    }
                                },
                                error: function () {
                                    // Fail message
                                    $('#success2').html("<div class='alert alert-danger'>");
                                    $('#success2 > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                            .append("</button>");
                                    $('#success2 > .alert-danger').append("<strong>Sorry " + firstName + " uppps! el servidor no esta respondiendo...</strong> Intetelo despues. Perdone por las molestias!");
                                    $('#success2 > .alert-danger').append('</div>');
                                },
//incluido para hacer pruebas, cuando funcione quitar y recuperar lo de arriba
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(xhr.status);
                                    alert(thrownError);
                                },
                                //hasta aqui


                            })
                        },
                        filter: function () {
                            return $(this).is(":visible");
                        },
                    });

            $("a[data-toggle=\"tab\"]").click(function (e) {
                e.preventDefault();
                $(this).tab("show");
            });
        });


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function () {
    $('#success2').html('');
});