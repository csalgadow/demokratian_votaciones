<?php

$timezone = false;
include("../basicos_php/basico.php");
include_once("../config/config.inc.php");
if (isset($nombre_web)) { //miramos que no exista ya la variable en el archivo
    echo "ERROR#La configuracion de este paso ya se ha realizado o hay algun error en el archivo config";
} else {
    $nombre_web = fn_filtro_nodb($_POST['nombre_web']);
    $email_env = fn_filtro_nodb($_POST['email_env']);
    $tipo_envio = fn_filtro_nodb($_POST['tipo_envio']);
    $puerto_correo = fn_filtro_nodb($_POST['puerto_correo']);
    $pass_correo = fn_filtro_nodb($_POST['pass_correo']);
    $user_correo = fn_filtro_nodb($_POST['user_correo']);
    $server_correo = fn_filtro_nodb($_POST['server_correo']);
    $theme = fn_filtro_nodb($_POST['theme']);
    $SMTPAuth = fn_filtro_nodb($_POST['SMTPAuth']);
    $SMTPSecure = fn_filtro_nodb($_POST['SMTPSecure']);
    $IsHTML = fn_filtro_nodb($_POST['IsHTML']);
    $smtp = fn_filtro_nodb($_POST['smtp']);
    $correo_insti = fn_filtro_nodb($_POST['correo_insti']);
    $elCorreo_insti = fn_filtro_nodb($_POST['elCorreo_insti']);
    $nombre_empresa = fn_filtro_nodb($_POST['nombre_empresa']);
    $idioma = fn_filtro_nodb($_POST['idioma']);
    if ($correo_insti == "true") {
        $es_municipal = "true";
    }
    if ($correo_insti == "false") {
        $es_municipal = fn_filtro_nodb($_POST['tipo_circuns']);
    }

    if ($_POST['zona_horaria'] == "false") {
        $zona_horaria = "false";
    } else if ($_POST['zona_horaria'] == "true") {
        $zona_horaria = fn_filtro_nodb($_POST['timezone']);
    }

    //$es_municipal = fn_filtro_nodb($_POST['tipo_circuns']);

    $file = "../config/config.inc.php"; //archivo que hay que modificar					
    #Abrimos el fichero en modo de escritura 

    $fh = fopen($file, 'r+');


    #preparamos el string con los datos
    $string1 = "
###################################################################################################################################
##########                                               Configuracion sitio                                             ##########  
###################################################################################################################################

								
\$nombre_web = \"" . $nombre_web . "\";    // Nombre del sitio web
\$tema_web = \"" . $theme . "\";                       // Nombre del tema (carpeta donde se encuentra)
\$info_versiones = true;         //sistema de avisos de nuevas versiones, por defecto habilitado (true)";

    if ($zona_horaria == "false") {
        $string5 = "
\$timezone = false ;    // zona horaria";
    } else {
        $string5 = "
\$timezone = \"" . $zona_horaria . "\";    // zona horaria";
    }

    $string4 = "
\$defaul_lang = \"" . $idioma . "\";  //idioma por defecto


###################################################################################################################################
##########                          Direcciones de correo para los distintos tipos de envios                             ##########  
###################################################################################################################################


\$email_env = \"" . $email_env . "\"; //direccion de correo general
\$email_error = \"" . $email_env . "\"; //sitio al que enviamos los correos de los que tienen problemas y no estan en la bbdd, 
                                     //este correo es el usado si no hay datos en la bbdd de los contactos por provincias
\$email_control = \"" . $email_env . "\"; //Direccion que envia el correo para el control con interventores

\$email_error_tecnico = \"" . $email_env . "\";//correo electronico del responsable tecnico
\$email_sistema = \"" . $email_env . "\"; //correo electronico del sistema, demomento incluido en el envio de errores de la bbdd

\$asunto_mens_error = \"Usuario de votaciones " . $nombre_web . " con problemas \"; //asunto del mensaje de correo cuando hay problemas de acceso
\$nombre_eq = \"Votaciones " . $nombre_web . "\"; //asunto del correo


\$nombre_sistema = \"Sistema de  votaciones " . $nombre_web . "\"; // Nombre del sistema cuando se envia el correo de recupercion de clave
\$asunto = \"Recuperar tu contraseña en " . $nombre_web . "\";// asunto para recuperar la contraseña



###################################################################################################################################
#############         Configuracion del correo smtp , solo si es tenemos como true la variable \$correo_smtp          #############
###################################################################################################################################
\$correo_smtp = " . $smtp . ";        //poner en false si queremos que el envio se realice con phpmail() y true si es por smtp

\$user_mail = \"" . $user_correo . "\";        // root de correo
\$pass_mail = \"" . $pass_correo . "\";  //  de correo
\$host_smtp = \"" . $server_correo . "\";        // localhost del correo  
\$puerto_mail = " . $puerto_correo . ";
\$mail_sendmail = " . $tipo_envio . "; // en algunos sevidores como 1¬1 hay que usar IsSendMail() en vez de IsSMTP() por defecto dejar en false";

    if ($SMTPSecure == "false") {
        $string2 = "
\$mail_SMTPSecure = " . $SMTPSecure . "; //Set the encryption system to use - ssl (deprecated) or tls";
    } else {
        $string2 = "
\$mail_SMTPSecure = \"" . $SMTPSecure . "\"; //Set the encryption system to use - ssl (deprecated) or tls";
    }

    $string3 = "
\$mail_SMTPAuth = " . $SMTPAuth . "; //Whether to use SMTP authentication
\$mail_IsHTML = " . $IsHTML . "; //
\$mail_SMTPOptions = false; //por defecto false. Algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validación de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción en true.
###################################################################################################################################
##########                                 Configuración para uso municipal                                              ##########  
###################################################################################################################################	
\$es_municipal = " . $es_municipal . ";  // true si vamos a trabajar solo con un tipo de circunscripcion y no queremos que pregunte la provincia
                      //Si se usa la opción de correos institucionales debe de ser true

###################################################################################################################################
##########              Configuración para uso de correos institucionales de empresas u organizaciones                   ##########  
###################################################################################################################################	
\$insti = " . $correo_insti . ";   //si usa el sistema de inscripcion de correos instirucionales, por defecto false
\$term_mail = \"" . $elCorreo_insti . "\";    // terminacion del correo institucional incluyendo la arroba, por defecto vacio
\$term_empre = \"" . $nombre_empresa . "\";    // Nombre de la empresa

###################################################################################################################################
##########                                 Configuración del uso de recaptcha                                            ##########
###################################################################################################################################
\$reCaptcha = false;  // true si ponemos en marcha el recaptcha
\$reCAPTCHA_site_key = \" \";        //Clave publica que proporciona google recaptcha
\$reCAPTCHA_secret_key = \" \";      //Clave privada que proporciona google recaptcha


?>";


    $string = $string1 . $string5 . $string4 . $string2 . $string3;

    fseek($fh, -3, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
    fwrite($fh, $string) or die("Could not write file!");
    /* if (fwrite($fh, $string1) === FALSE){ //escribimos en el archivo
      echo "ERROR#No se puede escribir en el archivo ($file)";

      }else{
      echo "OK#Se han guardado los datos correctamente)";
      }
     */


    fclose($fh);  //  Cerramos el fichero 
    chmod($file, 0600); //cambiamos los permisos de ese archivo a 600										
    echo "OK#<div class=\"alert alert-info\"> Se han guardado los datos correctamente</div>";
}
?>
