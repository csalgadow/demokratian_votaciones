﻿# README #

Los requerimientos necesarios para poner en marcha la plataforma DEMOKRATIAN VOTACIONES son muy sencillos. Necesita un servidor apache con php7.2 y una base de datos mysql. Dependiendo de si tiene muchos miles de inscritos el servidor tendrá que ser más o menos potente.

##Donar##

Si quieres ayudarnos a mantener el proyecto, puedes hacer una donación.
[DONAR con PAYPAL](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=LAC3ZCRAWYAU2&lc=ES&item_name=Donaci%c3%b3n%20a%20%20Demokratian%2eorg&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)
También puedes donar tu tiempo y participar en el desarrollo de DEMOKRATIAN, contacta con nosotros info@demokratian.org

### ¿Para que es este repositorio? ###

* DEMOKRATIAN VOTACIONES, una plataforma desarrollada por [Carlos Salgado Werner](http://carlos-salgado.es) para poder implementar una forma de decisión horizontal en su organización. Todos los miembros registrados podrán votar de una forma sencilla con la seguridad de que su voto es secreto.
Permite múltiples tipos de votación, actualmente dispone de 4 tipos o formas de votación. De esta forma el administrador puede elegir entre abrir un debate, realizar una encuesta, hacer una votación con voto ponderado, o realizar una votación con recuento por VUT.
DEMOKRATIAN VOTACIONES está especialmente diseñada para que sea muy fácil de usar, tanto por el usuario final, el votante, como por los distintos niveles de administradores. De esta forma no tendrá que preocuparse más que de realizar las preguntas o añadir sus candidatos para sus votaciones.
* Puedes descargar la ultima versión en: (https://bitbucket.org/csalgadow/demokratian_votaciones/)
* Tienes información adicional en el [WIKI del repositorio](bitbucket.org/csalgadow/demokratian_votaciones/wiki/)
)
* Web de [demokratian.org](http://demokratian.org)


### Como configurar la aplicación ###

Para instalar la aplicación, suba todos los archivos a su servidor.
Hay 4 carpetas que tienen que tener permiso de escritura para el usuario con el que corre apache: config, upload_user, upload_pic, data_vut (usamos chown $USUARIO.$GRUPO y chmod 0700 para ello). Estas carpetas puede cambiarlas el nombre posteriormente mediante el archivo de configuración.

La aplicación tiene un sistema de instalación automático, para ello debe de ir mediante su navegador a la carpeta install (midominio.org/install)
Para realizar la instalación necesita conocer los datos de la base de datos de su servidor asi como los datos de configuración del servidor de correo.
Esa información se usa para configurar el archivo config.inc.php que está en la carpeta config. Si por cualquier motivo la creación automática no funcionara, o posteriormente quiere hacer una modificación que no pueda realizar mediante el panel de administración de la aplicación, siempre podrá modificar ese archivo a mano.
Es posible que si su servidor tiene poca memoria asignada, o el tiempo de ejecución de PHP es escaso, no funcione la instalación automatica, en ese caso, si no puede cambiarla, tendrá que hacer una instalación manual [instrucciones de Instalacón](https://bitbucket.org/csalgadow/demokratian_votaciones/wiki/Instalaci%C3%B3n)

Si usa una base de datos compartida con otras aplicaciones, es muy recomendable hacer una copia de seguridad de la misma antes de proceder a la instalación. Durante la misma se pueden producir errores inesperados y perder datos. Demokratian no se responsabiliza de posibles bugs o errores que puedan generar perdida de datos.  

### Como contribuir ###

Demokratian VOTACIONES es software libre bajo licencia gpl-3.0 por tanto eres libre de usarlo, pero si quieres también puedes colaborar en su desarrollo.
* Si sabes programar en PHP y te apetece mejorar la aplicación y convertirte en contribuidor, contacta con nosotros, todas las manos para mejorar y crear nuevas opciones para DEMOKRATIAN son bienvenidas.
* La nueva versión de DEMOKRATIAN permitirá cambiar el idioma y usar otros idiomas, asi que necesitamos traductores, preferentemente de Ingles, Francés, Alemán, Italiano, Euskera, Catalán, Gallego o Esperanto.
Si te animas a participar puedes hacerlo aquí [https://poeditor.com/join/project/3kA5ce9fns]   Si quieres traducirlo a otro idioma, también será muy bien venido
* También puedes colaborar haciendo una donación que nos pueden ayudar a mantener los servidores de desarrollo o animarnos a seguir desarrollando en proyecto.[DONAR con PAYPAL](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=LAC3ZCRAWYAU2&lc=ES&item_name=Donaci%c3%b3n%20a%20%20Demokratian%2eorg&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)


### Contactar ###

Para contactar puede hacerlo mediante el correo info@demokratian.org
Aun no tenemos equipo de contribuidores así que  no podemos darle más formas de contacto.

### Contribuidores ###
* En el código:
-> Antonio Garcia (rotobator[AT]gmail.com)
