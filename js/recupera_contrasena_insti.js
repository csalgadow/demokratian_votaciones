// JavaScript Document

$(function () {
    $("#contrasenaFormInsti").find("input,select").jqBootstrapValidation(// este seria con un formulario con class="form-horizontal"

            {

                preventSubmit: true,
                submitError: function ($form, event, errors) {
                    // something to have when submit produces an error ?
                    // Not decided if I need it yet
                },

                submitSuccess: function ($form, event) {
                    event.preventDefault(); // prevent default submit behaviour
                    // get values from FORM
                    var dataString = $("#contrasenaFormInsti").serialize();

                    $.ajax({
                        url: "basicos_php/procesar_insti.php",
                        type: "POST",
                        data: dataString,
                        cache: false,
                        success: function (data) {
                            // Success message
                            //$('#success').html(" " + data + " ");

                            var result = data.trim().split("##");
                            if (result[0] == 'OK') {
                                $("#divcontrasenaFormInsti").hide("slow");
                                $('#successInsti').html(result[1]);
                                $('#successInsti').show();
                                $('#contactForm').trigger("reset");
                            } else if (result[0] == 'ERROR') {

                                $("#successInsti").show();
                                $('#successInsti').html(result[1]);
                            } else {

                                $("#successInsti").show();
                                $('#successInsti').html(data);
                            }
                        },
                        error: function () {
// 				// Fail message
                            $('#success').html("<div class='alert alert-danger'>");
                            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                    .append(" " + contrasenaFormInsti + " , hay un error</button>");


                        }
                    })
                },
                filter: function () {
                    return $(this).is(":visible");
                },
            });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function () {
    $('#success').html('');
});