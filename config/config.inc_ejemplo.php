<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
##########                                                                                                                                           ##########
##########                                                 CONFIGURACIÓN del la PLATAFORMA DE VOTACIONES                                             ##########  
##########                                                   Software creado por Carlos Salgado                                                      ##########  
##########                                                                                                                                           ##########
###############################################################################################################################################################
###################################################################################################################################

$hostu = "usuario";                              // Usario de la BBDD
$hostp = "password";                          // Contraseña de la BBDD
$dbn = "votaciones_demokratian";              // Nombre de la BBDD
$host = "host";                            // localhost de la BBDD                  		  					 				 
$extension = "dk_";                             // prefijo de las tablas de la base de datos

$url_vot = "http://localhost/pruebas_2.2.x";      // Url donde instalamos nuestra aplicación
##################################################################################################################################
###############     variables del sistema de subida y/o redimension de imagenes de los candidatos y roots       ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

$upload_cat = "../upload_pic";          //carpeta donde se guardan las imagenes de los candidatos
$upload_user = "../upload_user";        //carpeta donde se guardan las imagenes de los roots
$baseUrl = $url_vot . "/userfile/";      //   carpeta donde se guardan las imagenes y archivos de gestor ckfinder
##################################################################################################################################
###############                                        configuración de carpetas                                   ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

$FileRec = "../data_rec/";                  //   carpeta donde se generan los archivos de recuento
$FilePath = "../data_vut/";                 //   carpeta donde se generan los archivos del vut
$path_bakup_bbdd = "../admin/backup";       // Carpeta donde se guardan los back-up de la bbdd
###################################################################################################################################
#############                       Todo este grupo de variables no tienen porque ser modificadas.                    #############
#############                                 hacerlo solo si se tiene conocimientos                                  #############
###################################################################################################################################

$b_debugmode = 0; // 0 || 1  Forma de errores cuando hay problemas con la base de datos
##################################################################################################################################
#####################                               Otras viariables del sistema                             #####################
##################################################################################################################################
$tiempo_session = 900;  // tiempo de caducidad de la sesion en segundos 900 son 15 minutos

$usuarios_sesion = "nqYnDSrLAF"; // nombre de la sesion 
$usuarios_sesion2 = "nxLRDqEMNJ"; // nombre de la sesion de los interventores
$clave_encriptacion = "WuSeGpLempg"; // 
$clave_encriptacion2 = "jOmGjIxXDZA";


###################################################################################################################################
##########                                               Configuracion sitio                                             ##########  
###################################################################################################################################


$nombre_web = "DEMOKRATIAN |  Centro de votaciones";    // Nombre del sitio web
$tema_web = "demokratian";                       // Nombre del tema (carpeta donde se encuentra)
$info_versiones = true;         //sistema de avisos de nuevas versiones, por defecto habilitado (true)
$timezone = false;    // zona horaria
$defaul_lang = "es_ES";  //idioma por defecto
###################################################################################################################################
##########                          Direcciones de correo para los distintos tipos de envios                             ##########  
###################################################################################################################################


$email_env = "info1@demokratian.org"; //direccion de correo general
$email_error = "info1@demokratian.org"; //sitio al que enviamos los correos de los que tienen problemas y no estan en la bbdd, 
//este correo es el usado si no hay datos en la bbdd de los contactos por provincias
$email_control = "info1@demokratian.org"; //Direccion que envia el correo para el control con interventores

$email_error_tecnico = "info1@demokratian.org"; //correo electronico del responsable tecnico
$email_sistema = "info1@demokratian.org"; //correo electronico del sistema, demomento incluido en el envio de errores de la bbdd

$asunto_mens_error = "Usuario de demokratian con problemas "; //asunto del mensaje de correo cuando hay problemas de acceso
$nombre_eq = "Votaciones demokratian"; //asunto del correo


$nombre_sistema = "Votaciones demokratian"; // Nombre del sistema cuando se envia el correo de recupercion de clave
$asunto = "Recuperar tu contraseña en demokratian"; // asunto para recuperar la contraseña
###################################################################################################################################
#############         Configuracion del correo smtp , solo si es tenemos como true la variable $correo_smtp          #############
###################################################################################################################################
$correo_smtp = true;        //poner en false si queremos que el envio se realice con phpmail() y true si es por smtp

$user_mail = "info@demokratian.org";        // root de correo
$pass_mail = "5555555555";  //  de correo
$host_smtp = "mail.demokratian.org";        // localhost del correo  
$puerto_mail = 587;
$mail_sendmail = false; // en algunos sevidores como 1¬1 hay que usar IsSendMail() en vez de IsSMTP() por defecto dejar en false
$mail_SMTPSecure = "TLS"; //Set the encryption system to use - ssl (deprecated) or tls
$mail_SMTPAuth = true; //Whether to use SMTP authentication
$mail_IsHTML = false; //
$mail_SMTPOptions = false; //por defecto false. Algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción en true.
###################################################################################################################################
##########                                 Configuración para uso municipal                                              ##########  
###################################################################################################################################	
$es_municipal = true;  // true si vamos a trabajar solo con un tipo de circunscripcion y no queremos que pregunte la provincia
//Si se usa la opción de correos institucionales debe de ser true
###################################################################################################################################
##########                                 Configuración del uso de recaptcha                                            ##########
###################################################################################################################################
$reCaptcha = false;  // true si ponemos en marcha el recaptcha
$reCAPTCHA_site_key = "";        //Clave publica que proporciona google recaptcha
$reCAPTCHA_secret_key = "";      //Clave privada que proporciona google recaptcha
###################################################################################################################################
##########              Configuración para uso de correos institucionales de empresas u organizaciones                   ##########  
###################################################################################################################################	
$insti = false;   //si usa el sistema de inscripcion de correos instirucionales, por defecto false
$term_mail = "";    // terminacion del correo institucional incluyendo la arroba, por defecto vacio
$term_empre = "";    // Nombre de la empresa
###################################################################################################################################
##########                                               Autenticacion                                                   ##########  
###################################################################################################################################								
$cfg_autenticacion_solo_local = true;    // ¿Permitimos autenticacion federada? cfg_autenticacion_solo_local==false -> SI
$cfg_dir_simplesaml = "";                    // Directorio donde hemos instalado simplesamlphp en modo SP.
$cfg_crear_usuarios_automaticamente = true;    // Cuando tenemos autenticacion federada, ¿creamos usuarios automaticamente?
$cfg_tipo_usuario = 1;    // Al crear automaticamente, el tipo por defecto
?>