<?php

if (@preg_match("time_stamp.php", $_SERVER['SCRIPT_NAME'])) {
    Header("Location: ../index.php");
    die();
}

function time_stamp($session_time) { //Tiempo transcurrido
    $time_difference = time() - $session_time;
    $seconds = $time_difference;
    $minutes = round($time_difference / 60);
    $hours = round($time_difference / 3600);
    $days = round($time_difference / 86400);
    $weeks = round($time_difference / 604800);
    $months = round($time_difference / 2419200);
    $years = round($time_difference / 29030400);

    if ($seconds <= 60) {
        echo _("Hace") . $seconds . _("segundos");
    } else if ($minutes <= 60) {
        if ($minutes == 1) {
            echo _("Hace un minuto");
        } else {
            echo _("Hace $minutes minutos");
        }
    } else if ($hours <= 24) {
        if ($hours == 1) {
            echo _("Hace una hora");
        } else {
            echo _("Hace") . "  " . $hours . ( " horas");
        }
    } else if ($days <= 7) {
        if ($days == 1) {
            echo _("Hace un día");
        } else {
            echo _("Hace") . "  " . $days . "  " . _("días");
        }
    } else if ($weeks <= 4) {
        if ($weeks == 1) {
            echo _("Hace una semana");
        } else {
            echo _("Hace") . "  " . $weeks . "  " . _("semanas");
        }
    } else if ($months <= 12) {
        if ($months == 1) {
            echo _("Hace un mes");
        } else {
            echo _("Hace") . "  " . $months . "  " . _("meses");
        }
    } else {
        if ($years == 1) {
            echo _("Hace un año");
        } else {
            echo _("Hace") . "  " . $years . "  " . _("años");
        }
    }
}

?>