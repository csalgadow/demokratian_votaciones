<?php

if (false === function_exists('gettext')) {
    echo "No tiene la libreria gettext instalada.";
    exit();
} else {
//$defaul_lang="es_ES.UTF-8";  //idioma por defecto
    /////////////////  ESTE ES EL SCRIP QUE FUNCIONARA CUANDO LOS USUARIOS PUEDAN COGER EL IDIOMA ////////////////////
    $dir_lang = "../locale"; //directorio conde tenemos las traducciones, ojo, cambiar para paginas interiores

    if (isset($_GET["lang"])) {
        $locale = $_GET["lang"];
        $_SESSION["locale"] = $locale;
    } else if (isset($_SESSION["locale"])) {
        $locale = $_SESSION["locale"];
    }
    /* else {
      $lang = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
      echo $lang;
      $nombre_fichero = $dir_lang . "/" . $lang;

      if (file_exists($nombre_fichero)) {  //comprobamos si exite el idioma
      $locale = $lang;
      $_SESSION["locale"] = $lang;
        } */
        else {  // si no exite ponemos  el idioma por defecto
        $locale = $defaul_lang;
        $_SESSION["locale"] = $defaul_lang;
    }
    //}

    $locale = $locale . ".UTF-8";
    //$locale = $defaul_lang . ".UTF-8";  //// eliminar esta linea cuando los usuarios puedan coger el idioma por defecto
// hay que mirar si con el forzado a UTF funcionan todos los idiomas bien

    $lang = substr($locale, 0, 2);
    putenv('LC_ALL=' . $lang);
    setlocale(LC_ALL, $locale);
    bindtextdomain("messages", "./locale"); // locale para las paginas exteriores
    bindtextdomain("messages", "./../locale"); // locale para el interior
    bind_textdomain_codeset('default', 'UTF-8');
    textdomain("messages");
}
?>