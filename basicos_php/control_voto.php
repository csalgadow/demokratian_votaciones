<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
?>
<div id="control_seg">
    <?php
    // sistema para incluir internventores o clave voto seguro


    if ($seguridad == 2 or $seguridad == 4) {
        ?>
        <p><?= _("Esta votación permite comprobacion de voto posterior, para ello debe introducir una clave que debe recordar y que le servira para una comprobacion posterior") ?></p>
        <p> <?= _("Se recomienda no usar la misma clave que usa para identificarse al acceder a la plataforma de votaciones para evitar algun tipo de vinculación de su voto con su usuario") ?></p>
        <div class="form-group">       
            <label for="clave_seg"  class="col-sm-3 control-label"><?= _("Clave de seguridad") ?></label>
            <div class="col-sm-3">
             <!--<input type="text" name="clave_seg" id="clave_seg" />-->
                <input type="password" name="clave_seg" id="clave_seg" value="" placeholder="<?= _("Introduzca su clave") ?>" autofocus required class="form-control"/>
            </div></div>
        <p>&nbsp;</p>
    <?php } else { ?>
        <input name="clave_seg" type="hidden" id="clave_seg" value="non_use" /> 
    <?php } ?> 


    <?php if ($seguridad == 4 or $seguridad == 3) { ?>
        <div class="alert alert-success">  
            <?= _("Esta votación tiene interventores que recibiran su papeleta de voto de forma anonima y son") ?>:

            <?php
            $sql = "SELECT nombre, apellidos FROM $tbn11 WHERE id_votacion = '$idvot'  and tipo<=1  ORDER BY 'nombre'";
            $result = mysqli_query($con, $sql);
            if ($row = mysqli_fetch_array($result)) {
                ?>

                <ul>   
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <li> <?php echo "$row[0]" ?> <?php echo "$row[1]" ?></li>

                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>
                </ul></div>
            <?php
        }
    }
    ?>
</div>