<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('verifica.php');
//$nivel_acceso=11; if ($nivel_acceso <= $_SESSION['usuario_nivel']){
if (empty($_SESSION['numero_vot'])) {
    header("Location: $redir?error_login=5");
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->


            <div class="row">




                <div class="col-md-2" >

                    <a href="log_out.php"><?= _("SALIR") ?></a>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1> <?= _("Ha accedido a la votación") ?> <strong>" <?php echo $_SESSION['nombre_votacion']; ?> "</strong><?= _("para generar sus claves publica y privada") ?>.</h1>
                    <h3><span class="label label-warning"> <?= _("Recuerde que es sumamente importante que guarde sus claves y realice correctamente el proceso") ?></span></h3>



                    <p><a href="genera_clave.php"><?= _("GENERAR CLAVES") ?></a>

                        <!--Final-->

                    </p>
                    <p><a href="guarda_clave.php"><?= _("GUARDAR CLAVE PRIVADA PARA INICIAR LA DESCODIFICACIÓN") ?></a></p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->

                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>


 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function loadAcceso() {
                //Funcion para cargar el muro
                $("#acceso").load('acceso.php?idvot=<?php echo ($_SESSION['numero_vot']); ?>');
                //Devuelve el campo message a vacio
                // $("#msg").val("")
                //var idvot = $("#idvot").val();
            }

        </script>

        <?php
//        if ($_SESSION['numero_inter'] == $contador) {
//            //echo "Ya estan validados todos los interventores para esta votación<br/>";
//
        ?>
<!--            <script type="text/javascript">
                $(document).ready(function() {
                    loadAcceso();
                });
            </script>-->
        <?php
//        }
        ?>

                                <!--<script type="text/javascript">
                                $(document).ready(function(){
                        loadAcceso();
                                 });
                                 </script>-->

    </body>
</html>
