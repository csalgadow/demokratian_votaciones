<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

////////////////Borrado multible/////////////////////////
if (isset($_POST["delete_multi"])) {
    $mens_ok = "";
    unset($_POST['tabla1_length']);


    foreach ($_POST as $k => $v)
        $a[] = $v;
    $datos = count($a) - 7;
    $i = 0;

    while ($i < $datos) {
        $val = fn_filtro($con, $a[$i]);

        ///borramos la lista de votaciones donde ha participado
        $borrado_usuario_x_votacion = "DELETE FROM $tbn2 WHERE id_votante=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " usuario_x_votacion";
        $result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);

        //borramos debate_comentario con esta id
        $borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_usuario=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " debate comentario";
        $result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);

        //borramos debate_votos con esta id
        $borrado_debate_votos = "DELETE FROM $tbn14 WHERE id_votante=" . $val . "";
        $mens = _("ERROR en el borrado de la tabla") . " debate votos";
        $result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);

        //borramos la relacion entre votantes y modifciaciones de los administradores
        $borrado_votantes_x_admin = "DELETE FROM $tbn17 WHERE id_votante=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " votantes_x_admin";
        $result_votantes_x_admin = db_query($con, $borrado_votantes_x_admin, $mens);

        //borramos si es administrador, la relacion de provincias que pueda tener asignadas
        $borrado_usuario_admin_x_provincia = "DELETE FROM $tbn5 WHERE id_usuario=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . "usuario_admin_x_provincia";
        $result_usuario_admin_x_provincia = db_query($con, $borrado_usuario_admin_x_provincia, $mens);

        //borramos si es administrador, la relacion de grupos de trabajo que pueda tener asignadas
        $borrado_usuario_x_g_trabajo = "DELETE FROM $tbn6 WHERE id_usuario=" . $val . " ";
        $mens = _("ERROR en el borrado de la tabla") . " usuario_x_g_trabajo";
        $result_usuario_x_g_trabajo = db_query($con, $borrado_usuario_x_g_trabajo, $mens);

        //miramos que imagen tiene el usuario para borrarla
        $sql_img = "SELECT id,imagen_pequena FROM $tbn9 WHERE ID=" . $val . " ";
        $result_img = mysqli_query($con, $sql_img);
        $row_img = mysqli_fetch_row($result_img);

        if ($row_img[1] != "peq_usuario.jpg") {

            $thumb_photo_exists = $upload_user . "/" . $row_img[1];
            echo $thumb_photo_exists;
            if (file_exists($thumb_photo_exists)) {
                unlink($thumb_photo_exists);
            }
        }

        $borrar = "DELETE FROM $tbn9 WHERE id=" . $val . " ";
        $mens = "ERROR en el borrado del votante";
        $result = db_query($con, $borrar, $mens);

        if (!$result) {
            $mens_error .= $mens . "<br/>";
        } else {
            $mens_ok .= _("Borrados correctamente $datos usuarios") . " <br/>";
        }

        $i++;
    }
}
if (isset($_POST['provincia'])) {
    $id_provincia = fn_filtro($con, $_POST['provincia']);
} else {
    $id_provincia = "";
}
$nombre_usuario = fn_filtro($con, $_POST['nombre_usuario']);
$correo_usuario = fn_filtro($con, $_POST['correo_electronico']);
$nif = fn_filtro($con, $_POST['nif']);
$tipo_votante = fn_filtro($con, $_POST['tipo_usuario']);
if (isset($_POST['usuario'])) {
    $usuario = fn_filtro($con, $_POST['usuario']);
} else {
    $usuario = "";
}

$sql = "SELECT ID, id_provincia,  nombre_usuario, correo_usuario, nif, tipo_votante, usuario,bloqueo,apellido_usuario FROM $tbn9 WHERE id_provincia like '%$id_provincia%' and nombre_usuario like '%$nombre_usuario%' and correo_usuario like '%$correo_usuario%' and nif like '%$nif%' and tipo_votante like '%$tipo_votante%' ORDER BY 'nombre_usuario' ";
$result = mysqli_query($con, $sql);
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1><?= _("CENSO") ?> </h1>
                    <p>&nbsp;</p>
                    <?php
                    if (isset($borrado)) {
                        echo "$borrado";
                    }
                    ?>
                    <p>&nbsp;</p>

                    <form name="formulario" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <?php
                        if ($row = mysqli_fetch_array($result)) {
                            ?>

                            <table id="tabla1" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width=15%><?= _("Nombre") ?></th>
                                        <th width=15%><?= _("Apellido") ?></th>
                                        <th width=29%><?= _("Correo") ?></th>
                                        <th width=3%><?= _("Tipo") ?></th>
                                        <th width=3%><?= _("Nif") ?></th>
                                        <?php if ($es_municipal == false) { ?>
                                            <th width=7%><?= _("provincia") ?></th>
                                        <?php } ?>
                                        <th width=8%><?= _("bloqueado") ?></th>
                                        <th width=8%><?= _("modificar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th width=6% align=center><?= _("borrar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th width=14% align=center><?= _("borrado") ?><br />
                                            <?= _("multiple") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    mysqli_field_seek($result, 0);

//echo "</tr> \n";
//$var_bol=true;
                                    do {
                                        ?>
                                        <tr>
                                            <td><?php echo "$row[2]" ?></td>
                                            <td><?php echo "$row[8]" ?></td>
                                            <td><?php echo "$row[3]" ?></td>
                                            <td><?php echo "$row[5]" ?></td>
                                            <td><?php echo "$row[4]" ?></td>
                                            <td><?php echo "$row[1]" ?></td>
                                            <td><?php echo "$row[7]" ?></td>

                                            <td><a href="censos.php?id=<?php echo "$row[0]" ?>&acc=modifika" ><?= _("modificar") ?><br>
                                                </a></td>
                                            <td><a href="censos_borra.php?id=<?php echo "$row[0]" ?>&idvot=<?php echo "$row[2]" ?>" onClick="return borravotante()" ><?= _("Borrar") ?> </a></td>
                                            <td>
                                                <input name="borrar_multiples<?php echo "$row[0]" ?>" type="checkbox" id="borrar_multiples" value="<?php echo "$row[0]" ?>">

                                            </td>
                                        </tr>
                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width=15%><?= _("Nombre") ?></th>
                                        <th width=15%><?= _("Apellido") ?></th>
                                        <th width=29%><?= _("Correo") ?></th>
                                        <th width=3%><?= _("Tipo") ?></th>
                                        <th width=3%><?= _("Nif") ?></th>
                                        <?php if ($es_municipal == false) { ?>
                                            <th width=7%><?= _("provincia") ?></th>
                                        <?php } ?>
                                        <th width=8%><?= _("bloqueado") ?></th>
                                        <th width=8%><?= _("modificar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th width=6% align=center><?= _("borrar") ?><br>
                                            <?= _("datos") ?></th>
                                        <th width=14% align=center><?= _("borrado") ?><br />
                                            <?= _("multiple") ?></th>
                                    </tr>
                                </tfoot>

                            </table>

                            <input name="provincia" type="hidden" value="<?php echo "$id_provincia"; ?>">
                            <input name="nombre_usuario" type="hidden" value="<?php echo "$nombre_usuario"; ?>">
                            <input name="correo_electronico" type="hidden" value="<?php echo "$correo_usuario"; ?>">
                            <input name="nif" type="hidden" value="<?php echo "$nif"; ?>">
                            <input name="tipo_usuario" type="hidden" value="<?php echo "$tipo_votante"; ?>">
                            <input name="usuario" type="hidden" value="<?php echo "$usuario"; ?>">
                            <p>&nbsp;</p>
                            <div class="derecha">
                                <input name="delete_multi" type="submit" class="btn btn-primary pull-right"   id="delete_multi" value="<?= _("Borrar multiples") ?>" />
                            </div>
                        </form>

                        <?php
                    } else {

                        echo _("¡No se ha encontrado ningún candidato!");
                    }
                    ?><!---->

                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" class="init">

                                                $(document).ready(function () {
                                                    $('#tabla1').dataTable({
                                                        "language": {
                                                            "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                                                            "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                                                            "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                                                            "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                                                            "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                                                            "loadingRecords": "<?= _("Cargando") ?>...",
                                                                                            "processing": "<?= _("Procesando") ?>...",
                                                                                            "search": "<?= _("Buscar") ?>:",
                                                                                            "paginate": {
                                                                                                "first": "<?= _("Primero") ?>",
                                                                                                "last": "<?= _("Ultimo") ?>",
                                                                                                "next": "<?= _("Siguiente") ?>",
                                                                                                "previous": "<?= _("Anterior") ?>"
                                                                                            },
                                                                                            "aria": {
                                                                                                "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                                                                "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                                                            }
                                                                                        },
                                                                                        "order": [0, "desc"],
                                                                                        "iDisplayLength": 50
                                                                                    });
                                                                                });
        </script>
        <script src="../js/admin_borrarevento.js"></script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
                                                                                $('#apuntarme').on('hidden.bs.modal', function () {
                                                                                    $(this).removeData('bs.modal');
                                                                                });
        </script>
    </body>
</html>
