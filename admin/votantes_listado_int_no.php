<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
///// este scrip se usa en varias paginas

if ($var_carga == true) {

    $contar = 1;
    $mensa = "";
///// cogemos los datos de la encuesta

    $result_vot = mysqli_query($con, "SELECT id_provincia, activa,nombre_votacion,tipo_votante, id_grupo_trabajo, demarcacion,id_ccaa, id_municipio  FROM $tbn1 where id=$idvot");
    $row_vot = mysqli_fetch_row($result_vot);

    $id_provincia_vot = $row_vot[0];
    $id_ccaa_vot = $row_vot[6];
    $activa = $row_vot[1];
    $tipo_votante = $row_vot[3];
    $id_grupo_trabajo = $row_vot[4];
    $id_municipio = $row_vot[7];

    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (isset($_SERVER['HTTP_VIA'])) {
        $ip = $_SERVER['HTTP_VIA'];
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $ip = $ip . "+" . $_SESSION['ID'];  //añadimos la ip y quien ha realizado la votacion presencial
    //$ip = $ip . "+" . $nombre . "+" . $_SESSION['ID'];  //añadimos la ip y quien ha realizado la votacion presencial
////si vota presencial metemos el registro

    if (isset($_GET['votacion'])) {
        if ($_GET['votacion'] == "ok") {
            $tipo_voto = 5;
            $id = $_GET['id'];

            $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn2 where id_votante='$id' and id_votacion='$idvot'") or die(mysqli_error());

            $total_encontrados = mysqli_num_rows($usuarios_consulta);

            mysqli_free_result($usuarios_consulta);


            if ($total_encontrados != 0) {

                $mensa = "<div class=\"alert alert-warning\">" . _("¡¡¡Error!!!") . " <br>" . _("El Usuario ya está registrado  o ha votado, operacion incorrecta") . ".</div>";
            } else {



                $result_votante = mysqli_query($con, "SELECT correo_usuario,tipo_votante,id_provincia FROM $tbn9 where id=$id");
                $row_votante = mysqli_fetch_row($result_votante);

                $correo_usuario = $row_votante[0];
                $tipo_usuario = $row_votante[1];
                $id_provincia_usu = $row_votante[2];
                // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
                $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                $cad = "";
                for ($i = 0; $i < 6; $i++) {
                    $cad .= substr($str, rand(0, 62), 1);
                }
                $time = microtime(true);
                $timecad2 = $time . $cad;
                $user_id = hash("sha256", $timecad2);
                $fecha_env = date("Y-m-d H:i:s");

                $insql_votacion = "insert into $tbn2 (ID, id_provincia, id_votante, id_votacion, tipo_votante, 	fecha, 	correo_usuario, forma_votacion,ip ) values (\"$user_id\",  \"$id_provincia_usu\",  \"$id\", \"$idvot\", \"$tipo_votante\", \"$fecha_env\", \"$correo_usuario\", \"$tipo_voto\",\"$ip\")";
                //$inres = @mysqli_query($con, $insql_votacion) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
                $mens = _("error añadido de votante de forma presencial");
                $result = db_query($con, $insql_votacion, $mens);
                if (!$result) {
                    echo "<div class=\"alert alert-danger\"><strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error") . "  </strong> </div>";
                } else {
                    $mensa .= "<div class=\"alert alert-success\">" . _("Actualizado correctamente") . " " . $correo_usuario . " " . _(" como votante presencial") . " </div>";
                }
            }
        }
    }


//permite impedir el voto forma temporal  para  congresos o eventos donde se vaya a votar de forma presencial
    if (isset($_POST["modificar_multi"])) {


        $tipo_voto = 6;
        //$id = $_GET['id'];
        unset($_POST['tabla1fal_length'], $_POST['modificar_multi']);
        foreach ($_POST as $k => $v) {
            $val = $v;

            $usuarios_consulta = mysqli_query($con, "SELECT ID,correo_usuario FROM $tbn2 where id_votante='$val' and id_votacion='$idvot'") or die(mysqli_error());

            $total_encontrados = mysqli_num_rows($usuarios_consulta);
            $row_error = mysqli_fetch_row($usuarios_consulta);
            mysqli_free_result($usuarios_consulta);


            if ($total_encontrados != 0) {
                $mensa .= "<div class=\"alert alert-warning\">" . _("¡¡¡Error!!! El Usuario") . " " . $row_error[1] . _("ya está registrado en el congreso o ha votado, operacion incorrecta") . ".</div>";
            } else {


                $result_votante = mysqli_query($con, "SELECT correo_usuario,tipo_votante,id_provincia  FROM $tbn9 where id=$val");
                $row_votante = mysqli_fetch_row($result_votante);



                $correo_usuario = $row_votante[0];
                $tipo_usuario = $row_votante[1];
                $id_provincia_usu = $row_votante[2];
                // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
                $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                $cad = "";
                for ($i = 0; $i < 6; $i++) {
                    $cad .= substr($str, rand(0, 62), 1);
                }
                $time = microtime(true);
                $timecad2 = $time . $cad;
                $user_id = hash("sha256", $timecad2);
                $fecha_env = date("Y-m-d H:i:s");

                $insql_votacion = "insert into $tbn2 (ID,id_provincia, 	id_votante,id_votacion, tipo_votante, fecha,correo_usuario, forma_votacion,ip ) values (\"$user_id\",  \"$id_provincia_usu\",  \"$val\", \"$idvot\", \"$tipo_votante\", \"$fecha_env\", \"$correo_usuario\", \"$tipo_voto\",\"$ip\")";

                $mens = _("error añadido de votante de forma presencial");
                $result = db_query($con, $insql_votacion, $mens);
                if (!$result) {
                    echo "<div class=\"alert alert-danger\"><strong> UPSSS!!!!<br/>" . ("esto es embarazoso, hay un error") . "  </strong> </div><strong>";
                } else {
                    $mensa .= "<div class=\"alert alert-success\">" . ("Actualizado correctamente") . " " . $correo_usuario . " " . _(" como votante del congreso") . " </div>";
                }
            }

            $i = $i + 1;
        }
    }

//aqui termina el scrip de bloqueo de voto
    if ($row_vot[5] == 1) {
        if (isset($_GET['id_nprov'])) {
            $id_provincia_url = fn_filtro($con, $_GET['id_nprov']);
        } else {
            if ($es_municipal == true) {  //incluimos que si es municipal la provincia es 001
                $id_provincia_url = 001;
            } else {
                $id_provincia_url = "";
            }
        }
        $sql = "SELECT ID,nombre_usuario , correo_usuario,tipo_votante, apellido_usuario
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_provincia = '$id_provincia_url' ";
    } else if ($row_vot[5] == 2) {
        $sql = "SELECT ID,nombre_usuario , correo_usuario,tipo_votante, apellido_usuario
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_ccaa = '$id_ccaa_vot' ";
    } else if ($row_vot[5] == 3) {
        $sql = "SELECT ID,nombre_usuario , correo_usuario,tipo_votante, apellido_usuario
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_provincia = '$id_provincia_vot' ";
    } else if ($row_vot[5] == 7) {
        $sql = "SELECT ID,nombre_usuario , correo_usuario,tipo_votante, apellido_usuario
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot
 ) and a.tipo_votante <=$tipo_votante and a.id_municipio = '$id_municipio' ";
    } else {
        ///falta los grupos    $sql = "SELECT a.ID, a.nombre_usuario , a.correo_usuario,a.tipo_votante FROM $tbn9 a,$tbn6 b  WHERE (a.ID= b.id_usuario) and id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' ";

        $sql = "SELECT a.ID, a.nombre_usuario , a.correo_usuario,a.tipo_votante, a.apellido_usuario
 FROM $tbn9 a,$tbn6 c
 WHERE  NOT EXISTS (
	 SELECT *
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante  and b.id_votacion=$idvot
 ) and (a.ID=c.id_usuario) and a.tipo_votante <=$tipo_votante and c.id_grupo_trabajo='$id_grupo_trabajo' ";
    }





    $result = mysqli_query($con, $sql);
    ?>

    <h1><?= _("Votación de") ?> <?php echo "$row_vot[2]" ?> </h1>
    <p><?php
        if (isset($mensa)) {
            echo "$mensa";
        }
        ?>&nbsp;</p>
    <h2><?= _("Listado del censo que NO ha votado") ?>
        <?php if ($es_municipal == false) { ?>
            <?php if ($row_vot[5] == 1 and isset($_GET['id_nprov'])) { ?>
                <?= _("para la provincia") ?> <strong> <?php echo $_GET['id_nprov']; ?></strong> <?= _("y tipo de votación") ?></h2>
        <?php } else if ($row_vot[5] == 2) { ?>
                <?= _("para la comunidad autonoma") ?> <strong><?php echo $row_vot[6]; ?></strong> <?= _("y tipo de votación") ?></h2>
        <?php } else if ($row_vot[5] == 3) { ?>
            <?= _("para la provincia") ?> <strong><?php echo $row_vot[0]; ?></strong> <?= _("y tipo de votación") ?></h2>
            <?php
        }
    } else {
        ?>
        </h2>  <h3><?= _("Tipo de votación") ?>  <?php } ?>

        <?php
        if ($row_vot[3] == 1) {
            echo _("solo para socios");
        } else if ($row_vot[3] == 2) {
            echo _("solo pata socios y simpatizantes");
        } else if ($row_vot[3] == 3) {
            echo _("abierta");
        }
        ?> </h3>



    <?php
    if ($row = mysqli_fetch_array($result)) {
        ?>
        <form name="form1" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">


            <table id="tabla1" class="cell-border hover" cellspacing="0" >
                <thead>
                    <tr>
                        <th width="3%">&nbsp;</th>
                        <th width="10%"><?= _("NOMBRE") ?></th>
                        <th width="30%"><?= _("APELLIDO") ?></th>
                        <th width="30%"><?= _("CORREO") ?></th>
                        <th width="5%"><?= _("TIPO") ?></th>
                        <th width="10%"><?= _("VOTA PRESENCIAL") ?></th>
                        <th width="5%"><?= _("CONGRESO") ?></th>

                    </tr>
                </thead>
                <tfoot>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th><input name="modificar_multi" type="submit" class="btn btn-primary pull-right" id="modificar_multi" value="<?= _("Modificar  multiples") ?>" />
                </th>
                </tfoot>
                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <tr>
                            <td><?php echo "$contar" ?></td>
                            <td><?php echo "$row[1]" ?></td>
                            <td><?php echo "$row[4]" ?></td>
                            <td><?php echo "$row[2]" ?></td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo _("socio");
                                } else if ($row[3] == 2) {
                                    echo _("simpatizante verificado");
                                } else if ($row[3] == 3) {
                                    echo _("simpatizante");
                                } else if ($row[3] == 5) {
                                    echo _("Aqui hay un error");
                                }
                                ?></td>
                            <td>
                                <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&votacion=ok&id=<?php echo "$row[0]" ?>&id_nprov=<?php echo $_GET['id_nprov']; ?>&cen=<?php echo $_GET['cen']; ?>&lit=<?php echo $_GET['lit']; ?>"  class=delete><?= _("Vota de forma presencial") ?> </a>    </td>
                            <td align="center"  bgcolor="<?php echo "$color" ?>"  ><input name="modificar_congreso<?php echo "$row[0]" ?>" type="checkbox" id="modificar_congreso" value="<?php echo "$row[0]" ?>" /></td>
                        </tr>
                        <?php
                        $contar = $contar + 1;
                    } while ($row = mysqli_fetch_array($result));
                    ?>
                </tbody>


            </table>
        </form>

        <?php
    } else {
        if ($id_provincia_url == "") {
            echo "<div class=\"alert alert-success\">" . _(" Escoja la provincia para la que quiere ver el censo") . "</div>";
        } else {
            echo "<div class=\"alert alert-success\"> " . _("¡No se ha encontrado votantes pendientes de votar!") . "</div> ";
        }
    }
} else {
    echo _("error acceso");
}
?>
