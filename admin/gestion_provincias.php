<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

if (ISSET($_POST["modifika_correo"])) {

    $id_provincia_mody = fn_filtro($con, $_POST['id_provincia_mody']);



    if (empty($_POST['correo_error'])) {
        $error = "error";
        $mensaje = "<div class=\"alert alert-warning\">El e-mail del usuario es un dato requerido</div>";
    } elseif (!filter_var($_POST['correo_error'], FILTER_VALIDATE_EMAIL)) {
        $error = "error";
        $mensaje = "<div class=\"alert alert-warning\">" . _("la direccion es erronea") . "</div>";
    } else {

        $correo_error = fn_filtro($con, $_POST['correo_error']);
        $sSQL = "UPDATE $tbn8 SET correo_notificaciones=\"$correo_error\" WHERE id='$id_provincia_mody'";
        mysqli_query($con, $sSQL) or die("Imposible modificar");
        $mensaje = "<div class=\"alert alert-success\">" . _("Modificado correo") . " " . $correo_error . " " . _("de la provincia numero") . " " . $id_provincia_mody . "</div>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h1><?= _("Correos notificaciones Provincias") ?></h1>


                    <?php
                    if ($_SESSION['nivel_usu'] == 2) {
                        $sql = "select DISTINCT id, provincia,correo_notificaciones from $tbn8  order by ID";
                    } else if ($_SESSION['nivel_usu'] == 3) {
                        $ids_ccaa = $_SESSION['id_ccaa_usu'];
                        $sql = "SELECT  id, provincia,correo_notificaciones FROM $tbn8 where id_ccaa=$ids_ccaa";
                    } else if ($_SESSION['nivel_usu'] == 4) {
                        $id_usu = $_SESSION['ID'];
                        $sql = "SELECT  a.id, a.provincia,a.correo_notificaciones FROM $tbn8 a, $tbn5 b  where (a.id=b.id_provincia) and b.id_usuario='$id_usu'";
                    }
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>
                        </p>
                        <?php
                        if (isset($mensaje)) {
                            echo $mensaje;
                        }
                        ?>

                        <form id="form1" name="form1" method="post" action="">

                            <table width=99%   class="table table-striped" cellspacing="0" >

                                <thead>
                                    <tr>
                                        <th width=5% align=center >Id</th>
                                        <th width=20% align=left ><?= _("Provincia") ?></th>
                                        <th width=65% align=center ><?= _("Correo para notificaciones de error") ?></td>
                                        <th width=10% align=center >&nbsp;</th>
                                    </tr></thead>

                                <tbody>

                                    <?php
                                    mysqli_field_seek($result, 0);

                                    do {
                                        ?>

                                        <tr>
                                    <form id="<?php echo "$row[1]" ?>" name="<?php echo "$row[1]" ?>" method="post" action="">
                                        <td><?php echo "$row[0]" ?>
                                        </td>
                                        <td> <?php echo $row[1] ?> </td>
                                        <td>
                                            <?php
                                            if ($row[0] == "000") {

                                                include ("../inc_web/verifica.php");
                                                echo"correo errores: $email_error <br/>";
                                                echo"correo notificaciones: $email_env";
                                            } else {
                                                ?>

                                                <input name="correo_error" type="text"  class="form-control"  id="correo_error" value="<?php echo "$row[2]" ?>" maxlength="200" />
                                            </td>

                                        <?php } ?>
                                        <td> <?php
                                            if ($row[0] == "000") {
                                                echo"&nbsp;";
                                            } else {
                                                ?> <input name="id_provincia_mody" type="hidden" id="id_provincia_mody" value="<?php echo "$row[0]" ?>" /> <input name="modifika_correo" type="submit"  class="btn btn-primary btn-xs" id="modifika_correo" value="<?= _("Modificar") ?>" /> <?php } ?></td>
                                    </form>     </tr>



                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                                </tbody>
                            </table>
                            <?php
                        }
                        ?>



                        <!--Final-->
                        <p>&nbsp;</p>
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>
