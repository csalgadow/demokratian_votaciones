<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 1;
include('../inc_web/nivel_acceso.php');
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->



                    <h1><?= _("Asambleas y Grupos de trabajo existentes") ?></h1>




                    <?php
                    if ($_SESSION['usuario_nivel'] == 0) {

                        $sql = "SELECT ID ,	tipo ,	subgrupo ,	id_provincia, 	id_ccaa, tipo,acceso FROM $tbn4 ORDER BY 'id_provincia' ";
                        $result = mysqli_query($con, $sql);

                        if ($row = mysqli_fetch_array($result)) {
                            ?>


                            <table id="tabla1" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="40%"><?= _("Titulo") ?></th>
                                        <th width="30%">&nbsp;</th>
                                        <th width="30%">&nbsp;</th>
                                        <th width="10%"><?= _("modificar") ?></th>
                                        <th width="10%"><?= _("borrar") ?></th>


                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    mysqli_field_seek($result, 0);

                                    do {
                                        ?>
                                        <tr>
                                            <td><?php echo "$row[2]" ?></td>
                                            <td><?php
                                                if ($row[5] == 2) {
                                                    $optiones2 = mysqli_query($con, "SELECT  ccaa FROM $tbn3 where ID=$row[4]");
                                                    $row_prov2 = mysqli_fetch_row($optiones2);
                                                    echo _("CCAA") . " -" . $row_prov2[0];
                                                } else if ($row[5] == 3) {
                                                    echo _("Estatal");
                                                } else {
                                                    $optiones2 = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$row[3]");
                                                    $row_prov2 = mysqli_fetch_row($optiones2);
                                                    echo _("Provincial") . " -" . $row_prov2[0];
                                                }
                                                ?></td>

                                            <td><?php
                                                if ($row[6] == 3) {

                                                    echo _("Cerrado");
                                                } else if ($row[6] == 2) {
                                                    echo _("Administrado");
                                                } else {

                                                    echo _("Abierto");
                                                }
                                                ?></td>

                                            <td><a href="asambleas.php?idvas=<?php echo "$row[0]" ?>&acc=modifika"  class="btn btn-primary btn-xs"><?= _("modificar") ?></a></td>
                                            <td><a href="asambleas_borra.php?idvas=<?php echo "$row[0]" ?>" onClick="return borrarevento()" class="btn btn-danger btn-xs"><?= _("borrar") ?></a> </td>
                                        </tr>


                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        } else {
                            echo " <div class=\"alert alert-warning\">" . _("¡No se ha encontrado ningún resultado!") . " </div>";
                        }
                        ?>
                    <?php } ?>

                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script src="../js/admin_borrarevento.js"></script>
        <script type="text/javascript" language="javascript" class="init">

                                                $(document).ready(function () {
                                                    $('#tabla1').dataTable({
                                                        "language": {
                                                            "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                                                            "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                                                            "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                                                            "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                                                            "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                                                            "loadingRecords": "<?= _("Cargando") ?>...",
                                                                                            "processing": "<?= _("Procesando") ?>...",
                                                                                            "search": "<?= _("Buscar") ?>:",
                                                                                            "paginate": {
                                                                                                "first": "<?= _("Primero") ?>",
                                                                                                "last": "<?= _("Ultimo") ?>",
                                                                                                "next": "<?= _("Siguiente") ?>",
                                                                                                "previous": "<?= _("Anterior") ?>"
                                                                                            },
                                                                                            "aria": {
                                                                                                "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                                                                "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                                                            }
                                                                                        },
                                                                                        "order": [0, "desc"],
                                                                                        "iDisplayLength": 50
                                                                                    });
                                                                                });
        </script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#apuntarme').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });
        </script>
    </body>
</html>
