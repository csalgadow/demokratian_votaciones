<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->


        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1><?= _("Votantes existentes con las caracteristicas seleccionadas") ?></h1><br/>
                    <?php
                    $id_provincia = fn_filtro($con, $_POST['provincia']);
                    $nombre_usuario = fn_filtro($con, $_POST['nombre_usuario']);
                    $correo_usuario = fn_filtro($con, $_POST['correo_electronico']);
                    $nif = fn_filtro($con, $_POST['nif']);
                    $tipo_votante = fn_filtro($con, $_POST['tipo_usuario']);
                    //$usuario = fn_filtro($con, $_POST['usuario']);
                    $nivel_admin = fn_filtro($con, $_POST['nivel_admin']);



                    $sql = "SELECT ID, id_provincia,  nombre_usuario, correo_usuario, nif, tipo_votante, usuario,bloqueo, nivel_usuario,	nivel_acceso,apellido_usuario FROM $tbn9
			WHERE id_provincia like '%$id_provincia%' and nombre_usuario like '%$nombre_usuario%' and correo_usuario like '%$correo_usuario%' and nif like '%$nif%' and tipo_votante like '%$tipo_votante%' and nivel_usuario like '%$nivel_admin%'   ORDER BY 'nombre_usuario' ";



                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>


                        <table id="tabla1" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">&nbsp;</th>
                                    <th width="28%"><?= _("Nombre") ?></th>
                                    <th width="17%"><?= _("Correo") ?></th>
                                    <?php if ($es_municipal == false) { ?>
                                        <th width="11%"><?= _("Provincia") ?></th>
                                    <?php } ?>
                                    <th width="9%"><?= _("Nivel Acceso") ?></th>

                                    <th width="9%"><?= _("Nivel usuario") ?></th>
                                    <th width="11%">&nbsp;</th>
                                    <th width="11%">&nbsp;</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?>
                                    <tr>
                                        <td><a data-toggle="modal"  href="usuario_datos.php?id=<?php echo "$row[0]" ?>" data-target="#apuntarme" title="<?php echo "$row[3]" ?>"><span class="glyphicon glyphicon-user  text-success" aria-hidden="true"></span></a></td>
                                        <td><?php echo "$row[2]" ?></td>
                                        <td><?php echo "$row[3]" ?> </td>
                                        <?php if ($es_municipal == false) { ?>
                                            <td><?php echo "$row[1]" ?></td>
                                        <?php } ?>
                                        <td><?php echo "$row[9]" ?></td>
                                        <td><?php
                                            $enlace_asigna = "";
                                            if ($row[8] == "2") {
                                                echo _("Administrador");
                                                $enlace_asigna = "NO";
                                            } else if ($row[8] == "3") {
                                                echo _("Administrador") . " " . _("CCAA");
                                                // $enlace_asigna="<a href=\"usuarios_asigna.php?id=".$row[0]."\" class=\"fotos\" title=\" ".$row[3]." \" rel=\"gb_page_center[760, 620]\>Asignar grupos</a>";
                                                $enlace_asigna = "NO";
                                            } else if ($row[8] == "4") {
                                                echo _("Administrador") . " " . _("provincia");
                                                $enlace_asigna = _("Asignar provincias o grupos");
                                            } else if ($row[8] == "6") {
                                                echo _("Administrador") . " " . _("Grupos");
                                                if ($es_municipal == false) {
                                                    echo " " . _("estatales");
                                                }
                                                $enlace_asigna1 = _("Asignar grupos");
                                                if ($es_municipal == false) {
                                                    $enlace_asigna2 = " " . _("Estatales");
                                                    $enlace_asigna = $enlace_asigna1 . $enlace_asigna2;
                                                } else {
                                                    $enlace_asigna = $enlace_asigna1;
                                                }
                                            } else if ($row[8] == "5") {
                                                echo _("Administrador") . " " . _("grupos provinciales");
                                                $enlace_asigna = _("Asignar provincias o grupos");
                                            } else if ($row[8] == "7") {
                                                echo _("Administrador") . " " . _("grupos autonomicos");
                                                $enlace_asigna = _("Asignar grupos de trabajo");
                                            } else {
                                                echo _("Votante");
                                                $enlace_asigna = "NO";
                                            }
                                            ?></td>
                                        <td>
                                            <?php
                                            if ($enlace_asigna == "NO") {
                                                echo "--";
                                            } else {
                                                ?>

                                                <a data-toggle="modal"  href="usuarios_asigna.php?id=<?php echo "$row[0]" ?>" data-target="#apuntarme" title="<?php echo $row[3]; ?>" class="btn btn-primary btn-xs" ><?php echo "$enlace_asigna"; ?></a><?php }
                                            ?></td>
                                        <td>

                                            <a href="usuarios_actualizar.php?id=<?php echo "$row[0]" ?>" class="btn btn-primary btn-xs" ><?= _("modificar") ?></a></td>
                                    </tr>

                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                        echo _("¡No se ha encontrado ningún grupo de trabajo!");
                    }
                    ?>


                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script src="../js/admin_borrarevento.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#tabla1').dataTable({
                    "language": {
                        "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                        "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                        "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                        "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                        "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                        "loadingRecords": "<?= _("Cargando") ?>...",
                                                        "processing": "<?= _("Procesando") ?>...",
                                                        "search": "<?= _("Buscar") ?>:",
                                                        "paginate": {
                                                            "first": "<?= _("Primero") ?>",
                                                            "last": "<?= _("Ultimo") ?>",
                                                            "next": "<?= _("Siguiente") ?>",
                                                            "previous": "<?= _("Anterior") ?>"
                                                        },
                                                        "aria": {
                                                            "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                            "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                        }
                                                    },
                                                    "order": [0, "desc"],
                                                    "iDisplayLength": 50
                                                });
                                            });
        </script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#apuntarme').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });
        </script>
    </body>
</html>
