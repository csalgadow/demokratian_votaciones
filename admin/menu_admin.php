<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
if ($_SESSION['nivel_usu'] != "1") {
    ?>

    <div class="navbar-wrapper">
        <div class="container  menu_admin">
            <?php include('../inc_web/version.php'); ?>

            <div class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><?php echo "$nombre_web"; ?></a>
                    </div>


                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav pull-right">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= _("Votaciones") ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="../admin/votacion.php"><?= _("Crear votación") ?></a></li>
                                    <li><a href="../admin/gestion_zonas.php"><?= _("Gestión  de votaciones") ?></a></li>
                                    <li><a href="../admin/gestion_votaciones_mis.php"><?= _("Gestion MIS votaciones") ?> <b class="glyphicon glyphicon-user"></b></a></li>

                                </ul>
                            </li>
                            <!---->


                            <?php if ($_SESSION['usuario_nivel'] <= "2") { ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= _("Censos") ?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../admin/censos_buscador.php"><?= _("Buscar / modificar votantes") ?></a></li>
                                        <li><a href="../admin/censos.php"><?= _("Incluir un votante") ?></a></li>
                                        <li><a href="../admin/censos_add_mas.php"><?= _("Añadir votantes de foma masiva") ?></a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header"><?= _("Bajas -modificaciones") ?></li>
                                        <li><a href="../admin/bloquear_censos_buscador.php"><?= _("Bloquear / desbloquear votantes") ?></a></li>
                                        <?php if ($_SESSION['usuario_nivel'] <= "1") { ?>
                                            <li><a href="../admin/censos_out_mas.php"><?= _("Bajas-Modificar votantes masiva") ?></a></li>
                                        <?php } ?>
                                        <?php if ($es_municipal == false) { ?>
                                            <!-- Opcion obsoleta
                                            <?php if ($_SESSION['usuario_nivel'] <= "1") { ?>
                                                                            <li><a href="../accesorios/censos_out_mas.php">Actualizar municipios de foma masiva</a></li>
                                            <?php } ?>
                                            -->
                                            <?php if ($_SESSION['usuario_nivel'] <= "1") { ?>
                                                <li><a href="../admin/poblaciones_consultar.php"><?= _("Buscar municipios") ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($insti == true) { ?>
                                            <li><a href="../admin/censos_block_insti.php"><?= _("Bloquear correos de forma masiva") ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>

                            <!---->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <?php
                                    $sql_cont = "SELECT a.ID  FROM $tbn9 a,$tbn6 b where (a.ID= b.id_usuario) and b.estado = 0 ";
                                    $result_cont = mysqli_query($con, $sql_cont);

                                    $quants_cont = mysqli_num_rows($result_cont);
                                    if ($quants_cont == 0) {
                                        
                                    } else {
                                        ?>
                                        <span class="badge"> <?php echo "$quants_cont"; ?></span> 
                                    <?php }
                                    ?> <?= _("Grupos trabajo") ?> 

                                    <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <?php if ($_SESSION['usuario_nivel'] <= "2") { ?>
                                        <li><a href="../admin/asambleas.php" class="menu"><?= _("Crear grupos de trabajo") ?></a>
                                        </li><?php } ?>
                                    <?php if ($_SESSION['usuario_nivel'] <= "6") { ?>
                                        <li><a href="../admin/asambleas_list.php" class="menu"><?= _("Modificar grupos de trabajo") ?></a></li><?php } ?>
                                    <li><a href="../admin/mis_grupos_list.php " class="menu"><?= _("Gestionar usuarios") ?> <span class="badge"> <?php echo "$quants_cont"; ?></spam></a> </li>


                                </ul>
                            </li>






                            <!---->

                            <?php if ($_SESSION['usuario_nivel'] <= "2") { ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= _("Gestion administracion") ?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="../admin/usuarios.php"><?= _("Gestion usuarios administracion") ?></a></li>
                                        <li><a href="../admin/usuarios_gestion_mis.php" ><?= _("Votaciones por usuario") ?></a></li>
                                        <li class="divider"></li>
                                            <?php if ($es_municipal == false) { ?>
                                            <li><a href="../admin/gestion_provincias.php"  ><?= _("Gestion notificaciones provincias") ?></a></li>
                                        <?php } ?>
                                        <!-- <li><a href="../admin/paginas_list.php"  >Gestion  paginas</a></li>-->
                                        <?php if ($_SESSION['usuario_nivel'] == "0") { ?>
                                            <li><a href="../admin/constantes.php"  ><?= _("Configuración variables de la web") ?></a></li>
                                            <li><a href="../admin/constantes_correo.php"  ><?= _("Configuración del correo electronico") ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>    
                            <?php } ?>
                            <li> <a data-toggle="modal"  href="https://docs.google.com/document/d/1Odyw5T7WFL82-tuo3Cgau_UzNSL-7LzCrisxAuAihg0/pub" data-target="#ayuda_admin"><?= _("Ayuda") ?></a></li>
                            <!-- <li> <a data-toggle="modal"  href="../admin/ayuda_admin.php" data-target="#ayuda_admin">Ayuda</a></li>-->
                            <li> <a></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php
}?>