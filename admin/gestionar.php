<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

$id = fn_filtro_numerico($con, $_GET['id']);

if (ISSET($_POST["desactivar"])) {

    $activo = "no";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activa=\"$activo\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

if (ISSET($_POST["activar"])) {
    $activo = "si";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activa=\"$activo\" WHERE id=" . $id . "";

    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

//////////////////////////////
if (ISSET($_POST["desactivar_resultados"])) {
    $activar = "no";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activos_resultados=\"$activar\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

if (ISSET($_POST["activar_resultados"])) {
    $activar = "si";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activos_resultados=\"$activar\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h1> <?= _("Panel de gestion de votación") ?> </h1>
                    <?php
                    $result = mysqli_query($con, "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor,demarcacion,fecha_com, fecha_fin,encripta FROM $tbn1 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>
                    <div class="form-group">
                        <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre votación") ?></label>

                        <div class="col-sm-9"> <h3> <?php echo "$row[1]"; ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <?= _("Tipo de votación") ?>
                        </div>
                        <div class="col-sm-9">
                            <?php
                            if ($row[2] == 1) {
                                echo _("primarias");
                            } else if ($row[2] == 2) {
                                echo _("VUT");
                            } else if ($row[2] == 3) {
                                echo _("Encuesta");
                            } else if ($row[2] == 4) {
                                echo _("Debate");
                            }
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <?= _("Fechas") ?>
                        </div>
                        <div class="col-sm-3">

                            <?php
                            echo $fecha_ini_ver = date("d-m-Y  / H:i", strtotime($row[9]));

// $hora_ini_ver=date("H:i", strtotime($row[9]));
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <?php
                            echo $fecha_fin_ver = date("d-m-Y / H:i", strtotime($row[10]));
                            //$hora_fin_ver=date("H:i", strtotime($row[6]));
                            ?>

                        </div>
                        <div class="col-sm-3">
                            <?php
                            $hoy = strtotime(date('Y-m-d H:i'));
                            $fecha_ini = strtotime($row[7]);
                            $fecha_fin = strtotime($row[8]);
                            if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                                ?>
                                <?= _("fuera fecha") ?>
                                <?php
                            } else {
                                ?>
                                <?= _("en fecha") ?>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <!---->
                    <div class="row">
                        <div class="col-sm-3">
                            <?= _("Tipo de votante") ?>
                        </div>
                        <div class="col-sm-9">
                            <?php
                            if ($row[3] == 1) {
                                echo _("solo socio");
                            } else if ($row[3] == 2) {
                                echo _("socio y simpatizante verificado");
                            } else if ($row[3] == 3) {
                                echo _("socio y simpatizante");
                            } else if ($row[3] == 5) {
                                echo _("abierta");
                            }
                            ?>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <!---->
                    <div class="row">
                        <div class="col-sm-3"><?= _("VOTACIÓN") ?></div>
                        <div class="col-sm-3">
                            <a href="votacion.php?id=<?php echo "$row[0]" ?>&acc=modifika" class="btn btn-primary btn-block"><?= _("modificar") ?></a>
                        </div>
                        <div class="col-sm-3">
                            <a href=votacion_borrar.php?id=<?php echo"$row[0]"; ?> class="btn btn-danger btn-block" onClick="return borrarevento()"><span class="glyphicon glyphicon-warning-sign"></span> <?= _("BORRAR") ?></a>
                        </div>
                        <div class="col-sm-3">
                            <form id="form_<?php echo $row[0]; ?>" name="form_<?php echo $row[0]; ?>" method="post" action="">
                                <?php if ($row[4] == "no") { ?>
                                    <input type="submit" name="activar" id="activar" value="<?= _("activar") ?>" class="btn btn-primary btn-block"/>
                                <?php } else { ?>
                                    <input type="submit" name="desactivar" id="desactivar" value="<?= _("desactivar") ?>"  class="btn btn-success btn-block" />
                                <?php } ?>
                                <input name="id" type="hidden" id="id" value="<?php echo $row[0]; ?>" />
                            </form>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <!---->
                    <div class="row">

                        <?php if ($row[2] == 4) { ?>
                            <div class="col-sm-3"><?= _("Preguntas") ?></div>
                            <div class="col-sm-3">
                                <a href="preguntas.php?idvot=<?php echo "$row[0]" ?>" class="btn btn-primary btn-block"><?= _("Añadir preguntas") ?></a>
                            </div>
                        <?php } else { ?>
                            <div class="col-sm-3"><?= _("CANDIDATOS U OPCIONES") ?></div>
                            <div class="col-sm-3">
                                <a href="candidatos.php?idvot=<?php echo "$row[0]" ?>" class="btn btn-primary btn-block" ><?= _("añadir candidatos") ?></a>
                            </div>
                        <?php } ?>

                        <div class="col-sm-3">
                            <?php if ($row[2] == 4) { ?>
                                <a href="preguntas_busq1.php?idvot=<?php echo "$row[0]" ?>"  class="btn btn-primary btn-block "><?= _("Modificar preguntas") ?></a>
                            <?php } else { ?>
                                <a href="candidatos_busq1.php?idvot=<?php echo "$row[0]" ?>" class="btn btn-primary btn-block"><?= _("Gestionar candidatos") ?></a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-3">

                            <?php
                            if ($row[2] == 4) {
                                
                            } else {
                                ?>
                                <?php
                                $result_vot = mysqli_query($con, "SELECT id,activo FROM $tbn22  where id_votacion=$id");
                                $quants = mysqli_num_rows($result_vot);
                                //miramos si hay resultados , si los hay , entonces toca modificar
                                if ($quants == 1) {
                                    $row_c = mysqli_fetch_array($result_vot);
                                    if ($row[1] == 0) {
                                        $estado = _("activo");
                                    } else {
                                        $estado = _("inactivo");
                                    }
                                    ?>
                                    <a href="candidatos_pagina.php?idvot=<?php echo "$row[0]" ?>"  class="btn btn-success btn-block "><?= _("Modificar pagina externa") ?> / <?php echo $estado; ?></a>
                                    <?php
                                } else if ($quants > 1) {
                                    $mensaje_error = _("Hay un error de algun tipo en la base de datos");
                                } else {
                                    ?>
                                    <a href="candidatos_pagina.php?idvot=<?php echo "$row[0]" ?>"  class="btn btn-warning btn-block "><?= _("Candidatos pagina externa") ?></a>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <!---->
                    <p>&nbsp;</p>
                    <!---->

                    <?php if ($row[6] == 3 or $row[6] == 4 or $row[7] == "si") { ?>
                        <div class="row">
                            <div class="col-sm-3"><?= _("INTERVENTORES") ?></div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3">
                                <a href="interventor_busq1.php?idvot=<?php echo"$row[0]"; ?>" class="btn btn-primary btn-block"><?= _("Gestionar interventores") ?></a>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                    <?php } ?>



                    <!---->
                    <?php if ($row[2] != 4) { ?>
                        <div class="row">
                            <div class="col-sm-3"> <?= _("RESULTADOS") ?></div>
                            <div class="col-sm-3">
                                <?php if ($row[2] == 1) { ?>
                                    <a href="../vota_orden/resultados_primarias.php?idvot=<?php echo $row[0]; ?>"  target="_blank " class="btn btn-warning btn-block" onclick="recargar()"><?= _("Lanzar recuento") ?></a>
                                    <?php
                                } else if ($row[2] == 2) {
                                    if ($_SESSION['usuario_nivel'] == 0) {
                                        echo "<a href=\"../vota_vut/lanza_recuento.php?idvot=" . $row[0] . "\"  class=\"btn btn-warning btn-block \" target=\"_blank \">" . _("Realizar recuento") . "</a>";
                                    }
                                }
                                if ($row[2] == 3) {
                                    ?>
                                    <a href="../vota_encuesta/resultados.php?idvot=<?php echo $row[0]; ?>"  target="_blank " class="btn btn-warning btn-block" onclick="recargar()"><?= _("Lanzar recuento") ?></a>
                                    <?php
                                }
                                ?>
                            </div>

                            <div class="col-sm-3">
                                <?php if ($row[2] == 1) { ?>
                                    <a href="../vota_orden/resultados_listar.php?idvot=<?php echo $row[0]; ?>"  target="_blank " class="btn btn-warning btn-block" onclick="recargar()"><?= _("Listar votos") ?></a>
                                    <?php
                                }
                                ?>
                                <?php if ($row[2] == 2) { ?>
                                    <a href="../vota_vut/resultados_listar.php?idvot=<?php echo $row[0]; ?>"  target="_blank " class="btn btn-warning btn-block" onclick="recargar()"><?= _("Listar votos") ?></a>
                                    <?php
                                }
                                ?>
                                <?php if ($row[2] == 3) { ?>
                                    <a href="../vota_encuesta/resultados_listar.php?idvot=<?php echo $row[0]; ?>"  target="_blank " class="btn btn-warning btn-block" onclick="recargar()"><?= _("Listar votos") ?></a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <form id="form_<?php echo $row[0]; ?>" name="form_<?php echo $row[0]; ?>" method="post" action="">

                                    <input name="id" type="hidden" id="id" value="<?php echo $row[0]; ?>" />

                                    <?php
                                    if ($row[2] == 4) {
                                        echo " <h3><span class=\"label label-info btn-block\">" . _("debate") . "<spam></h3>";
                                    } else {

                                        if ($row[5] == "no") {
                                            if ($row[2] == 1 or $row[2] == 3) {
                                                $nombre_fichero1 = $FileRec . $id . "_list.html";
                                                $nombre_fichero2 = $FileRec . $id . ".html";
                                                if (file_exists($nombre_fichero1) and file_exists($nombre_fichero2)) {
                                                    //comprobamos que se pueden activar los resultados porque se han lanzado los recuentos y el listado
                                                    $disable = "";
                                                } else {
                                                    $disable = "disabled=\"disabled\"";
                                                }
                                            } else if ($row[2] == 2) {
                                                $nombre_fichero1 = $FileRec . $id . "_list.html";
                                                if (file_exists($nombre_fichero1)) {
                                                    //comprobamos que se pueden activar los resultados porque se han lanzado los recuentos y el listado
                                                    $disable = "";
                                                } else {
                                                    $disable = "disabled=\"disabled\"";
                                                }
                                            }
                                            ?>
                                            <input type="submit" name="activar_resultados" id="activar_resultados" class="btn btn-primary btn-block" value="<?= _("activar resultados") ?>" <?php echo $disable; ?> />
                                        <?php } else {
                                            ?>
                                            <input type="submit" name="desactivar_resultados" id="desactivar_resultados"class="btn btn-success btn-block" value="<?= _("desactivar resultados") ?>" />

                                            <?php
                                        }
                                    }
                                    ?>
                                </form>
                            </div>

                        </div>
                        <p>&nbsp;</p>
                        <!---->

                        <?php if ($row[2] == 1 or $row[2] == 2) { ?>

                            <?php if ($row[11] == "si") { ?>
                                <div class="row">
                                    <div class="col-sm-3"><?= _("ENCRIPTACIÓN") ?></div>
                                    <div class="col-sm-3">

                                        <?php // if ($row[2] == 1) {  ?>
                                        <?php if ($_SESSION['usuario_nivel'] == "0") { ?>
                                            <a href="decodifica_rsa.php?idvot=<?php echo $row[0]; ?>"   class="btn btn-warning btn-block"><?= _("comprobar rsa") ?></a>
                                        </div>
                                        <div class="col-sm-3">
                                            <a href="codificador_busq1.php?idvot=<?php echo"$row[0]"; ?>" class="btn btn-primary btn-block"><?= _("Gestionar CODIFICADORES") ?></a>
                                            <?php
                                        }
                                        // }
                                        //else if ($row[2] == 2) {
                                        //   if ($_SESSION['usuario_nivel'] == 0) {
                                        //       echo "<br/><a href=\"preguntas_busq1.php?idvot=" . $row[0] . "\"  class=\"btn btn-warning btn-block\">Relanzar recuento</a>";
                                        //   }
                                        // }
                                        ?>
                                    </div>
                                </div>


                                <p>&nbsp;</p>
                                <?php
                            }
                        }
                        ?>

                        <!---->
                        <div class="row">
                            <div class="col-sm-3"><?= _("CENSOS") ?></div>
                            <div class="col-sm-3">
                                <?php if ($row[8] == "1") { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=com&lit=si" class="btn btn-primary btn-block"><?= _("Censo completo") ?></a>
                                <?php } else { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=com&lit=no" class="btn btn-primary btn-block"><?= _("Censo completo") ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">

                                <?php if ($row[8] == "1") { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=fal&lit=si" class="btn btn-primary btn-block  pull-right"><?= _("Faltan") ?></a>
                                <?php } else { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=fal&lit=no" class="btn btn-primary btn-block  pull-right"><?= _("Faltan") ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">

                                <?php if ($row[8] == "1") { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=stn&lit=si" class="btn btn-primary btn-block"><?= _("Ya ha votado") ?></a>
                                <?php } else { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=stn&lit=no" class="btn btn-primary btn-block"><?= _("Ya ha votado") ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <!---->
                        <div class="row">
                            <div class="col-sm-3"> </div>
                            <div class="col-sm-3">

                                <?php if ($row[8] == "1") { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=cong&lit=si" class="btn btn-primary btn-block"><?= _("presencial/congreso") ?></a>
                                <?php } else { ?>
                                    <a href="votantes_listado_multi.php?idvot=<?php echo "$row[0]" ?>&cen=cong&lit=no" class="btn btn-primary btn-block"><?= _("presencial/congreso") ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-3">

                                <a href="participacion.php?idvot=<?php echo "$row[0]" ?>" class= "btn btn-success btn-block "><?= _("participación") ?></a>
                            </div></div>
                    <?php } ?>
                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../js/admin_borravotacion.js"></script>
        <script type="text/javascript">
                                        function recargar() {
                                            // Recargo la página
                                            location.reload(true);
                                        }
                                        ;
        </script>
    </body>
</html>
