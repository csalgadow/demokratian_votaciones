<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require ("../inc_web/verifica.php");

$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

$idcat = fn_filtro_numerico($con, $_GET['idcat']);
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/Jcrop/main.css" rel="stylesheet" type="text/css" /><!-- -->
        <link href="../temas/<?php echo "$tema_web"; ?>/Jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">

                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-7">
                    <a href="candidatos.php?idvot=<?php echo $_GET['idvot']; ?>" class="btn btn-primary pull-right"><?= _("Añadir otra opcion o candidato en esta votación") ?></a>

                    <a href="candidatos_actualizar.php?id=<?php echo "$idcat"; ?>&idvot=<?php echo $_GET['idvot']; ?>" class="btn btn-primary pull-right"><?= _("Modificar esta opcion o candidato") ?></a>
                    <a href="candidatos_busq1.php?idvot=<?php echo "$idvot"; ?>" class="btn btn-primary pull-right"><?= _("Buscar en el directorio para modificar o borrar candiatos en esta encuesta") ?></a>
                    <h3><?= _("MODIFICAR IMAGEN DE LA OPCION O CANDIDATO") ?></h3>


                    <div class="demo">

                        <div class="bbody">

                            <!-- upload form -->
                            <form id="upload_form" enctype="multipart/form-data" method="post" action="candidatos_actualizar.php?id=<?php echo "$idcat"; ?>&idvot=<?php echo $_GET['idvot']; ?>" onsubmit="return checkForm()" class="well">
                                <!-- hidden crop params -->
                                <input type="hidden" id="x1" name="x1" />
                                <input type="hidden" id="y1" name="y1" />
                                <input type="hidden" id="x2" name="x2" />
                                <input type="hidden" id="y2" name="y2" />

                                <h3><?= _("PASO 1: Por favor, seleccione una imagen") ?></h3>
                                <div>
                                    <p>
                                        <input type="file" name="image_file" id="image_file" onchange="fileSelectHandler()" class="btn btn-primary "/>
                                    </p>
                                    <p>&nbsp; </p>
                                </div>

                                <div class="error"></div>

                                <div class="step2">
                                    <p>&nbsp;</p>
                                    <h3><?= _("PASO 2: Seleccione la zona de la imagen que desea recortar para usar como imagen del candidato u opcion") ?></h3>
                                    <img id="preview" />

                                    <div class="info">
                                        <label><?= _("Peso") ?> </label> <input type="text" id="filesize" name="filesize" />
                                        <label><?= _("Tipo") ?></label> <input type="text" id="filetype" name="filetype" />
                                        <label><?= _("Tamaño") ?></label> <input type="text" id="filedim" name="filedim" />
                                        <label><?= _("Alto") ?></label> <input type="text" id="w" name="w" />
                                        <label><?= _("Ancho") ?></label> <input type="text" id="h" name="h" />
                                    </div>

                                    <input name="modifika_imagen" type="submit" class="btn btn-primary pull-right" id="modifika_imagen" value="<?= _("Modifica la imagen") ?>"/>
                                </div>
                            </form>
                        </div>
                    </div>







                </div>

                <!--Final-->
            </div>



        </div>


        <div id="footer" class="row">
            <!--
        ===========================  modal para apuntarse
            -->
            <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-body"></div>

                    </div> <!-- /.modal-content -->
                </div> <!-- /.modal-dialog -->
            </div> <!-- /.modal -->

            <!--
           ===========================  FIN modal apuntarse
            -->
            <?php include("../votacion/ayuda.php"); ?>
            <?php include("../temas/$tema_web/pie.php"); ?>
        </div>
    </div>


 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
    <script src="../js/jquery-1.9.0.min.js"></script>
    <script src="../js/jquery-migrate-1.2.1.js"></script>
    <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
    <script src="../js/jquery.Jcrop.min.js"></script>
    <script src="../js/user_crop.js"></script>
</body>
</html>
