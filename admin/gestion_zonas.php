<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

$lista1 = "";
$lista2 = "";
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h2><?= _("Buscar VOTACIONES para GESTIONAR o MODIFICAR") ?></h2>

                    <?php if ($_SESSION['nivel_usu'] == 2) { ?>

                        <div  class="well">

                            <h4> <a href="gestion_votaciones.php"><?= _("Todas las Votaciones") ?> <?php if ($es_municipal == false) { ?><?= _("Estatales") ?> <?php } else { ?> <?= _("Generales") ?><?php } ?></a></h4>

                        </div>


                        <?php if ($es_municipal == false) { ?>
                            <form id="formulario1" name="formulario1" method="post" action="gestion_votaciones.php"  class="well">

                                <h4> <?= _("Buscar Votaciones por CCAA") ?></h4>

                                <?php
                                $options1 = "select DISTINCT ID, ccaa from $tbn3 order by ID";
                                $resulta1 = mysqli_query($con, $options1) or die("error: " . mysqli_error());

                                while ($listrows1 = mysqli_fetch_array($resulta1)) {
                                    $id_pro1 = $listrows1['ID'];
                                    $name1 = $listrows1['ccaa'];
                                    $lista1 .= "<option value=\"$id_pro1\"> $name1</option>";
                                }
                                ?>
                                <select name="id_ccaa"  id="id_ccaa"  class="form-control">
                                    <?php echo "$lista1"; ?>
                                </select>
                                <input type="submit" name="buscar_ccaa" id="buscar_ccaa" value="<?= _("Buscar") ?>" class="btn btn-primary " />

                            </form>


                            <?php
                        }
                    }
                    if ($es_municipal == false) {
                        if ($_SESSION['nivel_usu'] == 3) {
                            ?>

                            <div  class="well">
                                <a href="<?php echo "$url_gestion_votaciones"; ?>?id=<?php echo $_SESSION['id_ccaa_usu']; ?>">  <?php echo $_SESSION['ccaa']; ?>
                                </a>
                            </div>

                            <?php
                        }

                        if ($_SESSION['nivel_usu'] <= 3) {
                            if ($_SESSION['nivel_usu'] == 2) {
                                $options2 = "select DISTINCT id, provincia from $tbn8 where especial=0 order by ID";
                            } else {
                                $options2 = "select DISTINCT id, provincia from $tbn8 where id_ccaa = " . $_SESSION['id_ccaa_usu'] . " order by ID";
                            }

                            $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());

                            while ($listrows2 = mysqli_fetch_array($resulta2)) {
                                $id_pro2 = $listrows2['id'];
                                $name2 = $listrows2['provincia'];

                                $lista2 .= "<option value=\"$id_pro2\"> $name2</option>";
                                //$lista1 .="    <label><input type=\"checkbox\" name=\"tipo_$id_pro\" value=\"$id_pro\" id=\"tipo_$id_pro\" $chequed /> $name1</label> <br/>";
                            }
                            ?>

                            <form id="form1" name="form1" method="post" action="gestion_votaciones.php"  class="well">

                                <h4><?= _("Buscar Votaciones por provincias") ?></h4>
                                <select name="id_provincia"  class="form-control" id="id_provincia" >
                                    <?php echo "$lista2"; ?>
                                </select>
                                <input type="submit" name="buscar_prov" id="buscar_prov" value="<?= _("Buscar") ?>" class="btn btn-primary "  />

                            </form>


                            <?php
                        }
                        if ($_SESSION['nivel_usu'] == 4) {
                            //// lista las provincias que tienen los administradores provinciales
                            $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                            $quants2 = mysqli_num_rows($result2);

                            if ($quants2 != 0) {

                                while ($listrows2 = mysqli_fetch_array($result2)) {

                                    $name2 = $listrows2[id_provincia];
                                    $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                    $row_prov = mysqli_fetch_row($optiones);

                                    $lista2 .= "<option value=\"$name2\">" . $row_prov[0] . "</option>";
                                }
                                ?>

                                <form id="form1" name="form1" method="post" action="gestion_votaciones.php"  class="well">

                                    <h4><?= _("Buscar Votaciones por provincias") ?></h4>

                                    <select name="id_provincia"  class="form-control" id="id_provincia" >
                                        <?php echo "$lista2"; ?>
                                    </select>
                                    <p><?= _("selecciona municipio") ?> </p>
                                    <select name="municipio" id="municipio" class="form-control" > </select>
                                    <input type="submit" name="buscar_prov" id="buscar_prov" value="<?= _("Buscar") ?>"class="btn btn-primary "  />
                                </form>
                            </div>
                            <?php
                        } else {
                            echo _("No tiene asignadas provincias");
                        }
                    }
                    /**/

                    if ($_SESSION['nivel_usu'] <= 3) {
                        if ($_SESSION['nivel_usu'] == 2) {
                            $options2 = "select DISTINCT id, provincia from $tbn8 where especial=0 order by ID";
                        } else {
                            $options2 = "select DISTINCT id, provincia from $tbn8 where id_ccaa = " . $_SESSION['id_ccaa_usu'] . " order by ID";
                        }

                        $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());

                        while ($listrows2 = mysqli_fetch_array($resulta2)) {
                            $id_pro2 = $listrows2['id'];
                            $name2 = $listrows2['provincia'];

                            $lista2 .= "<option value=\"$id_pro2\"> $name2</option>";
                            //$lista1 .="    <label><input type=\"checkbox\" name=\"tipo_$id_pro\" value=\"$id_pro\" id=\"tipo_$id_pro\" $chequed /> $name1</label> <br/>";
                        }
                        ?>

                        <form id="form1" name="form1" method="post" action="gestion_votaciones.php"  class="well">

                            <h4><?= _("Buscar Votaciones por Municipios") ?></h4>
                            <select name="id_provincia2"  class="form-control" id="id_provincia2" >
                                <?php echo "$lista2"; ?>
                            </select>
                            <p><?= _("selecciona municipio") ?> </p>
                            <select name="municipio" id="municipio" class="form-control" > </select>
                            <input type="submit" name="buscar_municipio" id="buscar_municipio" value="<?= _("Buscar") ?>" class="btn btn-primary "  />

                        </form>


                        <?php
                    }
                    if ($_SESSION['nivel_usu'] == 4) {
                        //// lista las provincias que tienen los administradores provinciales
                        $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                        $quants2 = mysqli_num_rows($result2);

                        if ($quants2 != 0) {

                            while ($listrows2 = mysqli_fetch_array($result2)) {

                                $name2 = $listrows2['id_provincia'];
                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                $row_prov = mysqli_fetch_row($optiones);

                                $lista2 .= "<option value=\"$name2\">" . $row_prov[0] . "</option>";
                            }
                            ?>

                            <form id="form1" name="form1" method="post" action="gestion_votaciones.php"  class="well">

                                <h4><?= _("Buscar Votaciones por municipios") ?></h4>

                                <select name="id_provincia2"  class="form-control" id="id_provincia2" >
                                    <?php echo "$lista2"; ?>
                                </select>

                                <input type="submit" name="buscar_municipio" id="buscar_municipio" value="<?= _("Buscar") ?>"class="btn btn-primary "  />
                            </form>
                        </div>
                        <?php
                    } else {
                        echo _("No tiene asignadas provincias");
                    }
                }

                /* grupos de trabajo */
            }
            if ($_SESSION['nivel_usu'] <= 3) {
                if ($_SESSION['nivel_usu'] == 2) {
                    $options_sub = "select DISTINCT id, subgrupo , tipo,id_provincia,id_ccaa from $tbn4  order by tipo";
                } else {
                    $options_sub = "select DISTINCT id, subgrupo , tipo,id_provincia,id_ccaa from $tbn4 where  id_ccaa=" . $_SESSION['id_ccaa_usu'] . "
  order by tipo";
                }
                $lista_sub = "";
                $resulta_sub = mysqli_query($con, $options_sub) or die("error: " . mysql_error());
                $quantos_gr = mysqli_num_rows($resulta_sub);

                $datos = "";
                while ($listrows_sub = mysqli_fetch_array($resulta_sub)) {
                    $id_sub = $listrows_sub['id'];
                    $name_sub = $listrows_sub['subgrupo'];
                    $id_prov = $listrows_sub['id_provincia'];
                    $id_ccaa = $listrows_sub['id_ccaa'];
                    $id_tipo = $listrows_sub['tipo'];

                    $tipo = $listrows_sub['tipo'];
                    if ($tipo == 2) {
                        $tipos = _("AUTONOMICO");
                        $optiones_ccaa = mysqli_query($con, "SELECT  ccaa FROM $tbn3 where ID=$id_ccaa");
                        $row_ccaa = mysqli_fetch_row($optiones_ccaa);
                        $ccaaprov = $row_ccaa[0];
                    }if ($tipo == 1) {
                        $tipos = _("Provincia de");

                        $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                        $row_prov = mysqli_fetch_row($optiones);
                        $ccaaprov = $row_prov[0];
                    }
                    if ($tipo == 3) {
                        $tipos = _("ESTATAL");
                        $ccaaprov = "";
                    }
                    if ($es_municipal == false) { //// si no es municipal añadimos los datos
                        $datos = "-" . $tipos . " " . $ccaaprov;
                    }
                    $lista_sub .= "   <option value=\"$id_sub\">" . $name_sub . " " . $datos . "</option>";
                }
                ?>



                <form id="form1" name="form1" method="post" action="gestion_votaciones.php"  class="well">
                    <h4> <?= _("Grupos de trabajo o asambleas locales") ?></h4>
                    <select name="id_sub"  class="form-control" id="id_sub" >
                        <?php echo "$lista_sub"; ?>
                    </select>
                    <br/>
                    <input name="buscar_sub" type=submit  id="buscar_sub" value="<?= _("Buscar") ?>" class="btn btn-primary " >                  </td>
                </form>

                <?php
            }

            if ($_SESSION['nivel_usu'] == 4 or $_SESSION['nivel_usu'] == 5 or $_SESSION['nivel_usu'] == 6 or $_SESSION['nivel_usu'] == 7) {
                ?>

                <form id="form1" name="form1" method="post" action="gestion_votaciones.php"  class="well">
                    <h4><?= _("Grupos de trabajo") ?></h4>

                    <?php
                    $result2 = mysqli_query($con, "SELECT a.ID ,a.subgrupo,a.tipo_votante, a.id_provincia, a.tipo FROM $tbn4 a,$tbn6 b where (a.ID= b.id_grupo_trabajo) and b.id_usuario=" . $_SESSION['ID'] . " and b.admin=1 order by a.tipo");
                    $quants2 = mysqli_num_rows($result2);
//$row2=mysql_fetch_row($result2);

                    if ($quants2 != 0) {

                        while ($listrows2 = mysqli_fetch_array($result2)) {
                            $id_grupo = $listrows2['ID'];

                            $id_prov = $listrows2['id_provincia'];
                            $subgrupo = $listrows2['subgrupo'];

                            $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$id_prov");
                            $row_prov = mysqli_fetch_row($optiones);
                            ?>


                            <label>
                                <?php
                                if ($listrows2['tipo'] == 1) {
                                    echo _("Asamblea/grupo provincial");
                                } else if ($listrows2['tipo'] == 2) {
                                    echo _("Asamblea/grupo autonomico");
                                } else if ($listrows2['tipo'] == 3) {
                                    if ($es_municipal == false) {
                                        echo _("Asamblea/grupo estatal");
                                    }
                                }
                                ?>
                                <input  type="radio"  name="id_sub"  value="<?php echo "$id_grupo"; ?>" class="buttons" id="id_sub_<?php echo "$id_grupo"; ?>" > <?php echo $subgrupo; ?>  </label>

                                                                                                                         <!-- <a href="<?php echo "$url_gestion_votaciones"; ?>?id_grupo=<?php echo "$id_grupo"; ?>">  <?php echo "$subgrupo " . $row_prov[0] . ""; ?>
                                                                                                                          </a>--><br/>


                            <?php
                        }
                        ?><br/>
                        <input name="buscar_sub" type="submit" id="buscar_sub" value="<?= _("Buscar") ?>" class="btn btn-primary " />
                        <?php
                    } else {
                        echo _("No tiene asignados Grupos");
                    }
                    ?>
                </form>

            <?php }
            ?>




            <!--Final-->
        </div>



    </div>


    <div id="footer" class="row">
        <!--
    ===========================  modal para apuntarse
        -->
        <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body"></div>

                </div> <!-- /.modal-content -->
            </div> <!-- /.modal-dialog -->
        </div> <!-- /.modal -->

        <!--
       ===========================  FIN modal apuntarse
        -->
        <?php include("../votacion/ayuda.php"); ?>
        <?php include("../temas/$tema_web/pie.php"); ?>
    </div>
</div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
<script src="../js/jquery-1.9.0.min.js"></script>
<script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#id_provincia2').change(function () {

            var id_provincia = $('#id_provincia2').val();
            $('#municipio').load('../basicos_php/genera_select.php?id_provincia=' + id_provincia);
            $("#municipio").html(data);
        });
    });
</script>



<?php if ($_SESSION['nivel_usu'] <= 4) { ?>
    <script type="text/javascript">
        function loadPoblacion() {

            $('#municipio').load('../basicos_php/genera_select.php?id_provincia=1');
            $("#municipio").html(data);
        }


        $(document).ready(function () {
            loadPoblacion();
        });
    </script>

<?php } else { ?>
    <script type="text/javascript">
        function loadPoblacion() {

            $('#municipio').load('../basicos_php/genera_select.php?id_provincia=<?php echo $name2; ?>');
            $("#municipio").html(data);
        }


        $(document).ready(function () {
            loadPoblacion();
        });
    </script>

<?php } ?>


</body>
</html>
