<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

if (ISSET($_POST["desactivar"])) {

    $activo = "no";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activa=\"$activo\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

if (ISSET($_POST["activar"])) {
    $activo = "si";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activa=\"$activo\" WHERE id=" . $id . "";

    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

//////////////////////////////
if (ISSET($_POST["desactivar_resultados"])) {
    $activar = "no";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activos_resultados=\"$activar\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}

if (ISSET($_POST["activar_resultados"])) {
    $activar = "si";
    $id = fn_filtro_numerico($con, $_POST["id"]);
    $sSQL = "UPDATE $tbn1 SET activos_resultados=\"$activar\" WHERE id=" . $id . "";
    mysqli_query($con, $sSQL) or die("Imposible modificar");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->


        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <?php
                    if (ISSET($_POST['id_usurio_vot'])) {
                        $id_usurio_vot = fn_filtro_numerico($con, $_POST['id_usurio_vot']);
                        $url_usurio_vot = "&id_usurio_vot=$id_usurio_vot";
                    } else {
                        $id_usurio_vot = $_SESSION['ID'];
                    }

                    $sql = "SELECT  ID , nombre_votacion,tipo, tipo_votante, activa, activos_resultados,seguridad,interventor ,fecha_com, fecha_fin  FROM $tbn1 where anadido='$id_usurio_vot'   ORDER BY 'ID' ";
                    ?>
                    <h1><?= _("Mis Votaciones") ?></h1><br/>

                    <?php
//$sql = "SELECT * FROM $tbn1 where id_provincia like '%$ids_provincia%' ORDER BY 'id_provincia' ";

                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>


                        <table id="tabla1" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="3%">ID</th>
                                    <th width="10%"><?= _("Fecha") ?></th>
                                    <th width="45%"><?= _("Titulo") ?></th>
                                    <th width="7%"><?= _("Tipo") ?></th>
                                    <th width="10%"><?= _("Tipo votante") ?></th>
                                    <th width="10%" align="center"><?= _("Estado") ?></th>

                                    <th width="15%" align="center"><?= _("Resultados") ?></th>
                                    <th width="10%">&nbsp;</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);
                                do {
                                    ?>
                                    <tr>
                                        <td><?php echo "$row[0]" ?></td>
                                        <td>
                                            <?php
                                            $hoy = strtotime(date('Y-m-d H:i'));
                                            $fecha_ini = strtotime($row[8]);
                                            $fecha_fin = strtotime($row[9]);
                                            if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                                                ?>
                                                <?= _("En fecha") ?>
                                                <?php
                                            } else {
                                                ?>
                                                <?= _("Fuera de fecha") ?>
                                            <?php }
                                            ?>  </td>
                                        <td><?php echo "$row[1]" ?></td>
                                        <td>
                                            <?php
                                            if ($row[2] == 1) {
                                                echo _("primarias");
                                            } else if ($row[2] == 2) {
                                                echo _("VUT");
                                            } else if ($row[2] == 3) {
                                                echo _("Encuesta");
                                            } else if ($row[2] == 4) {
                                                echo _("Debate");
                                            }
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            if ($row[3] == 1) {
                                                echo _("solo socio");
                                            } else if ($row[3] == 2) {
                                                echo _("socio y simpatizante verificado");
                                            } else if ($row[3] == 3) {
                                                echo _("socio y simpatizante");
                                            } else if ($row[3] == 5) {
                                                echo _("abierta");
                                            }
                                            ?>

                                        </td>
                                        <td align="center">
                                            <?php if ($row[4] == "no") { ?>
                                                <?= _("INACTIVO") ?>  <span class="glyphicon glyphicon-ban-circle  text-danger"></span>
                                            <?php } else {
                                                ?>
                                                <?= _("ACTIVO") ?> <span class="glyphicon glyphicon-ok  text-success"></span>
                                            <?php } ?>
                                        </td>
                                        <td align="center">
                                            <?php
                                            if ($row[2] == 4) {
                                                echo " <h4><span class=\"label label-info\">" . _("Debate") . "<spam></h4>";
                                            } else {
                                                if ($row[5] == "no") {
                                                    ?>
                                                    <?= _("RESULTADOS INACTIVOS") ?>  <span class="glyphicon glyphicon-eye-close  text-danger"></span>
                                                <?php } else {
                                                    ?>
                                                    <?= _("RESULTADOS ACTIVOS") ?> <span class="glyphicon glyphicon-eye-open  text-success"></span>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </td>


                                        <td>

                                            <table width="100%" border="0">
                                                <tr>
                                                    <th scope="row">
                                                        <a href="gestionar.php?id=<?php echo "$row[0]" ?>" class="btn btn-primary btn-xs"><?= _("GESTIONAR") ?></a>

                                                    </th>
                                                </tr>


                                            </table>

                                        </td>


                                    </tr>

                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                        echo "<div class=\"alert alert-success\">" . _("¡No se ha encontrado ninguna votación!") . " </div>";
                    }
                    ?>


                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script src="../js/admin_borrarevento.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#tabla1').dataTable({
                    "language": {
                        "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                        "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                        "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                        "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                        "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                        "loadingRecords": "<?= _("Cargando") ?>...",
                                                        "processing": "<?= _("Procesando") ?>...",
                                                        "search": "<?= _("Buscar") ?>:",
                                                        "paginate": {
                                                            "first": "<?= _("Primero") ?>",
                                                            "last": "<?= _("Ultimo") ?>",
                                                            "next": "<?= _("Siguiente") ?>",
                                                            "previous": "<?= _("Anterior") ?>"
                                                        },
                                                        "aria": {
                                                            "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                            "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                        }
                                                    },
                                                    "order": [0, "desc"],
                                                    "iDisplayLength": 50
                                                });
                                            });
        </script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#apuntarme').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });
        </script>
    </body>
</html>
