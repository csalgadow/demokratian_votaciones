<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 6;
include('../inc_web/nivel_acceso.php');



$idvas = fn_filtro_numerico($con, $_GET['idvas']);
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <?php
                    $result = mysqli_query($con, "SELECT * FROM $tbn4 where ID=$idvas");
                    $row = mysqli_fetch_row($result);
                    ?>

                    <h1> <?= _("BORRADA ASAMBLEA O GRUPO DE TRABAJO") ?></h1>

                    <form class="well form-horizontal">
                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9"><?php echo "$row[2]"; ?>

                            </div></div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("TIPO") ?></label>

                            <div class="col-sm-9">
                                <?php
                                if ($row[1] == 1) {

                                    echo _("Provincial") . "  |   $row[4]";
                                } else if ($row[1] == 2) {

                                    echo _("Autonomico") . " |  $row[5]";
                                } else if ($row[1] == 3) {
                                    echo _("Estatal");
                                }
                                ?>
                            </div></div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Forma de acceso al grupo") ?></label>

                            <div class="col-sm-9">
                                <?php
                                if ($row[7] == 1) {
                                    echo _("Abierto (no necesita validación para suscribirse)");
                                } else if ($row[7] == 2) {
                                    _("Administrado (Necesita que los administradores validen el acceso)");
                                } else {

                                    echo _("Cerrado (Solo los administradores añaden usuarios)");
                                }
                                ?>

                            </div></div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Activo") ?>  </label>

                            <div class="col-sm-9">
                                <?php
                                if ($row[8] == 2) {
                                    echo _("No");
                                } else {

                                    echo _("Si");
                                }
                                ?>


                            </div></div>





                        <div class="form-group">
                            <label for="Sexo" class="col-sm-3 control-label"><?= _("TIPO DE VOTANTE") ?></label>
                            <div class="col-sm-9">
                                <?php
                                if ($row[10] == 5) {
                                    echo _("Abierta");
                                } else if ($row[10] == 2) {
                                    echo _("Socios y simpatizantes verificados");
                                } else if ($row[10] == 3) {
                                    echo _("Socios y simpatizantes");
                                } else {
                                    echo _("Solo socios");
                                }
                                ?>

                            </div></div>

                        <div class="form-group">

                            <label for="nombre" class="col-sm-3 control-label" ><?= _("Texto") ?></label>
                            <div class="col-sm-9">

                                <?php echo "$row[6]"; ?>

                            </div></div>
                        <p>&nbsp;</p>

                        <?php
                        $borrado1 = mysqli_query($con, "DELETE FROM $tbn4 WHERE id=" . $idvas . "") or die("No puedo ejecutar la instrucción de borrado SQL query");
                        if (!$borrado1) {
                            echo "<div class=\"alert alert-warning\">" . _("Error en el borrado asamblea o grupo de trabajo") . "</div>";
                        } elseif (mysqli_affected_rows($con) == 0) {
                            echo "<div class=\"alert alert-warning\">" . _("Error en el borrado asamblea o grupo de trabajo") . "</div>";
                        } else {
                            echo "<div class=\"alert alert-success\">" - _("Borrada asamblea o grupo de trabajo") . "</div>";
                        }



                        $borrado2 = mysqli_query($con, "DELETE FROM $tbn6 WHERE id_grupo_trabajo=$idvas") or die("No puedo ejecutar la instrucción de borrado SQL query");

                        if (!$borrado2) {
                            echo "<div class=\"alert alert-warning\">" . _("No se ha podido borrar relacion de usuarios de esta asamblea o grupo de trabajo") . ".</div>";
                        } elseif (mysqli_affected_rows($con) == 0) {
                            echo "<div class=\"alert alert-warning\">" . _("No se ha podido borrar a relacion de usuarios de esta asamblea o grupo de trabajo") . "</div>";
                        } else {
                            echo "<div class=\"alert alert-success\">" . _("Borrada relacion de usuarios de esta asamblea o grupo de trabajo") . "</div>";
                        }


                        $borrado3 = mysqli_query($con, "DELETE FROM $tbn1 WHERE id_grupo_trabajo=$idvas") or die("No puedo ejecutar la instrucción de borrado SQL query");
                        if (!$borrado3) {
                            echo "<div class=\"alert alert-warning\">" . _("No se han podido borrar  votaciones de esta asamblea o grupo de trabajo") . "</div>";
                        } elseif (mysqli_affected_rows($con) == 0) {
                            echo "<div class=\"alert alert-warning\">" . _("No se han podido borrar  votaciones de esta asamblea o grupo de trabajo") . "</div>";
                        } else {
                            echo "<div class=\"alert alert-success\">" . _("Borradas votaciones de esta asamblea o grupo de trabajo") . "</div>";
                        }


                        /* faltaria borrar los candidatos de esta votacion y ademas todos los votos y otras cosas relacionadas */
                        ?>
                    </form>



                </div>
            </div>




            <!--Final-->
        </div>



    </div>


    <div id="footer" class="row">
        <!--
    ===========================  modal para apuntarse
        -->
        <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body"></div>

                </div> <!-- /.modal-content -->
            </div> <!-- /.modal-dialog -->
        </div> <!-- /.modal -->

        <!--
       ===========================  FIN modal apuntarse
        -->
        <?php include("../votacion/ayuda.php"); ?>
        <?php include("../temas/$tema_web/pie.php"); ?>
    </div>
</div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
<script src="../js/jquery-1.9.0.min.js"></script>
<script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<script src="../modulos/ui/jquery-ui.custom.js"></script>
<script src="../js/jqBootstrapValidation.js"></script>
<script type='text/javascript' src='../js/admin_funciones.js'></script>


</body>
</html>
