<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 1;
include('../inc_web/nivel_acceso.php');

include("../basicos_php/modifika_config.php");

$file = "../config/config.inc.php";

if (ISSET($_POST["modifika_general"])) {
    $com_string = "email_env = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_error"])) {
    $com_string = "email_error = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_control"])) {
    $com_string = "email_control = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tecnico"])) {
    $com_string = "email_error_tecnico = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_sistema"])) {
    $com_string = "email_sistema = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_nombre"])) {
    $com_string = "nombre_sistema = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_asunto"])) {
    $com_string = "asunto = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mens_error"])) {
    $com_string = "asunto_mens_error = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_user_mail"])) {
    $com_string = "user_mail = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_host_smtp"])) {
    $com_string = "host_smtp = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_pass_mail"])) {
    $com_string = "pass_mail = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_puerto_mail"])) {
    $com_string = "puerto_mail = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_sendmail"])) {
    $com_string = "mail_sendmail = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_SMTPSecure"])) {
    $com_string = "mail_SMTPSecure = ";
    if ($_POST['valor'] == "false") {
        $dato_viejo = fn_filtro_nodb($_POST['valor']);
        $nuevo_dato = "\"" . fn_filtro_nodb($_POST['direccion_general']) . "\"";
    }
    if ($_POST['valor'] == "SSL" or $_POST['valor'] == "TLS") {
        if ($_POST['direccion_general'] == "false") {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = fn_filtro_nodb($_POST['direccion_general']);
        } else {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = "\"" . fn_filtro_nodb($_POST['direccion_general']) . "\"";
        }
    }
    $find = $com_string . $dato_viejo;
    $replace = $com_string . $nuevo_dato;
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_SMTPAuth"])) {
    $com_string = "mail_SMTPAuth = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_IsHTML"])) {
    $com_string = "mail_IsHTML = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

include('../config/config.inc.php');
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">
                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">



                    <!--Comiezo-->
                    <h2><?= _("Constantes de configuración") ?> </h2>
                    <table width="100%" border="0"  class="table table-striped">

                        <tr>
                            <th colspan="4" scope="row"><h5><?= _("Configuración del servidor de correo") ?> </h5></th>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Tipo de envio de correo") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($correo_smtp = true) {
                                    echo " SMTP";
                                } else {
                                    echo "Mail";
                                }
                                ?>
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Usuario de correo") ?> </th>
                            <td>&nbsp;</td>
                            <td><?php echo $user_mail; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general"  value="<?php echo $user_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $user_mail; ?>" >
                                    <input type="submit" name="modifika_user_mail" id="modifika_user_mail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> <?= _("Host del correo") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $host_smtp; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general"  value="<?php echo $host_smtp; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $host_smtp; ?>" >
                                    <input type="submit" name="modifika_host_smtp" id="modifika_host_smtp" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Contraseña") ?></th>
                            <td>&nbsp;</td>
                            <td>*******</td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general"  type="password"  autofocus required class="form-control" id="direccion_general"  value="<?php echo $pass_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $pass_mail; ?>" >
                                    <input type="submit" name="modifika_pass_mail" id="modifika_pass_mail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Puerto del servidor") ?></th>
                            <td>&nbsp;</td>
                            <td><p>&nbsp;</p>
                                <p><?php echo $puerto_mail; ?></p></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $puerto_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $puerto_mail; ?>" >
                                    <input type="submit" name="modifika_puerto_mail" id="modifika_puerto_mail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label">SMTPSecure</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_SMTPSecure == false) {
                                    echo _("Deshabilitado");
                                    $check1 = "checked=\"CHECKED\"";
                                } else if ($mail_SMTPSecure == "TLS") {
                                    echo"TLS";
                                    $check2 = "checked=\"CHECKED\"";
                                } else if ($mail_SMTPSecure == "SSL") {
                                    echo"SSL";
                                    $check3 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check1)) {
                                            echo $check1;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_2" value="TLS" <?php
                                        if (isset($check2)) {
                                            echo $check2;
                                        }
                                        ?>>
                                        TLS</label>
                                    <label>
                                        <input name="direccion_general" type="radio"  id="direccion_general_1" value="SSL" <?php
                                        if (isset($check3)) {
                                            echo $check3;
                                        }
                                        ?>>
                                        SSL</label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_SMTPSecure == false) {
                                        echo "false";
                                    } else if ($mail_SMTPSecure == "TLS") {
                                        echo "TLS";
                                    } else if ($mail_SMTPSecure == "SSL") {
                                        echo"SSL";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_SMTPSecure" id="modifika_SMTPSecure" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label"><?= _("Autentificación por SMTP") ?> (SMTPAuth)</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_SMTPAuth == true) {
                                    echo _("Habilitado");
                                    $check4 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check5 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_2" value="true" <?php
                                        if (isset($check4)) {
                                            echo $check4;
                                        }
                                        ?>>
                                               <?= _("Habilitado") ?>
                                    </label>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_3" value="false" <?php
                                        if (isset($check5)) {
                                            echo $check5;
                                        }
                                        ?>>
                                               <?= _("Deshabilitado") ?>
                                    </label>
                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_SMTPAuth == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_mail_SMTPAuth" id="modifika_mail_SMTPAuth" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label"><?= _("Envio html o texto plano") ?>(IsHTML)</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_IsHTML == true) {
                                    echo _("Habilitado");
                                    $check6 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check7 = "checked=\"CHECKED\"";
                                }
                                ?>
                            </td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_4" value="true" <?php
                                        if (isset($check6)) {
                                            echo $check6;
                                        }
                                        ?>>
                                        <?= _("Habilitado") ?></label>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_5" value="false" <?php
                                        if (isset($check7)) {
                                            echo $check7;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>
                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_IsHTML == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_mail_IsHTML" id="modifika_mail_IsHTML" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Forma de envio de correo") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_sendmail == true) {
                                    echo "IsSendMail";
                                    $check8 = "checked=\"CHECKED\"";
                                } else {
                                    echo"IsSMTP";
                                    $check9 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_6" value="false" <?php
                                        if (isset($check9)) {
                                            echo $check9;
                                        }
                                        ?>>
                                        IsSMTP</label>
                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_7" value="true" <?php
                                        if (isset($check8)) {
                                            echo $check8;
                                        }
                                        ?>>
                                        IsSendMail</label>
                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_sendmail == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_mail_sendmail" id="modifika_mail_sendmail" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th colspan="4" scope="row"><a data-toggle="modal"  href="../admin/comprueba_correo.php" data-target="#ayuda_contacta"  class="btn btn-success pull-right"><?= _("Comprobar configuración de correo") ?></a><br/><p></p><h6 class="pull-right"><?= _("Se enviara a") ?>: <?php echo $email_env; ?> </h6> </th>
                        </tr>
                        <tr>
                            <th colspan="4" scope="row"><h5><?= _("Otros datos del correo") ?> </h5></th>
                        </tr>
                        <tr>
                            <th width="39%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%">&nbsp;</td>
                            <td width="33%">&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Direccion de correo general") ?></th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%"><?php echo $email_env; ?></td>
                            <td width="33%">
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_env; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_env; ?>" >
                                    <input type="submit" name="modifika_general" id="modifika_general" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Sitio al que enviamos los correos de los que tienen problemas y no estan en la bbdd, este correo es el usado si no hay datos en la bbdd de los contactos por provincias") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_error; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_error; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_error; ?>" >
                                    <input type="submit" name="modifika_error" id="modifika_error" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Direccion que envia el correo para el control con interventores") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_control; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_control; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_control; ?>" >
                                    <input type="submit" name="modifika_control" id="modifika_control" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Correo electronico del responsable tecnico") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_error_tecnico; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_error_tecnico; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_error_tecnico; ?>" >
                                    <input type="submit" name="modifika_tecnico" id="modifika_tecnico" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Correo electronico del sistema, demomento incluido en el envio de errores de la bbdd") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_sistema; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="<?= _("Direccion de correo") ?>" value="<?php echo $email_sistema; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_sistema; ?>" >
                                    <input type="submit" name="modifika_sistema" id="modifika_sistema" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <th scope="row"> <?= _("Nombre del sistema cuando se envia el correo de recupercion de clave") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $nombre_sistema; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $nombre_sistema; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $nombre_sistema; ?>" >
                                    <input type="submit" name="modifika_nombre" id="modifika_nombre" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Asunto del correo para recuperar la contraseña") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $asunto; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $asunto; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $asunto; ?>" >
                                    <input type="submit" name="modifika_asunto" id="modifika_asunto" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Asunto del mensaje de correo cuando hay problemas de acceso") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $asunto_mens_error; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $asunto_mens_error; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $asunto_mens_error; ?>" >
                                    <input type="submit" name="modifika_mens_error" id="modifika_mens_error" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

                    </table>


                    <!--Final-->


                </div>
            </div>


            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#ayuda_contacta').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });

        </script>
    </body>
</html>
