<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 2;
include('../inc_web/nivel_acceso.php');

$acc = "";
$row[0] = "";
$row[1] = "";
$row[2] = "";
$row[3] = "";
$row[4] = "";
$row[5] = "";
$row[6] = "";
$row[7] = "";
$row[8] = "";
$row[9] = "";
$row[10] = "";
$row[11] = "";
$row[12] = "";

$fecha_ver = date("d-m-Y ");
$fecha = date("Y-m-d H:i:s");

if (isset($_GET['id'])) {
    $id = fn_filtro_numerico($con, $_GET['id']);
    $acc = fn_filtro_nodb($_GET['acc']);
}

if (ISSET($_POST["modifika_votante"])) {



    if (empty($_POST['correo'])) {
        $error = "error";
        $inmsg = "<div class=\"alert alert-danger\">" . _("El e-mail del usuario es un dato requerido") . "</div>";
    } elseif (!filter_var($_POST['correo'], FILTER_VALIDATE_EMAIL)) {
        $error = "error";
        $inmsg = "<div class=\"alert alert-danger\">" . _("la direccion es erronea") . "<div>";
    } else {


        $id_provincia = fn_filtro_numerico($con, $_POST['provincia']);
        $nombre = fn_filtro($con, $_POST['nombre']);
        $apellidos = fn_filtro($con, $_POST['apellidos']);
        $correo = strip_tags($_POST['correo']);
        $nif = strip_tags($_POST['nif']);
        $tipo_votante = fn_filtro_numerico($con, $_POST['tipo_usuario']);
        $bloqueado = fn_filtro($con, $_POST['bloqueado']);
        $razon_bloqueo = fn_filtro($con, $_POST['razon_bloqueo']);
        $id_municipio = fn_filtro($con, $_POST['municipio']);

////miramos la provincia y cogemos el codigo de ccaa
        $optiones = "select DISTINCT id_ccaa from $tbn8 where ID ='$id_provincia' ";
        $resultas = mysqli_query($con, $optiones) or die("error: " . mysql_error($con));

        while ($listrowes = mysqli_fetch_array($resultas)) {
            $id_ccaa = $listrowes['id_ccaa'];
        }


        $sSQL = "UPDATE $tbn9 SET nombre_usuario=\"$nombre\", apellido_usuario=\"$apellidos\", id_provincia=\"$id_provincia\",  correo_usuario=\"$correo\" ,nif=\"$nif\",  tipo_votante=\"$tipo_votante\"  ,id_ccaa=\"$id_ccaa\", bloqueo=\"$bloqueado\", razon_bloqueo=\"$razon_bloqueo\" ,id_ccaa=\"$id_ccaa\" ,id_municipio=\"$id_municipio\"  WHERE id='$id'";

        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

        /* metemos un control para saber quien ha modificado este votante */
        $accion = "2"; //dos , modifiicar votante
        $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"$id\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
        $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>" . _("Imposible añadir. Cambie los datos e intentelo de nuevo") . ".</font></strong>");

        $inmsg = " <div class=\"alert alert-success\">" . _("modificado") . "  <strong> " . $nombre . " " . $apellidos . "</strong> " . _("a la base de datos") . " </div>";
    }
}

if (ISSET($_POST["add_votante"])) {





    if (empty($_POST['correo'])) {
        $error = "error";
        $inmsg = "<div class=\"alert alert-danger\">" . _("El e-mail del usuario es un dato requerido") . "</div>";
    } elseif (!filter_var($_POST['correo'], FILTER_VALIDATE_EMAIL)) {
        $error = "error";
        $inmsg = "<div class=\"alert alert-danger\">" . _("la direccion es erronea") . "</div>";
    } else {

        $id_provincia = $_POST['provincia'];

        $nombre = fn_filtro($con, $_POST['nombre']);
        $apellidos = fn_filtro($con, $_POST['apellidos']);
        $correo = fn_filtro($con, $_POST['correo']);
        $nif = fn_filtro($con, $_POST['nif']);
        $tipo_votante = fn_filtro($con, $_POST['tipo_usuario']);
        $id_municipio = fn_filtro($con, $_POST['municipio']);

        $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn9 WHERE nif='$nif' or correo_usuario='$correo' ") or die(mysql_error($con));

        $total_encontrados = mysqli_num_rows($usuarios_consulta);

        mysqli_free_result($usuarios_consulta);



        if ($total_encontrados != 0) {

            $inmsg = "<div class=\"alert alert-danger\"> ¡¡¡Error!!! <br>" . _("El Usuario") . " <strong>" . $nombre . " </strong> " . _("con correo") . " <strong>" . $correo . "</strong>  " . _("y nif") . " <strong>" . $nif . "</strong> " . _("ya está registrado") . ".</div>";
        } else {
            $optiones = "select DISTINCT id_ccaa from $tbn8 where ID ='$id_provincia' ";
            $resultas = mysqli_query($con, $optiones) or die("error: " . mysql_error($con));

            while ($listrowes = mysqli_fetch_array($resultas)) {
                $id_ccaa = $listrowes['id_ccaa'];
            }


            $insql = "insert into $tbn9 (nombre_usuario, apellido_usuario, id_provincia, 	correo_usuario, nif,tipo_votante,id_ccaa,id_municipio ) values (  \"$nombre\",\"$apellidos\", \"$id_provincia\", \"$correo\", \"$nif\", \"$tipo_votante\", \"$id_ccaa\", \"$id_municipio\" )";
            $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>" . _("Imposible añadir. Cambie los datos e intentelo de nuevo") . ".</font></strong>");
            $inmsg = " <div class=\"alert alert-success\">" . _("Añadido votante") . " <strong> " . $nombre . "</strong> " . _("a la base de datos") . "</div> ";
            $idvot = mysqli_insert_id($con);
            $accion = "1"; //uno , incluir nuevo votante
            /* metemos un control para saber quien ha incluido este votante */
            $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"$idvot\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
            $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
        }
    }
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../modulos/themes-jquery-iu/base/jquery.ui.all.css">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>
                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1> <?php
                        if ($acc != "modifika") {
                            echo _("AÑADIR UN NUEVO AFILIADO") . " / " . _("SIMPATIZANTE");
                        } else {
                            echo _("MODIFICAR ESTE AFILIADO") . " / " . _("SIMPATIZANTE");

                            $result = mysqli_query($con, "SELECT ID, id_provincia, nombre_usuario, apellido_usuario, nivel_usuario, nivel_acceso,	correo_usuario, nif, id_ccaa, pass, tipo_votante ,usuario, bloqueo, razon_bloqueo,id_municipio  FROM $tbn9 where id=$id");
                            $row = mysqli_fetch_row($result);
                        }
                        ?></h1>
                    <p>&nbsp;</p>

                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>

                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post name="frmDatos" id="frmDatos"  class="well form-horizontal" >

                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Nombre") ?></label>

                            <div class="col-sm-9">

                                <input name="nombre" type="text" id="nombre" value="<?php echo "$row[2]"; ?>" class="form-control" placeholder="<?= _("Nombre") ?>" required autofocus data-validation-required-message="<?= _("El nombre  es un dato requerido") ?>">
                            </div></div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Apellidos") ?></label>

                            <div class="col-sm-9">

                                <input name="apellidos" type="text" id="apellidos" value="<?php echo "$row[3]"; ?>" class="form-control" placeholder="<?= _("Apellidos") ?>" required autofocus data-validation-required-message="<?= _("El nombre  es un dato requerido") ?>">
                            </div></div>
                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Correo electronico") ?> </label>

                            <div class="col-sm-9">
                                <div class="controls">
                                    <input name="correo" type="email"  id="correo" value="<?php echo "$row[6]"; ?>"  class="form-control" placeholder="<?= _("Correo electronico") ?>" required  data-validation-required-message="<?= _("Por favor, ponga un correo electronico") ?>" />
                                    <p class="help-block"></p>
                                </div>
                            </div></div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"> <?= _("Nif") ?> </label>

                            <div class="col-sm-4">
                                <input name="nif" type="text" id="nif" value="<?php echo "$row[7]"; ?>" class="form-control" placeholder="<?= _("Nif") ?>" required autofocus data-validation-required-message="<?= _("El NIF  es un dato requerido") ?>" />
                            </div></div>

                        <?php if ($es_municipal == false) { ?>
                            <div class="form-group">
                                <label for="nombre" class="col-sm-3 control-label"><?= _("Provincia") ?>: </label>

                                <div class="col-sm-4">

                                    <?php
                                    $lista = "";


                                    $options = "select DISTINCT id, provincia from $tbn8  order by ID";
                                    $resulta = mysqli_query($con, $options) or die("error: " . mysql_error($con));

                                    while ($listrows = mysqli_fetch_array($resulta)) {
                                        $id_pro = $listrows['id'];
                                        $name1 = $listrows['provincia'];

                                        if ($id_pro == $row[1]) {
                                            $check = "selected=\"selected\" ";
                                        } else {
                                            $check = "";
                                        }
                                        $lista .= "<option value=\"$id_pro\" $check> $name1</option>";
                                    }
                                    ?>

                                    <select name="provincia" class="form-control"  id="provincia" >
                                        <?php echo "$lista"; ?>
                                    </select>



                                </div></div>
                            <div class="form-group">
                                <label for="nombre" class="col-sm-3 control-label"><?= _("Municipio") ?>: </label>

                                <div class="col-sm-4">
                                    <select name="municipio" id="municipio" class="form-control" > </select>
                                </div></div>

                        <?php } else { ?>
                            <input name="provincia" type="hidden" id="provincia" value="001">
                            <input name="municipio" type="hidden" id="municipio" value="1">
                        <?php } ?>


                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label"><?= _("Tipo") ?> </label>

                            <div class="col-sm-9">

                                <?php
                                if ($row[10] == 1) {
                                    $chekeado1 = "checked=\"checked\" ";
                                } else if ($row[10] == 2) {
                                    $chekeado2 = "checked=\"checked\" ";
                                } else {
                                    $chekeado3 = "checked=\"checked\" ";
                                }
                                ;
                                ?>
                                <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" <?php
                                if (isset($chekeado1)) {
                                    echo "$chekeado1";
                                }
                                ?> />
                                <?= _("socio") ?> <br/>

                                <input type="radio" name="tipo_usuario" value="2" id="tipo_usuario_1"  <?php
                                if (isset($chekeado2)) {
                                    echo "$chekeado2";
                                }
                                ?> />
                                <?= _("simpatizante verificado") ?><br/>

                                <input type="radio" name="tipo_usuario" value="3" id="tipo_usuario_2"  <?php
                                if (isset($chekeado3)) {
                                    echo "$chekeado3";
                                }
                                ?> />
                                       <?= _("simpatizante") ?>
                            </div></div>


                        <?php if ($acc == "modifika") { ?>
                            <div class="form-group">
                                <label for="nombre" class="col-sm-3 control-label"><?= _("Bloqueado") ?> </label>

                                <div class="col-sm-9">


                                    <?php
                                    if ($row[12] == "si") {
                                        $chekeado3 = "checked=\"checked\" ";
                                    } else {
                                        $chekeado4 = "checked=\"checked\" ";
                                    }
                                    ;
                                    ?>
                                    <input name="bloqueado" type="radio" id="bloqueado_0" value="si"  <?php
                                    if (isset($chekeado3)) {
                                        echo "$chekeado3";
                                    }
                                    ?> />
                                    <?= _("SI") ?><br/>
                                    <input type="radio" name="bloqueado" value="no" id="bloqueado_1"  <?php
                                    if (isset($chekeado4)) {
                                        echo "$chekeado4";
                                    }
                                    ?> />
                                           <?= _("NO") ?>
                                </div>
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-3 control-label"><?= _("Razón Bloqueo") ?> </label>

                                    <div class="col-sm-9">
                                        <textarea name="razon_bloqueo"  class="form-control"  id="razon_bloqueo"><?php echo "$row[13]"; ?></textarea>
                                    </div></div> <?php } ?>


                            <input name="incluido" type="hidden" id="incluido" value="<?php echo"$nombre_usuario"; ?>">
                            <input name="fecha" type="hidden" id="fecha" value="<?php echo"$fecha"; ?>" />
                            </td>

                            <?php if ($acc == "modifika") { ?>
                                <input name="modifika_votante" type=submit  class="btn btn-primary pull-right"  id="modifika_votante" value="<?= _("ACTUALIZAR  socio o simpatizante") ?>" />
                            <?php } else { ?>
                                <input name="add_votante" type=submit class="btn btn-primary pull-right"  id="add_votanteo" value="<?= _("AÑADIR  socio o simpatizante") ?>" />
                            <?php } ?>

                            <p>&nbsp;</p>

                    </form>

                    <p>&nbsp;</p>



                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/ui/jquery-ui.custom.js"></script>
        <script src="../js/jqBootstrapValidation.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#provincia').change(function () {

                    var id_provincia = $('#provincia').val();
                    $('#municipio').load('../basicos_php/genera_select.php?id_provincia=' + id_provincia);
                    $("#municipio").html(data);
                });
            });
        </script>
        <?php if ($acc == "modifika") { ?>
            <script type="text/javascript">
                function loadPoblacion() {
                    //Funcion para cargar poblacion si estamos modificando
                    //		 $("#wall").load('wall.php?idgr=<?php echo $row[1]; ?>');
                    $('#municipio').load('../basicos_php/genera_select.php?id_provincia=<?php echo $row[1]; ?>&id_municipio=<?php echo $row[14]; ?>');
                    $("#municipio").html(data);
                }


                $(document).ready(function () {
                    loadPoblacion();
                });
            </script>
        <?php } else { ?>
            <script type="text/javascript">
                function loadPoblacion() {
                    //Funcion para cargar poblacion si estamos modificando
                    //		 $("#wall").load('wall.php?idgr=<?php echo $row[1]; ?>');
                    $('#municipio').load('../basicos_php/genera_select.php?id_provincia=1');
                    $("#municipio").html(data);
                }


                $(document).ready(function () {
                    loadPoblacion();
                });
            </script>
        <?php } ?>
    </body>
</html>
