<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 1;
include('../inc_web/nivel_acceso.php');

include("../basicos_php/modifika_config.php");

$file = "../config/config.inc.php";


if (ISSET($_POST["modifika_url"])) {
    $com_string = "url_vot = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_nombre"])) {
    $com_string = "nombre_sistema = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}


if (ISSET($_POST["modifika_web"])) {
    $com_string = "nombre_web = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tema"])) {
    $com_string = "tema_web = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}



if (ISSET($_POST["modifika_auth"])) {
    $com_string = "cfg_autenticacion_solo_local = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_versiones"])) {
    $com_string = "info_versiones = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_zonaHoraria"])) {
    $com_string = "timezone = ";
    if ($_POST['valor'] == "false" and $_POST['direccion_general'] == "true") {
        $dato_viejo = fn_filtro_nodb($_POST['valor']);
        $nuevo_dato = "\"" . fn_filtro_nodb($_POST['timezone']) . "\"";
    } else {
        if ($_POST['direccion_general'] == "false") {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = fn_filtro_nodb($_POST['direccion_general']);
        } else {
            $dato_viejo = "\"" . fn_filtro_nodb($_POST['valor']) . "\"";
            $nuevo_dato = "\"" . fn_filtro_nodb($_POST['timezone']) . "\"";
        }
    }
    $find = $com_string . $dato_viejo;
    $replace = $com_string . $nuevo_dato;
    find_replace($find, $replace, $file, $case_insensitive = true);
}


if (ISSET($_POST["modifika_locale"])) {
    $com_string = "defaul_lang = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}


if (ISSET($_POST["modifika_recaptcha"])) {
    $com_string = "reCaptcha = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_site_key"])) {
    $com_string = "reCAPTCHA_site_key = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_secret_key"])) {
    $com_string = "reCAPTCHA_secret_key = \"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tiempo_session"])) {
    $com_string = "tiempo_session = ";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

include('../config/config.inc.php');
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">
                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">



                    <!--Comiezo-->
                    <h2><?= _("Constantes de configuración") ?> </h2>
                    <table width="100%" border="0"  class="table table-striped">
                        <tr>
                            <th width="39%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%">&nbsp;</td>
                            <td width="33%">&nbsp;</td>
                        </tr>

                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Url de la web de la votación,(ojo  sin barra final)") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_vot; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_vot; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_vot; ?>" >
                                    <input type="submit" name="modifika_url" id="modifika_url" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Nombre del sitio web") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $nombre_web; ?></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $nombre_web; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $nombre_web; ?>" >
                                    <input type="submit" name="modifika_web" id="modifika_web" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> <?= _("Nombre del tema (carpeta donde se encuentra)") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $tema_web; ?></td>
                            <td><form name="form1" method="post" action="">

                                    <?php
                                    $carpeta = "../temas"; //ruta actual
                                    $lista = "";
                                    if (is_dir($carpeta)) {
                                        if ($dir = opendir($carpeta)) {
                                            while (($archivo = readdir($dir)) !== false) {
                                                if ($archivo != '.' && $archivo != '..' && $archivo != '.htaccess' && $archivo != 'index.html' && $archivo != 'index.php') {

                                                    if ($archivo == $tema_web) {
                                                        $check = "selected=\"selected\" ";
                                                    } else {
                                                        $check = "";
                                                    }
                                                    $lista .= "<option value=\"" . $archivo . "\" $check > " . $archivo . "</option>";
                                                }
                                            }
                                            closedir($dir);
                                        }
                                    }
                                    ?>
                                    <select name="direccion_general" class="form-control"  id="direccion_general" >
                                        <?php echo "$lista"; ?>
                                    </select>


                                    <input name="valor" type="hidden" id="valor" value="<?php echo $tema_web; ?>" >
                                    <input type="submit" name="modifika_tema" id="modifika_tema" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>



                            </td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Autenfificación federada") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($cfg_autenticacion_solo_local == true) {
                                    echo "Solo Local";
                                    $check3 = "checked=\"CHECKED\"";
                                } else {
                                    echo"Autentificación federada";
                                    $check4 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check4)) {
                                            echo $check4;
                                        }
                                        ?>>
                                        <?= _("Autentificación federada") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check3)) {
                                            echo $check3;
                                        }
                                        ?>>
                                        <?= _("Solo Local") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($cfg_autenticacion_solo_local == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_auth" id="modifika_auth" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><a name="avisos"></a><?= _("Avisos de actualizaciones") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($info_versiones == true) {
                                    echo _("Habilitado");
                                    $check15 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check16 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check16)) {
                                            echo $check16;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check15)) {
                                            echo $check15;
                                        }
                                        ?>>
                                        <?= _("Habilitado") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($info_versiones == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_versiones" id="modifika_versiones" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Zona horaria") ?></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($timezone == false) {
                                    echo _("Hora del servidor OK");
                                    $check17 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Zona horaria modificada a:") . " " . $timezone;
                                    $check18 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check17)) {
                                            echo $check17;
                                        }
                                        ?>>
                                        <?= _("Usar hora del servidor") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check18)) {
                                            echo $check18;
                                        }
                                        ?>>
                                        <?= _("Usar otra zona horaria") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($timezone == false) {
                                        echo "false";
                                    } else {
                                        echo $timezone;
                                    }
                                    ?>" >


                                    <?php
                                    $timeZONE = $timezone; // cambiamos el nombre de la variable ya que la usamos tambien en el bucle
                                    $regions = array(
                                        'Europa' => DateTimeZone::EUROPE,
                                        'America' => DateTimeZone::AMERICA,
                                        'Africa' => DateTimeZone::AFRICA,
                                        'Antartica' => DateTimeZone::ANTARCTICA,
                                        'Asia' => DateTimeZone::ASIA,
                                        'Atlantica' => DateTimeZone::ATLANTIC,
                                        'Indian' => DateTimeZone::INDIAN,
                                        'Pacific' => DateTimeZone::PACIFIC
                                    );
                                    $timezones = array();
                                    foreach ($regions as $name => $mask) {
                                        $zones = DateTimeZone::listIdentifiers($mask);
                                        foreach ($zones as $timezone) {
                                            // Lets sample the time there right now
                                            $time = new DateTime(NULL, new DateTimeZone($timezone));
                                            // Us dumb Americans can't handle millitary time
                                            $ampm = $time->format('H') > 12 ? ' (' . $time->format('g:i a') . ')' : '';
                                            // Remove region name and add a sample time
                                            $timezones[$name][$timezone] = substr($timezone, strlen($name) + 1) . ' - ' . $time->format('H:i') . $ampm;
                                        }
                                    }
// View
                                    echo '<select id="timezone" name="timezone" class="form-control"> ';
                                    foreach ($timezones as $region => $list) {
                                        print '<optgroup label="' . $region . '">' . "\n";
                                        foreach ($list as $timezone => $name) {
                                            if ($timeZONE == false) {
                                                if ($timezone == "Europe/Madrid") {
                                                    $checked = "selected";
                                                } else {
                                                    $checked = "";
                                                }
                                            } else {
                                                if ($timezone == $timeZONE) {
                                                    $checked = "selected";
                                                } else {
                                                    $checked = "";
                                                }
                                            }
                                            echo '<option value="' . $timezone . '" ' . $checked . '>' . $name . '</option>' . "\n";
                                        }
                                        echo '<optgroup>' . "\n";
                                    }
                                    echo '</select>';
                                    ?>

                                    <input type="submit" name="modifika_zonaHoraria" id="modifika_zonaHoraria" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Idioma por defecto de la web") ?></th>
                            <td>&nbsp;</td>
                            <td><?php echo $defaul_lang; ?></td>
                            <td>
                                <form name="form1" method="post" action="">

                                    <?php
                                    $carpeta_locale = "../locale"; //ruta actual
                                    $lista_locale = "";
                                    if (is_dir($carpeta_locale)) {
                                        if ($dir_locale = opendir($carpeta_locale)) {
                                            while (($archivo_locale = readdir($dir_locale)) !== false) {
                                                if ($archivo_locale != '.' && $archivo_locale != '..' && $archivo_locale != '.htaccess' && $archivo_locale != 'index.html' && $archivo_locale != 'index.php' && $archivo_locale != 'messages.po' && $archivo_locale != 'messages.pot') {

                                                    if ($archivo_locale == $defaul_lang) {
                                                        $check_locale = "selected=\"selected\" ";
                                                    } else {
                                                        $check_locale = "";
                                                    }
                                                    $lista_locale .= "<option value=\"" . $archivo_locale . "\" $check_locale > " . $archivo_locale . "</option>";
                                                }
                                            }
                                            closedir($dir_locale);
                                        }
                                    }
                                    ?>
                                    <select name="direccion_general" class="form-control"  id="direccion_general" >
                                        <?php echo "$lista_locale"; ?>

                                    </select>


                                    <input name="valor" type="hidden" id="valor" value="<?php echo $defaul_lang; ?>" >
                                    <input type="submit" name="modifika_locale" id="modifika_locale" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?= _("Tiempo de caducidad de la sesión") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo $tiempo_session; ?>
                            </td>
                            <td>
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="tiempo_session"  value="<?php echo $tiempo_session; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $tiempo_session; ?>" >
                                    <input type="submit" name="modifika_tiempo_session" id="modifika_tiempo_session" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                    <p> en segundos</p>
                                </form>
                            </td>
                        </tr>
                        
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>
                                &nbsp; 
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Configuración recaptcha") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <a href="https://www.google.com/recaptcha/intro/v3.html" target="_blank"><?= _("Mas información sobre") ?> recaptcha</a>

                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        

                        <tr>
                            <th scope="row"><?= _("Activar") ?> recaptcha</th>
                            <td>&nbsp;</td>
                            <td>
                                <?php
                                if ($reCaptcha == true) {
                                    echo _("Habilitado");
                                    $check18 = "checked=\"CHECKED\"";
                                } else {
                                    echo _("Deshabilitado");
                                    $check17 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action="">
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php
                                        if (isset($check17)) {
                                            echo $check17;
                                        }
                                        ?>>
                                        <?= _("Deshabilitado") ?></label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php
                                        if (isset($check18)) {
                                            echo $check18;
                                        }
                                        ?>>
                                        <?= _("Habilitado") ?></label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($reCaptcha == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_recaptcha" id="modifika_recaptcha" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>


                        <tr>
                            <th scope="row"><?= _("Clave publica") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo $reCAPTCHA_site_key; ?>
                            </td>
                            <td>
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $reCAPTCHA_site_key; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $reCAPTCHA_site_key; ?>" >
                                    <input type="submit" name="modifika_site_key" id="modifika_site_key" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row"><?= _("Clave privada") ?></th>
                            <td>&nbsp;</td>
                            <td>
                                <?php echo $reCAPTCHA_secret_key; ?>
                            </td>
                            <td>
                                <form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $reCAPTCHA_secret_key; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $reCAPTCHA_secret_key; ?>" >
                                    <input type="submit" name="modifika_secret_key" id="modifika_secret_key" value="<?= _("modificar") ?>" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>



                    </table>


                    <!--Final-->

                    <p>&nbsp;</p>
                </div>
            </div>


            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#ayuda_contacta').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });

        </script>
    </body>
</html>
