<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');
include('../basicos_php/url.php');

$idvot = fn_filtro($con, $_GET['idvot']);
if (isset($_GET['est'])) {
    $est = fn_filtro($con, $_GET['est']);
} else {
    $est = "";
}
$fecha = date("Y-m-d h:i:s");
$tam = 250; //tamaño en Kb
$size = $tam * 1024; // tamaño maximo de los archivos en bits
$situacion = "";

if (ISSET($_POST["add_pagina"])) {


    $estado = fn_filtro($con, $_POST['estado']);
    $presentacion = fn_filtro($con, $_POST['presentacion']);
    $cabecera = fn_filtro($con, $_POST['cabecera']);
    $aux = fn_filtro($con, $_POST['aux1']);
    $diseno = fn_filtro($con, $_POST['diseno']);

    $grabar_bbdd = 0;


    if ($_FILES['fileToUpload']['name']) {  ////miramos si hay archivo a subir
        $imagen_cab = basename($_FILES["fileToUpload"]["name"]);
        $tamano = $_FILES ['fileToUpload']['size'];
        $target_file = $upload_cat . "/" . $imagen_cab;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Check si existe el archivo
        if (file_exists($target_file)) {
            $msg = $msg . "<div class=\"alert alert-warning\">" . _("Un archivo con ese nombre ya existe") . "</div>";
            $uploadOk = 0;
        }

        //miramos el tipo de archivo si es de imagen
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $msg = $msg . " <div class=\"alert alert-warning\">" . _("Tu archivo tiene que ser JPG o GIF. Otros archivos no son permitidos") . "</div>";
            $uploadOk = 0;
            $imagen_cab = "";
        }

        if ($uploadOk == 1) {
            $ha_subido = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
            $codigo_error = $_FILES['fileToUpload']['error'];
            if ($tamano > $size) {
                unlink($target_file);
                $msg .= "<div class=\"alert alert-warning\">" . _("Error, La imagen es demasiado grande, superior a") . ": " . $tam . "KB.";
            } else {
                $msg = "<div class=\"alert alert-success\"> " . _("La imagen") . " " . $imagen_cab . " " . _("ha sido subida satisfactoriamente") . " </div>";
                $grabar_bbdd = 1; //´
            }

            if ($ha_subido == false) {
                $msg .= "<div class=\"alert alert-warning\">" . _("ERRROR!! Hay un error inesperado al subir su archivo, codigo") . ": " . $codigo_error . "  </div>";
                $grabar_bbdd = 0;
            }
        } else {
            $grabar_bbdd = 0;
        }
    } else {
        $grabar_bbdd = 0;
    }

    if ($grabar_bbdd == 1) {


        $insql = "insert into $tbn22 (id_votacion, activo, presentacion,cabecera,imagen_cab,aux,diseno) values ( \"$idvot\", \"$estado\",  \"$presentacion\", \"$cabecera\", \"$imagen_cab\", \"$aux\", \"$diseno\")";
        $mens = _("Error al añadir una pagina de candidatos externa");
        $result = db_query($con, $insql, $mens);

        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\">" . _("Hay un error al incluir los datos en la BBDD") . "</div>";
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Añadida la pagina externa  a la base de datos") . " </div>";
        }
    } else {  // si no se ha subido la imagen, añadimos los datos pero sin nombre de imagen
        $insql = "insert into $tbn22 (id_votacion,activo, presentacion, cabecera, aux, diseno) values ( \"$idvot\", \"$estado\",  \"$presentacion\", \"$cabecera\",  \"$aux\", \"$diseno\")";
        $mens = _("Error al añadir una pagina de candidatos externa");

        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\">" . _("Hay un error al incluir los datos en la BBDD") . "</div>";
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Añadida la pagina externa  a la base de datos") . " </div>";
        }
    }
}

if (ISSET($_POST["modifika_pagina"])) {
    $id_pag = fn_filtro($con, $_POST['id_pag']);
    $estado = fn_filtro($con, $_POST['estado']);
    $presentacion = fn_filtro($con, $_POST['presentacion']);
    $cabecera = fn_filtro($con, $_POST['cabecera']);
    $aux = fn_filtro($con, $_POST['aux1']);
    $diseno = fn_filtro($con, $_POST['diseno']);

    $sube_img = 0;


    if ($_FILES['fileToUpload']['name']) {  ////miramos si hay archivo a subir
        $imagen_cab = basename($_FILES["fileToUpload"]["name"]);
        $tamano = $_FILES ['fileToUpload']['size'];
        $target_file = $upload_cat . "/" . $imagen_cab;
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $msg .= "<div class=\"alert alert-warning\">" . _("Perdone, solo estan permitidos archivos con extension JPG, JPEG, PNG & GIF") . " .";
            $uploadOk = 0;
        }


        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $msg .= "<br/>" . _("su archivo no ha sido incluido") . ".</div>";
            $sube_img = 0;
            // if everything is ok, try to upload file
        } else {
            /// si todo esta bien subimos la nueva imagen pero primero miramos si existe una en la base de datos una en esta votacion
            $uploadOk2 = 1;
            $result_img = mysqli_query($con, "SELECT imagen_cab FROM $tbn22  where id_votacion=$idvot");
            $row_img = mysqli_fetch_array($result_img);
            if ($row_img[0] != "") {
                $borra_img = $upload_cat . "/" . $row_img[0];
                unlink($borra_img);
                $borradoOk = 1;
            }
            //miramos si existe un fichero con el mismo nombre
            if (file_exists($target_file)) {
                $msg .= "<div class=\"alert alert-warning\">" . _("Perdone, ya existe un archivo con ese nombre") . " </br> " . _("su archivo no ha sido incluido") . "</div>.";

                if ($borradoOk == 1) {//si hemos borrado una imagen, pero no hemos podido subir la imagen porque existe otra, actualizamos la bbdd y dejamos el campo vacio)
                    $imagen_cab = "";
                    $sube_img = 1; //
                    $uploadOk2 = 0;
                }
                $uploadOk2 = 0;
            }

            if ($uploadOk2 == 1) { // si esta todo correcto, subimos el fichero
                $ha_subido = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
                $codigo_error = $_FILES['fileToUpload']['error'];
                if ($tamano > $size) {
                    unlink($target_file);
                    $msg .= "<div class=\"alert alert-warning\">" . _("Error, La imagen es demasiado grande, superior a") . ": " . $tam . "KB.";
                    $imagen_cab = "";
                    $sube_img = 1; //
                } else {
                    $msg = "<div class=\"alert alert-success\"> " . _("La imagen") . " " . $imagen_cab . " " . _("ha sido subida satisfactoriamente") . " </div>";
                    $sube_img = 1; //´
                }

                if ($ha_subido == false) {
                    $msg .= "<div class=\"alert alert-warning\">" . _("ERRROR!! Hay un error inesperado al subir su archivo, codigo") . ": " . $codigo_error . "  </div>";
                    $sube_img = 0;
                }
            }
        }
    } else {
        //$grabar_bbdd=1;
    }

    if ($sube_img == 1) {
        $sSQL = "UPDATE $tbn22 SET activo=\"$estado\",presentacion=\"$presentacion\",  cabecera=\"$cabecera\" ,imagen_cab=\"$imagen_cab\" ,aux=\"$aux\",diseno=\"$diseno\" WHERE id=$id_pag";
        $mens = "Error al añadir una pagina de candidatos externa";
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = _("Hay un error") . " " . $idvot;
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Realizadas las Modificaciones") . " </div>";
        }
    } else {
        $sSQL = "UPDATE $tbn22 SET activo=\"$estado\",presentacion=\"$presentacion\",  cabecera=\"$cabecera\"  ,aux=\"$aux\",diseno=\"$diseno\" WHERE id=$id_pag";
        $mens = _("Error al añadir una pagina de candidatos externa");
        $result = db_query($con, $sSQL, $mens);

        if (!$result) {
            $inmsg = "<div class=\"alert alert-warning\"> " . _("Hay un error al actualizar la base de datos") . " </div>";
        } else {
            $inmsg = "<div class=\"alert alert-success\"> " . _("Realizadas las Modificaciones") . " </div>";
        }
    }
}


$result_vot = mysqli_query($con, "SELECT id,activo,presentacion, cabecera, imagen_cab,aux,diseno  FROM $tbn22  where id_votacion=$idvot");
//$row_vot = mysqli_fetch_row($result_vot);
$quants = mysqli_num_rows($result_vot);
//miramos si hay resultados y el usuario pude votar en este tipo de votaion y si esta inscrito
if ($quants == 1) {
    $row = mysqli_fetch_array($result_vot);
    $situacion = "modifica";
    $est = "modca";
} else if ($quants > 1) {
    $mensaje_error = _("Hay un error de algun tipo en la base de datos");
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <p>&nbsp;</p>
                    <?php
                    $result_nom = mysqli_query($con, "SELECT nombre_votacion FROM $tbn1 where id=$idvot");
                    $row_nom = mysqli_fetch_row($result_nom);
                    ?>
                    <h1><?php if ($est == "modca") { ?><?= _("MODIFICAR") ?> <?php } else { ?><?= _("GENERAR") ?> <?php } ?> <?= _("PAGINA EXTERNA DE CANDIDATOS U OPCIONES") ?> </h1>
                    <h2><?php echo $row_nom[0]; ?></h2>
                    <p>&nbsp;</p>


                    <?php
                    if (isset($inmsg)) {
                        echo "$inmsg";
                    }
                    ?>
                    <?php
                    if (isset($msg)) {
                        echo "$msg";
                    }
                    ?>
                    <form action="candidatos_pagina.php?idvot=<?php echo $idvot; ?>&est=modca" enctype="multipart/form-data" method=post class="well form-horizontal" id="form1">

                        <?php if ($est == "modca") { ?>
                            <div class="form-group">
                                <label for="presentacion" class="col-sm-3 control-label"><?= _("Enlace externo a la pagina") ?></label>
                                <div class="col-sm-9">
                                    <a href="<?php echo "$url_vot"; ?>/consulta/consulta.php?d=<?php echo "$idvot"; ?>&title=<?php echo urls_amigables($row_nom[0]); ?>" target="_blank"> <?php echo "$url_vot"; ?>/consulta/consulta.php?d=<?php echo "$idvot"; ?>&title=<?php echo urls_amigables($row_nom[0]); ?></a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php
                        $chekeado1 = "checked=\"checked\"";
                        $chekeado2 = "";
                        if ($est == "modca") {
                            if ($row[1] == 0) {
                                $chekeado1 = "checked=\"checked\" ";
                            } else {
                                $chekeado2 = "checked=\"checked\" ";
                            }
                        }
                        ?>
                        <div class="form-group">
                            <label for="presentacion" class="col-sm-3 control-label"><?= _("Estado") ?></label>
                            <div class="col-sm-9">
                                <label>
                                    <input name="estado" type="radio" id="estado_1" value="0"  <?php echo "$chekeado1"; ?>/>
                                    <?= _("Activo") ?></label><span class="label label-warning"></span> <br/>
                                <label>
                                    <input name="estado" type="radio" id="estado_2" value="1"  <?php echo "$chekeado2"; ?>/>
                                    <?= _("Inactivo") ?></label>

                            </div>
                        </div>

                        <?php
                        $chekeado_pres1 = "checked=\"checked\"";
                        $chekeado_pres2 = "";
                        $chekeado_pres3 = "";
                        $chekeado_pres4 = "";
                        $chekeado_pres5 = "";
                        $chekeado_pres6 = "";
                        if ($est == "modca") {
                            if ($row[2] == 1) {
                                $chekeado_pres1 = "checked=\"checked\" ";
                            } else if ($row[2] == 2) {
                                $chekeado_pres2 = "checked=\"checked\" ";
                            } else if ($row[2] == 4) {
                                $chekeado_pres4 = "checked=\"checked\" ";
                            } else if ($row[2] == 5) {
                                $chekeado_pres5 = "checked=\"checked\" ";
                            } else if ($row[2] == 6) {
                                $chekeado_pres6 = "checked=\"checked\" ";
                            } else {
                                $chekeado_pres3 = "checked=\"checked\" ";
                            }
                        }
                        ?>
                        <div class="form-group">
                            <label for="presentacion" class="col-sm-3 control-label"><?= _("Presentación") ?></label>

                            <div class="col-sm-9"><label>
                                    <input name="presentacion" type="radio" id="presentacion_2" value="1"  <?php echo "$chekeado_pres1"; ?> />
                                    <?= _("Presentacion") ?> 1</label>
                                <br/>
                                <label>

                                    <input name="presentacion" type="radio" id="presentacion_0" value="2"  <?php echo "$chekeado_pres2"; ?> />
                                    <?= _("Presentacion") ?> 2</label>
                                <br />
                                <label>
                                    <input name="presentacion" type="radio" id="presentacion_1" value="3"  <?php echo "$chekeado_pres3"; ?> />
                                    <?= _("Presentacion") ?> 3</label>
                                <br />
                                <label>
                                    <input name="presentacion" type="radio" id="presentacion_3" value="4"  <?php echo "$chekeado_pres4"; ?> />
                                    <?= _("Presentacion") ?> 4</label>
                                <br />
                                <label>
                                    <input name="presentacion" type="radio" id="presentacion_4" value="5"  <?php echo "$chekeado_pres5"; ?> />
                                    <?= _("Presentacion") ?> 5</label>
                                <br />
                                <label>
                                    <input name="presentacion" type="radio" id="presentacion_5" value="6"  <?php echo "$chekeado_pres6"; ?> />
                                    <?= _("Presentacion") ?> 6</label>
                                <br />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="presentacion" class="col-sm-3 control-label"><?= _("Numero de columnas") ?></label>
                            <div class="col-sm-9">

                                <?php
                                $chekeado_col1 = "";
                                $chekeado_col2 = "";
                                $chekeado_col3 = "selected=\"selected\"";
                                $chekeado_col4 = "";
                                $chekeado_col6 = "";
                                $chekeado_col12 = "";
                                if ($est == "modca") {
                                    if ($row[5] == 1) {
                                        $chekeado_col1 = "selected=\"selected\" ";
                                    } else if ($row[5] == 2) {
                                        $chekeado_col2 = "selected=\"selected\" ";
                                    } else if ($row[5] == 3) {
                                        $chekeado_col3 = "selected=\"selected\" ";
                                    } else if ($row[5] == 6) {
                                        $chekeado_col6 = "selected=\"selected\" ";
                                    } else if ($row[5] == 12) {
                                        $chekeado_col12 = "selected=\"selected\" ";
                                    } else {
                                        $chekeado_col4 = "selected=\"selected\" ";
                                    }
                                }
                                ?>


                                <select name="aux1" id="aux1">
                                    <option value="1" <?php echo "$chekeado_col1"; ?>>1</option>
                                    <option value="2" <?php echo "$chekeado_col2"; ?>>2</option>
                                    <option value="3" <?php echo "$chekeado_col3"; ?>>3</option>
                                    <option value="4" <?php echo "$chekeado_col4"; ?>>4</option>
                                    <option value="6" <?php echo "$chekeado_col6"; ?>>6</option>
                                    <option value="12" <?php echo "$chekeado_col12"; ?>>12</option>
                                </select>
                            </div>
                        </div>

                        <?php
                        $chekeado_cab1 = "checked=\"checked\" ";
                        $chekeado_cab2 = "";
                        if ($est == "modca") {
                            if ($row[3] == 1) {
                                $chekeado_cab2 = "checked=\"checked\" ";
                            } else {
                                $chekeado_cab1 = "checked=\"checked\" ";
                            }
                        }
                        ?>
                        <div class="form-group">
                            <label for="cabecera" class="col-sm-3 control-label"><?= _("Cabecera de la pagina") ?></label>

                            <div class="col-sm-9"><label>
                                    <input name="cabecera" type="radio" id="cabecera_0" value="0"  <?php echo "$chekeado_cab1"; ?> />
                                    <?= _("Cabecera de la web") ?></label><span class="label label-warning"></span>
                                <br/>
                                <label>

                                    <input name="cabecera" type="radio" id="cabecera_1" value="1"  <?php echo "$chekeado_cab2"; ?> />
                                    <?= _("No usar cabecera") ?></label>
                                <br />
                                <label>
                                    <input name="cabecera" type="radio" id="cabecera_2" value="2" />
                                    <?= _("Cabecera personalizada") ?></label><br />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cabecera perso" class="col-sm-3 control-label"><?= _("Subir archivo para cabecera pesonalizada") ?></label>

                            <div class="col-sm-9"><label>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="300000000">
                                    <input type="file" name="fileToUpload" id="fileToUpload">

                                    <span id="image"></span><br>
                                </label> <br />

                            </div>
                        </div>
                        <?php
                        if ($est == "modca") {
                            if ($row[4] != "") {
                                ?>

                                <div class="form-group">
                                    <label for="cabecera" class="col-sm-3 control-label"><?= _("Cabecera de la pagina") ?></label>

                                    <div class="col-sm-9">
                                        <img src="<?php echo $upload_cat; ?>/<?php echo $row[4]; ?>" class="img-responsive" alt="<?php echo $nombre_votacion; ?>">
                                    </div>
                                </div>


                                <?php
                            }
                        }
                        ?>

                        <div class="form-group">
                            <label for="css" class="col-sm-3 control-label"><?= _("Incluir CSS personalizado") ?></label>
                            <div class="col-sm-9">
                                <textarea name="diseno" id="diseno" class="form-control" rows="8"><?php
                                    if ($est == "modca") {
                                        echo $row[6];
                                    }
                                    ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">

                                <p>&nbsp;</p>

                                <input name="fecha" type="hidden" id="fecha" value="<?php echo"$fecha"; ?>" />
                                <?php if ($est == "modca") { ?><input name="id_pag" type="hidden" id="id_pag" value="<?php echo $row[0]; ?>"><?php } ?>

                                <?php if ($situacion == "") { ?>
                                    <input name="add_pagina" type=submit  class="btn btn-primary pull-right" id="add_pagina" value="<?= _("GENERAR PAGINA EXTERNA") ?>">
                                <?php } else { ?>
                                    <input name="modifika_pagina" type=submit  class="btn btn-primary pull-right" id="modifika_pagina" value="<?= _("MODIFICAR PAGINA EXTERNA") ?>">
                                <?php } ?>
                                </form>




                            </div>
                        </div>




                        <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

                <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#ayuda_contacta').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });
        </script>
    </body>
</html>
