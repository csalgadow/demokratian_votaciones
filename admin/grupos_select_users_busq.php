<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

if (empty($_POST['idgr'])) {
    echo "esta accedeiendo de forma incorrecta o hay algun problema";
    exit();
} else {
    $idgr = fn_filtro($con, $_POST['idgr']);
}
$id_provincia = fn_filtro($con, $_POST['provincia']);
$nombre_usuario = fn_filtro($con, $_POST['nombre_usuario']);
$correo_usuario = fn_filtro($con, $_POST['correo_electronico']);
$nif = fn_filtro($con, $_POST['nif']);
if (isset($_POST['tipo_votante'])) {
    $tipo_votante = fn_filtro($con, $_POST['tipo_votante']);
} else {
    $tipo_votante = "";
}


////////////////bloqueo multible/////////////////////////
if (isset($_POST["add_multi"])) {
    unset($_POST['tabla1_length']);

    foreach ($_POST as $k => $v)
        $a[] = $v;
    $datos = count($a) - 7;
    $i = 0;

    while ($i < $datos) {
        $val = fn_filtro($con, $a[$i]);

        $estado = 1; // como lo incluimos nostros, el estado es que puede acceder

        $sSQL = "insert into $tbn6 (id_usuario,id_grupo_trabajo,estado) values ( \" $val \",  \"  $idgr \",\"$estado\")";



        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

        $i++;
        $modifi = _("modificados usuarios");
    }
}

$sql = "select ID, id_provincia,nombre_usuario,correo_usuario,nif, tipo_votante
from $tbn9
where id not in (
    select id_usuario
    from $tbn6
    where id_grupo_trabajo=" . $idgr . "
)";

/* $sql = "SELECT a.ID, a.id_provincia,  a.nombre_usuario, a.correo_usuario,a.nif, a.tipo_votante  FROM $tbn9 a, $tbn6 b WHERE (a.id = b.id_usuario), a.id_provincia like '%$id_provincia%' and a.nombre_usuario like '%$nombre_usuario%' and a.correo_usuario like '%$correo_usuario%' and a.nif like '%$nif%' and a.tipo_votante like '%$tipo_votante%' and b.id_grupo_trabajo not in '$idgr' group by a.id ORDER BY 'a.nombre_usuario' "; */
$result = mysqli_query($con, $sql);
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1><?= _("CENSO") ?> </h1>
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <form name="formulario" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
                        <?php
                        if ($row = mysqli_fetch_array($result)) {
                            ?>

                            <table id="tabla1" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width=10%>ID</th>
                                        <th width=20%><?= _("Nombre") ?></th>
                                        <th width=20%><?= _("Correo") ?></th>
                                        <th width=10%><?= _("Tipo") ?></th>
                                        <th width=10%><?= _("Nif") ?></th>
                                        <?php if ($es_municipal == false) { ?>
                                            <th width=10%><?= _("provincia") ?></th>
                                        <?php } ?>


                                        <th width=3% align=center><?= _("Incluir") ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    mysqli_field_seek($result, 0);

//echo "</tr> \n";
//$var_bol=true;
                                    do {
                                        ?>
                                        <tr>
                                            <td><?php echo "$row[0]" ?></td>
                                            <td><?php echo "$row[2]" ?></td>
                                            <td><?php echo "$row[3]" ?></td>
                                            <td><?php echo "$row[5]" ?></td>
                                            <td><?php echo "$row[4]" ?></td>
                                            <?php if ($es_municipal == false) { ?>
                                                <td><?php echo "$row[1]" ?></td>
                                            <?php } ?>

                                            <td>
                                                <input name="borrar_multiples<?php echo "$row[0]" ?>" type="checkbox" id="borrar_multiples" value="<?php echo "$row[0]" ?>">

                                            </td>
                                        </tr>
                                        <?php
                                    } while ($row = mysqli_fetch_array($result));
                                    ?>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width=10%><?= _("Incluido") ?></th>
                                        <th width=20%><?= _("Nombre") ?></th>
                                        <th width=20%><?= _("Correo") ?></th>
                                        <th width=10%><?= _("Tipo") ?></th>
                                        <th width=10%><?= _("Nif") ?></th>
                                        <?php if ($es_municipal == false) { ?>
                                            <th width=10%><?= _("provincia") ?></th>
                                        <?php } ?>


                                        <th width=3% align=center><?= _("Incluir") ?></th>
                                    </tr>
                                </tfoot>

                            </table><p>&nbsp;</p>

                            <input name="provincia" type="hidden" value="<?php echo "$id_provincia"; ?>">
                            <input name="nombre_usuario" type="hidden" value="<?php echo "$nombre_usuario"; ?>">
                            <input name="correo_electronico" type="hidden" value="<?php echo "$correo_usuario"; ?>">
                            <input name="nif" type="hidden" value="<?php echo "$nif"; ?>">
                            <input name="tipo_votante" type="hidden" value="<?php echo "$tipo_votante"; ?>">

                            <input name="idgr" type="hidden" value="<?php echo $idgr; ?>">

                            <p>&nbsp;</p>

                            <input name="add_multi" type="submit" id="add_multi" value="<?= _("Incluir") ?>" class="btn btn-primary pull-right"  />

                        </form>

                        <?php
                    } else {

                        echo _("¡No se ha encontrado ningún registro!");
                    }
                    ?><!---->

                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script src="../js/admin_borrarevento.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#tabla1').dataTable({
                    "language": {
                        "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                        "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                        "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                        "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                        "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                        "loadingRecords": "<?= _("Cargando") ?>...",
                                                        "processing": "<?= _("Procesando") ?>...",
                                                        "search": "<?= _("Buscar") ?>:",
                                                        "paginate": {
                                                            "first": "<?= _("Primero") ?>",
                                                            "last": "<?= _("Ultimo") ?>",
                                                            "next": "<?= _("Siguiente") ?>",
                                                            "previous": "<?= _("Anterior") ?>"
                                                        },
                                                        "aria": {
                                                            "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                            "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                        }
                                                    },
                                                    "order": [0, "desc"],
                                                    "iDisplayLength": 50
                                                });
                                            });
        </script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#apuntarme').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });
        </script>
    </body>
</html>
