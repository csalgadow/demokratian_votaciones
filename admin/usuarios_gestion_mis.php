<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

$lista1 = "";
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h1><?= _("BUSCAR  EN EL CENSO DE USUARIOS PARA VER QUE VOTACIONES TIENEN") ?> </h1>
                    <p>&nbsp;</p>
                    <!---->

                    <form id="form1" name="form1" method="post" action="usuarios_busq_mis.php" class="well form-horizontal">

                        <div class="form-group">
                            <label for="nombre_usuario" class="col-sm-3 control-label"><?= _("Nombre") ?> </label>

                            <div class="col-sm-9">
                                <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control" /></div></div>

                        <div class="form-group">
                            <label for="correo_electronico" class="col-sm-3 control-label"> <?= _("Correo electronico") ?></label>

                            <div class="col-sm-9">
                                <input name="correo_electronico" type="email" id="correo_electronico" class="form-control"/>

                            </div>
                        </div>


                        <div class="form-group">
                            <label for="nif" class="col-sm-3 control-label"> <?= _("Nif") ?></label>

                            <div class="col-sm-5">
                                <input name="nif" type="text" id="nif" class="form-control" />

                            </div>
                        </div>


                        <?php if ($es_municipal == false) { ?>
                            <div class="form-group">
                                <label for="provincia" class="col-sm-3 control-label"> <?= _("Provincia") ?></label>

                                <div class="col-sm-5">   <?php
                                    if ($_SESSION['nivel_usu'] == 2) {
                                        // listar para meter en una lista del cuestionario buscador


                                        $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];

                                            $lista1 .= "<option value=\"$id_pro\"  > $name1</option>";
                                        }
                                        ?>
                                        <h3> <?= _("Escoja una Provincia") ?> </h3>
                                        <select name="provincia" class="form-control" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else if ($_SESSION['nivel_usu'] == 3) {

                                        $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=" . $_SESSION['id_ccaa_usu'] . "  order by ID";
                                        $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                        while ($listrows = mysqli_fetch_array($resulta)) {
                                            $id_pro = $listrows['id'];
                                            $name1 = $listrows['provincia'];

                                            $lista1 .= "<option value=\"$id_pro\" > $name1</option>";
                                        }
                                        ?>
                                        <h3> <?= _("Escoja una Provincia") ?> </h3>
                                        <select name="provincia" class="form-control" id="provincia" >
                                            <?php echo "$lista1"; ?>
                                        </select>
                                        <?php
                                    } else {

                                        $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                        $quants2 = mysqli_num_rows($result2);

                                        if ($quants2 != 0) {

                                            while ($listrows2 = mysqli_fetch_array($result2)) {

                                                $name2 = $listrows2['id_provincia'];
                                                $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                                $row_prov = mysqli_fetch_row($optiones);

                                                $lista1 .= "    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"    id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                            }
                                            echo "$lista1";
                                        } else {
                                            echo _("No tiene asignadas provincias, no podra crear votación");
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="nombre_usuario" class="col-sm-3 control-label"> <?= _("Nivel administrativo") ?></label>

                            <div class="col-sm-9">
                                <input name="nivel_admin" type="radio" id="nivel_admin_0" value="vacio" checked="checked" />
                                <?= _("Todos") ?>
                                <br/>
                                <input type="radio" name="nivel_admin" value="2" id="nivel_admin_1" />
                                <?= _("Administrador") ?><br/>
                                <?php if ($es_municipal == false) { ?>
                                    <input type="radio" name="nivel_admin" value="3" id="nivel_admin_2" />
                                    <?= _("Administrador") ?> <?= _("CCAA") ?><br/>
                                    <input type="radio" name="nivel_admin" value="4" id="nivel_admin_3" />
                                    <?= _("Administrador") ?> <?= _("Provincia") ?><br/>
                                    <input type="radio" name="nivel_admin" value="5" id="nivel_admin_4" />
                                    <?= _("Administrador") ?> <?= _("Grupo Provincial") ?><br/>
                                <?php } ?>
                                <input type="radio" name="nivel_admin" value="6" id="nivel_admin_5" />
                                <?= _("Administrador") ?> <?= _("Grupo") ?> <?php
                                if ($es_municipal == false) {
                                    echo "Estatal";
                                }
                                ?>
                            </div></div>
                        <p>
                            <input type="submit" name="buscar" id="buscar" value="<?= _("Buscar") ?>"  class="btn btn-primary pull-right" />
                        </p>
                        <p>&nbsp; </p>
                        <p>
                            <input name="busqueda" type="hidden" id="busqueda" value="si">
                        </p>

                    </form>



                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>
