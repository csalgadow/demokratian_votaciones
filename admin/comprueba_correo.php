<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 1;
include('../inc_web/nivel_acceso.php');

//require_once('../modulos/PHPMailer/PHPMailerAutoload.php');
use PHPMailer\PHPMailer\PHPMailer;

require_once '../modulos/PHPMailer/src/PHPMailer.php';
require_once '../modulos/PHPMailer/src/SMTP.php';
require_once '../modulos/PHPMailer/src/Exception.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?= _("Ayuda") ?></title>
    </head>
    <body>

        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" >x</a>
                <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h4 class="modal-title"><?= _("Prueba de configuracón de correo") ?></h4>

            </div>            <!-- /modal-header -->
            <div class="modal-body">
                <p>
                    <?php
                    $name = _("prueba correo");
                    $mensaje = _("Es una prueba de configuracion de correo");


                    $mail = new PHPMailer();

//Enable SMTP debugging.
                    $mail->SMTPDebug = 4;
//Set PHPMailer to use SMTP.
                    if ($mail_IsHTML == true) {
                        $mail->IsHTML(true);
                    } else {
                        $mail->IsHTML(false);
                    }

                    if ($mail_sendmail == true) {
                        $mail->IsSendMail();
                    } else {
                        $mail->IsSMTP();
                    }
//$mail->SMTPAuth = true;
                    if ($mail_SMTPAuth == true) {
                        $mail->SMTPAuth = true;
                    } else {
                        $mail->SMTPAuth = false;
                    }

                    if ($mail_SMTPSecure == false) {
                        $mail->SMTPSecure = false;
                        $mail->SMTPAutoTLS = false;
                    } else if ($mail_SMTPSecure == "SSL") {
                        $mail->SMTPSecure = 'ssl';
                    } else {
                        $mail->SMTPSecure = 'tls';
                        //$mail->SMTPAutoTLS = false;
                    }


                    if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );
                    }

                    $mail->Port = $puerto_mail; // Puerto a utilizar

                    $mail->Host = $host_smtp;
                    $mail->SetFrom($email_env, $nombre_sistema);
                    $mail->MsgHTML($mensaje);
                    $mail->AddAddress($email_env, $name);
                    $mail->Username = $user_mail;
                    $mail->Password = $pass_mail;

                    $mail->Subject = _("prueba de configuracion");
                    $mail->Body = _("<i>Texto de correo en HTML</i>");
                    $mail->AltBody = "This is the plain text version of the email content";

                    if (!$mail->send()) {
                        echo "<div class=\"alert alert-warning\"> Mailer Error: " . $mail->ErrorInfo . "</div>";
                    } else {
                        echo "<div class=\"alert alert-success\">" . _("El correo ha sido enviado, compruebe su buzon de correo y si lo ha recibido la configuración es correcta") . " " . $email_env . "</div>";
                    }
                    ?>


                </p>



                <!--
            ===========================  fin texto ayuda
                -->             </div>            <!-- /modal-body -->
            <!-- /modal-footer -->
        </div>         <!-- /modal-content -->

    </body>
</html>
