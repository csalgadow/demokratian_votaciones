<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 2;
include('../inc_web/nivel_acceso.php');

$inmsg = "";
$mensaje2 = "";
$mensaje1 = "";
$lista1 = "";
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h1><?= _("INCLUIR CORREOS Y BLOQUEARLOS DE FORMA MASIVA") ?></h1>
                    <p><?= _("Estas direcciones de correo no podran votar aunque tengan un correo de la empresa u organización") ?> </p>
                    <?php
                    if ($inmsg != "") {
                        echo "<div class=\"alert alert-success\">" . _("¡¡¡Error!!!  No se han  registrado estos usuarios") . ".</div>";
                        echo "$inmsg";
                    }
                    ?>
                    <?php echo "$mensaje1"; ?>


                    <?php
                    if ($_POST['emails'] != '') {
                        if ($_POST['provincia'] != '00') {

                            $provincia = fn_filtro($con, $_POST['provincia']);
                            $optiones = "select  id_ccaa from $tbn8 where ID ='" . $provincia . "'";
                            $resultas = mysqli_query($con, $optiones) or die("error: " . mysqli_error($con));

                            while ($listrowes = mysqli_fetch_array($resultas)) {
                                $id_ccaa = $listrowes[id_ccaa];
                            }
                        }

                        $emails = strip_tags($_POST['emails'], '<\n><br/>');
                        $emailarray = explode("\n", $emails);

                        $numrows = count($emailarray);
                        for ($i = 0; $i < $numrows; $i++) {
                            $emailarray1 = explode(',', $emailarray[$i]);
                            $nif_val = fn_filtro($con, trim($emailarray1[2]));
                            $user_val = fn_filtro($con, trim($emailarray1[1]));
                            $municipio_val = fn_filtro($con, trim($emailarray1[3]));
                            //en el caso que sea circunscripcion unica metemos el dato de municipio 001
                            if ($es_municipal == true) {
                                $municipio_val = 001;
                            }
                            //miramos si llegan todos los datos necesarios
                            if (!isset($emailarray1[0])) {
                                $inmsg .= _("Falta correo") . " <br/>";
                            } elseif (!filter_var($emailarray1[0], FILTER_VALIDATE_EMAIL)) {
                                $error = "error";
                                $inmsg .= _("La direccion") . " " . $emailarray1[0] . " " . _("es erronea") . ". <br/>";
                            } else if ($_POST['provincia'] == '00') {
                                $mensaje2 = _("ERROR ¡¡escoja una provincia!!") . " <br/>";
                            } else if ($municipio_val == "") {
                                $error = "error";
                                $inmsg .= _("Indique un municipio para") . " " . $emailarray1[0] . " , " . $user_val . " , " . $nif_val . "<br/>";
                            } else {


/////miramos si el usaurio ya existe
                                $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn9 WHERE correo_usuario='" . trim($emailarray1[0]) . "' ") or die(mysqli_error($con));

                                $total_encontrados = mysqli_num_rows($usuarios_consulta);

                                mysqli_free_result($usuarios_consulta);



                                if ($total_encontrados != 0) {

                                    $inmsg .= _("El Usuario") . " " . trim($emailarray1[1]) . " " . _(" con correo") . " " . trim($emailarray1[0]) . "  " . _("y nif") . " " . trim($emailarray1[2]) . " " . _("ya está registrado") . ".<br/>";
                                } else {

                                    $tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);
                                    $provincia = fn_filtro($con, $_POST['provincia']);
                                    $razon_bloqueo = fn_filtro($con, $_POST['razon_bloqueo']);
                                    $bloqueo = "si";
                                    $tipo_usuario = 3;
                                    $query = "	insert into $tbn9  (ID,correo_usuario, nombre_usuario, nif, tipo_votante, id_provincia,id_ccaa,id_municipio,bloqueo,razon_bloqueo )	values	 ('', '" . trim($emailarray1[0]) . "', '" . $user_val . "', '" . $nif_val . "', '" . $tipo_usuario . "', '" . $provincia . "', '" . $id_ccaa . "', '" . $municipio_val . "', '" . $bloqueo . "', '" . $razon_bloqueo . "')";
                                    $mens = "error incluir datos de votantes";
                                    $result = db_query_id($con, $query, $mens);

                                    /* metemos un control para saber quien ha incluido este votante */
                                    $fecha = date("Y-m-d H:i:s");
                                    $accion = "1"; //uno , incluir nuevo votante
                                    $insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"$result\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
                                    $mens = "error incluir quien ha incluido el votantes";
                                    $resultado = db_query($con, $insql, $mens);



                                    //$result = @mysqli_query($con, $query) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
                                    if (!$result) {
                                        echo "<div class=\"alert alert-danger\">
                                        <strong> UPSSS!!!!<br/>" . _("esto es embarazoso, hay un error en la conexion con la base de datos") . " </strong> </div><strong>";
                                    }
                                    if ($result) {

                                        $mensaje1 .= _("Usuario") . ": " . trim($emailarray1[1]) . "  " . _("Correo") . ": " . trim($emailarray1[0]);
                                        if ($es_municipal == false) {
                                            $mensaje1 .= _("Idenitifcador municipio") . " : " . $municipio_val . "<br/>";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                    <?php if ($inmsg != "") { ?> <br/><div class="alert alert-warning"> <?php echo "$inmsg"; ?> </div> <?php } ?>
                    <?php echo "$mensaje2"; ?>
                    <?php if ($mensaje1 != "") { ?>
                        <br/>
                        <div class="alert alert-success"> <?= _("Estos usuarios han sido bloqueados") ?>:</div>
                        <div class="alert alert-success">
                            <?php echo "$mensaje1"; ?></div>
                    <?php } ?>

                    <?= _("Para añadir nuevos suscriptores a su lista escriba  la dirección de correo, el nombre, separadas por una coma (,) Use una linea para cada suscriptor , si desconoce el nombre ponga algo como, miembro, usuario..etc  ya que este dato no se puede quedar vacio. La direccion de correo es imprescindible") ?>.<br />
                    <br/>
                    <?= _("Ejemplo") ?>: <br/>
                    <ul>
                        carlos@yahoo.es, carlos<br/>
                        juan@yahoo.es, juan erez<br/>etc</ul>
                    <?= _("Recuerde elegir el tipo de usuario , añada primero los de un tipo y luego los del otro, no se pueden mezclar") ?>.

                    <form Action="<?php $_SERVER['PHP_SELF'] ?>" Method=POST class="well form-horizontal">
                        <?php if ($es_municipal == false) { ?>
                            <?php
                            if ($_SESSION['nivel_usu'] == 2) {
                                // listar para meter en una lista del cuestionario buscador


                                $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                    $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                }
                                ?>
                                <h2> <?= _("Escoja una Provincia") ?> </h2>

                                <div class="col-sm-4">  <select name="provincia" class="form-control"  id="provincia" ><?php echo "$lista1"; ?></select>
                                </div>
                                <?php
                            } else if ($_SESSION['nivel_usu'] == 3) {

                                $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=$ids_ccaa  order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows['id'];
                                    $name1 = $listrows['provincia'];
                                    $lista1 .= "<option value=\"$id_pro\"> $name1</option>";
                                }
                                ?>

                                <h2> <?= _("Escoja una Provincia") ?> </h2><div class="col-sm-9">
                                    <select name="provincia" class="form-control"  id="provincia" ><?php echo "$lista1"; ?></select>
                                </div>
                                <p>
                                    <?php
                                } else {

                                    $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                    $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                    if ($quants2 != 0) {

                                        while ($listrows2 = mysqli_fetch_array($result2)) {

                                            $name2 = $listrows2['id_provincia'];
                                            $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                            $row_prov = mysqli_fetch_row($optiones);
                                            $lista1 .= "<label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"  checked=\"checked\"  id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                        }
                                        echo "$lista1";
                                    } else {
                                        echo _("No tiene asignadas provincias, no podra crear votación");
                                    }
                                }
                                ?>
                            <?php } else { ?>
                                <input name="provincia" type="hidden" id="provincia" value="001">
                            <?php } ?>
                        </p>
                        <p>
                            <label for="razon_bloqueo"><?= _("Razon para bloquear estos correos") ?>:</label>
                            <textarea name="razon_bloqueo" cols="80" rows="3" id="razon_bloqueo" class="form-control" ></textarea>
                        </p>
                        <div class="col-sm-8"></div>
                        <label for="emails"></label>
                        <textarea name="emails" cols="80" rows="30" id="emails" class="form-control" ></textarea>
                        <p>&nbsp;</p>   <input type="submit" value="<?= _("Añadir votantes de la lista") ?>"  class="btn btn-primary pull-right" > <p>&nbsp;</p>

                    </form>





                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>
