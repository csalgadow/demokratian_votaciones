<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('../inc_web/seguri.php');
require ("../basicos_php/funcion_control_votacion.php");
ini_set('memory_limit', '3064M'); //cantidad de memoria
ini_set('max_execution_time', 900); //900 seconds = 15 minutes
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">

                <div class="col-md-2" >

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>
                <div class="col-md-7">



                    <!--Comiezo-->
                    <?php
                    if (ISSET($_POST["add_voto"])) {

                        $id_votante = $_SESSION['ID'];
                        $idvot = fn_filtro_numerico($con, $_POST['id_vot']);
                        $id_provincia = fn_filtro_numerico($con, $_POST['id_provincia']);
                        $id_ccaa = $_SESSION['id_ccaa_usu']; /// este esta mal??
                        $valores = fn_filtro($con, $_POST['valores']);
                        $id_ccaa = fn_filtro_numerico($con, $_POST['id_ccaa']);   ///o este esta mal??
                        $id_subzona = fn_filtro($con, $_POST['id_subzona']);
                        $id_grupo_trabajo = fn_filtro($con, $_POST['id_grupo_trabajo']);
                        $demarcacion = fn_filtro($con, $_POST['demarcacion']); //necesario
                        $clave_seg = fn_filtro($con, $_POST['clave_seg']);

                        reset($_POST);
                        foreach ($_POST as $k => $v)
                            $a[] = $v;
                        $datos = count($a) - 8;

                        if ($datos == 0) {
                            echo "<div class=\"alert alert-danger\">
   <strong>Hay un error, no ha seleccionado ninguna opción <a href=\"vota_encuesta.php?idvot=$idvot\">volver</a></strong></div>";
                        } else {



                            $sql3 = "SELECT  seguridad,nombre_votacion FROM $tbn1 WHERE ID ='$idvot' ";
                            $resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());

                            while ($listrows3 = mysqli_fetch_array($resulta3)) {
                                $seguridad = $listrows3[seguridad];
                                $nombre_votacion = $listrows3[nombre_votacion];
                            }



                            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                            } elseif (isset($_SERVER['HTTP_VIA'])) {
                                $ip = $_SERVER['HTTP_VIA'];
                            } elseif (isset($_SERVER['REMOTE_ADDR'])) {
                                $ip = $_SERVER['REMOTE_ADDR'];
                            }

                            $forma_votacion = 3;

//$error = FALSE;


                            if ($_POST['clave_seg'] == "") {
                                echo " <div class=\"alert alert-danger\">
   <strong>Falta la clave de seguridad <br> vuelva a realizar la votación</strong></div>";
                            } else {



                                /*                               list ($estado, $razon, $tipo_votante) = fn_mira_si_puede_votar($demarcacion, $_SESSION['ID'], $idvot, $id_ccaa, $id_provincia, $id_grupo_trabajo, $id_municipio);

                                  /////////////////////////// si podemos procesar el formulario

                                  if ($estado == "error") {
                                  if ($razon == "direccion_no_existe") {
                                  echo "<div class=\"alert alert-danger\">
                                  <strong>Esta direccion de correo no la tenemos registrada para esta provincia, quizas sea un error de nuestra base de datos , si consideras que tienes derecho a votar haz  enviarnos tus datos a traves de nuestro formulario</a></strong></div>";
                                  }
                                  if ($razon == "ya_ha_votado") {
                                  echo "<div class=\"alert alert-danger\">
                                  <strong>Ya ha votado en esta votación</strong></div>";
                                  }
                                  } else if ($estado == "TRUE" and $razon == "usuario_ok") { */
                                $codi = hash("sha512", $clave_seg);


                                $i = 0;

                                while ($i < $datos) {
                                    $val = fn_filtro($con, $a[$i]);

                                    $vot = 1;



                                    //$datos_votado.="Orden $voto  | Identificador candidato -->  $val |  Valor voto ---  $vot"."<br/>"; //cojemos el array de votos para enviar por correo si es necesario

                                    $datos_votado .= $val . "-" . $vot . ","; //cojemos el array de votos para enviar por correo si es necesario

                                    $i++;
                                }
                                for ($q = 1; $q <= 10000; $q ++) {
                                    //////	identofocador unico
                                    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                    $cad = "";
                                    for ($b = 0; $b < 4; $b++) {
                                        $cad .= substr($str, rand(0, 62), 1);
                                    }
                                    $time = microtime(true);
                                    $timecad = $time . $cad;
                                    $res_id = hash("sha256", $timecad);
                                    /// finidentificador unico

                                    $datos_del_voto = trim($datos_votado, ','); ///quitamos la ultima coma de la cadena para meterlo en la bbdd

                                    $fecha_env = date("Y-m-d H:i:s");
                                    $insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val) values (\"$res_id\",\"$vot\",\"$datos_del_voto\"," . $_SESSION['localidad'] . ",\"$idvot\",\"$codi\")";
                                    $mens = "mensaje añadido";
                                    $result = db_query($con, $insql, $mens);

                                    if (!$result) {
                                        echo "<div class=\"alert alert-danger\">
						   			<strong> UPSSS!!!!<br/>esto es embarazoso, hay un error y su votacion no ha sido registrada  </strong> </div><strong>";
                                    }
                                    if ($result) {

                                        // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
                                        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
                                        $cad = "";
                                        for ($i = 0; $i < 6; $i++) {
                                            $cad .= substr($str, rand(0, 62), 1);
                                        }
                                        $time = microtime(true);
                                        $timecad2 = $time . $cad;
                                        $user_id = hash("sha256", $timecad2);
                                        $fecha_env = date("Y-m-d H:i:s");
                                        $insql = "insert into $tbn2 (ID,id_provincia,id_votacion,id_votante,fecha,tipo_votante,ip,forma_votacion) values (\"$user_id\"," . $_SESSION['localidad'] . ",\"$idvot\"," . $_SESSION['ID'] . ",\" $fecha_env\",\" $tipo_votante\",\" $ip\",\" $forma_votacion\")";
                                        $mens = "<br/>¡¡¡ATENCION!!!!, el voto ha sido registrado , pero el usuario no ha sido bloqueado <br> el ID de usuario es:" . $_SESSION['ID'];
                                        $result = db_query($con, $insql, $mens);


                                        ////metemos el voto en un txt y lo registramos como medida de seguridad
                                        $Pollname = md5($idvot);
                                        $file = $FilePath . $Pollname . "_ballots.txt";
                                        $fh = fopen($file, 'a');
                                        fwrite($fh, $datos_votado . PHP_EOL) or die("Could not write file!");
                                        fclose($fh);  //  Cerramos el fichero

                                        $file2 = $FilePath . $Pollname . "_tally.txt";
                                        $fh2 = fopen($file2, 'r+');
                                        $búfer = fgets($fh2, 100);
                                        $partes = explode("|", $búfer);
                                        $partes1 = $partes[1] + 1;
                                        $partes = $partes[0] . "|" . $partes1;
                                        fseek($fh2, 0);
                                        fwrite($fh2, $partes) or die("Could not write file!");
                                        fclose($fh2);  //  Cerramos el fichero
                                        //// metemos el dato de que ha votado en la base de datos
                                        echo "Ha sido recogido el voto de: " . $_SESSION['nombre_usu'] . "<br/>";

                                        ///// metemos una copia encriptada del voto
                                        //// primero sacamos los datos de la tabla temporal para hacer una cadena quemeteremos tambien encriptada
                                        $sql = "SELECT 	ID, datos  FROM $tbn20 where id_votacion=\"$Pollname\" ORDER BY ID DESC";
                                        $mens = "Error en la consulta a la tabla temporal";
                                        $result = db_query($con, $sql, $mens);
                                        if ($row = mysqli_fetch_array($result)) {
                                            mysqli_field_seek($result, 0);
                                            do {
                                                $cadena .= $row[1] . "_";  ////generamos la caena que vamos a meter en la tabla de votos de seguridad
                                                $id_borrado = $row[0];
                                            } while ($row = mysqli_fetch_array($result));
                                        }


                                        $cadena = trim($cadena, '_'); // quitamos de la cadena el ultimo _
                                        //$id_borrado=$id_borrado-4; ///miramos el id del ultimo registro de la tabla temporal y descontamos 4 para su posterior borrado

                                        $borrado = "DELETE FROM $tbn20 WHERE ID=" . $id_borrado . " ";
                                        $mens = " error en el borrado de " . $id_borrado;
                                        $result = db_query($con, $borrado, $mens);

                                        $cadena_temp = $res_id . "+" . $datos_del_voto; //esta variable sirve para esta querry y para la querry de la tabla temporal siguiente
                                        /////// miramos si hay que encriptar la cadena de voto
                                        /* if ($encripta=="SHA"){
                                          $cadena_temp = hash('sha256', $cadena_temp);
                                          }else */
                                        if ($encripta == "si") {

                                            /* require_once "Crypt/AES.php"; */

                                            //Function for encrypting with RSA
                                            // hacemos una busqueda y cargamos la clave publica
                                            $result = mysqli_query($con, "SELECT clave_publica FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden");
                                            if ($row = mysqli_fetch_array($result)) {
                                                mysqli_field_seek($result, 0);
                                                do {
                                                    $clave_publica = $row[0];

                                                    $cadena = rsa_encrypt($cadena, $clave_publica);
                                                } while ($row = mysqli_fetch_array($result));
                                            }
                                        }
                                        //metemos los datos codificados en la bbdd
                                        $ident = md5($timecad2);

                                        $shadatovoto = hash('sha256', $cadena_temp);
                                        $insql1 = "insert into $tbn19 (ID,voto,cadena,id_votacion) values (\"$ident\",\"$shadatovoto\",\"$cadena\",\"$Pollname\")";
                                        $mens1 = "ERROR en el añadido voto encriptado ";
                                        $result = db_query($con, $insql1, $mens1);

                                        //metemos el dato en la tabla temporal
                                        $cadena_temp = $res_id . "+" . $datos_del_voto;
                                        $insql2 = "insert into $tbn20 (datos,fecha,id_votacion) values (\"$cadena_temp\",\"$time\",\"$Pollname\")";
                                        $mens2 = "ERROR en el añadido  del voto tabla temporal";
                                        $result = db_query($con, $insql2, $mens2);



                                        //////////////////////metemos la seguridad del envio de correos a interventores

                                        if ($seguridad == 3 or $seguridad == 4) {
                                            include('../basicos_php/envio_interventores.php');
                                        }
                                        ///////// fin envio a interventores
                                        ?><!--si todo va bien damos las gracisa por participar-->
                                        <div class="alert alert-success">
                                            <h3  align="center">Gracias por participar</h3>
                                            <strong>En  breve estaran los resultados </strong></div>
                                        <p> Guarde este codigo de su voto si quiere comprobar que es correctamente contabilizado</p>
                                        <div class='alert alert-success'>
                                            <p> <?php echo $res_id ?> </p>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                    $datos_votado = "";
                    ?>


                    <!--Final-->


                </div>

                <div class="col-md-3">

                    <?php // include("lateral_derecho.php");  ?>
                </div>

            </div>


            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>
