<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la ###
### Free Software Foundation, bien de la versi�n 3 de dicha Licencia o bien de cualquier versi�n posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General de GNU para m�s detalles.                                               ###
### Deber�a haber recibido una copia de la Licencia P�blica General junto con este programa. Si no ha sido as�, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### Tambi�n puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("conexion.php");
include_once ('../basicos_php/basico.php');
include_once ('../inc_web/version.php');


/* establecer el limitador de cache a 'private' */

session_cache_limiter('private');
/* establecer la caducidad de la cache a 30 minutos */
session_cache_expire(30);
session_name($usuarios_sesion);
session_start();


require_once("../basicos_php/lang.php");


if ($reCaptcha == true) {


    // Google reCAPTCHA API secret key
    $secretKey = $reCAPTCHA_secret_key;

    // Verify the reCAPTCHA response
    $verifyResponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

    // Decode json data
    $responseKeys = json_decode($verifyResponse, true);

    // should return JSON with success as true
    if ($responseKeys["success"]) {

        //Captcha correcto
        $validoCaptcha = true;
    } else {
        // eres un robot, asi que terminamos
        $validoCaptcha = false;
         Header("Location: $url_vot/index.php?error_login=10");
         exit;
    }
} else {
    // si no tenemos activado el sistema de captcha mandamos un true para  que se realice la autentificación
    $validoCaptcha = true;
}

if(isset($_SERVER['HTTP_REFERER'])) {
    $url = explode("?", $_SERVER['HTTP_REFERER']);
    $pag_referida = $url[0];
    $redir = $pag_referida; 
   }
else
{
   $redir = $url_vot; 
}

$federada = null;
$db_conexion = mysqli_connect("$host", "$hostu", "$hostp") or die(header("Location:  $url_vot/index.php?error_login=0"));
mysqli_set_charset($db_conexion, "utf8");
mysqli_select_db($db_conexion, "$dbn");


if ($validoCaptcha == true) {

    if (!$cfg_autenticacion_solo_local && isset($_REQUEST['federada']) && $_REQUEST['federada'] == "1") {

        $as = new SimpleSAML_Auth_Simple('default-sp');
        $as->requireAuth(); // esto hace exit si no esta autenticado

        $attributes = $as->getAttributes();

        $federada = true;
        $_POST['user'] = $attributes['usuario'][0];
        $_POST['pass'] = "";

        // creamos usuario si hemos elegido la opcion en configuracion
        if ($cfg_crear_usuarios_automaticamente) {

            $username = fn_filtro($db_conexion, $_POST['user']);
            $passw = "NOIMPORTAESMD5";
            $nivel_usuario = 1;
            $nivel_acceso = 10; //maximo nivel de administrador
            $tipo_votante = $cfg_tipo_usuario;

            $nif = fn_filtro($db_conexion, $_POST['user']);
            $nombre_usuario_new = fn_filtro($db_conexion, $_POST['user']);
            $email = $attributes['correo_usuario'][0];

            $usuario_consulta = mysqli_query($db_conexion, "SELECT usuario FROM $tbn9 WHERE usuario='" . $username . "'");

            // no existe lo creamos
            if (mysqli_num_rows($usuario_consulta) == 0) {
                $insql = "insert into $tbn9 (pass,usuario,correo_usuario,tipo_votante,nivel_acceso,nivel_usuario,nombre_usuario,apellido_usuario,nif) values (  \"$passw\",  \"$nombre_usuario_new\", \"$email\", \"$tipo_votante\", \"$nivel_acceso\", \"$nivel_usuario\", \"$username\",\"$nif\")";
                mysqli_query($con, $insql);
            }

            if (mysqli_error($con)) {
                session_destroy();
                Header("Location: $url_vot/index.php?error_login=8");
                exit;
            }
        }

        $federada = true;
    }


    if ($federada || (isset($_POST['user']) && isset($_POST['pass']))) {

        //evitamos el sql inyeccion
        $user1 = fn_filtro($db_conexion, $_POST['user']);
        $pass1 = fn_filtro($db_conexion, $_POST['pass']);

        $usuario_consulta = mysqli_query($db_conexion, "SELECT ID,usuario,pass,id_provincia,nombre_usuario,apellido_usuario ,tipo_votante,bloqueo,id_ccaa,nivel_usuario,nivel_acceso,imagen_pequena,fecha_control,id_municipio,razon_bloqueo   FROM $tbn9 WHERE usuario='" . $user1 . "'") or die(header("Location:  $redir?error_login=1"));

        if (mysqli_num_rows($usuario_consulta) != 0) {
            $login = stripslashes($user1);
            //$password = md5($pass1);


            $usuario_datos = mysqli_fetch_array($usuario_consulta);
            mysqli_free_result($usuario_consulta);

            mysqli_close($db_conexion);


            if ($login != $usuario_datos['usuario']) {
                Header("Location: $url_vot/index.php?error_login=4");
                exit;
            }

            // if (!$federada && $password != $usuario_datos['pass']) {
            if (!$federada && !password_verify($pass1, $usuario_datos['pass'])) {
                Header("Location: $url_vot/index.php?error_login=3");
                exit;
            }


            if ($usuario_datos['bloqueo'] == "si") {
                Header("Location: $url_vot/index.php?error_login=7&tx=" . $usuario_datos['razon_bloqueo'] . "");
                exit;
            }

            unset($login);
            unset($password);

           /* if ($civi == true) {
                 $datetime1 = $usuario_datos['fecha_control'];
                  $datetime2 = date("Y-m-d");
                  $interval = date_diff($datetime1, $datetime2);
                  $interval->format('%R%a d�as');
                
            }*/

            if ($federada) {
                $_SESSION['federada'] = true;
            }

            $_SESSION['ID'] = $usuario_datos['ID'];
            $_SESSION['tipo_votante'] = $usuario_datos['tipo_votante'];
            $_SESSION['usuario_login'] = $usuario_datos['usuario'];
            $_SESSION['usuario_password'] = $usuario_datos['pass'];
            $_SESSION['nombre_usu'] = $usuario_datos['nombre_usuario'];
            $_SESSION['apellido_usu'] = $usuario_datos['apellido_usuario'];
            $_SESSION['localidad'] = $usuario_datos['id_provincia'];
            $_SESSION['id_ccaa_usu'] = $usuario_datos['id_ccaa'];
            $_SESSION['id_municipio'] = $usuario_datos['id_municipio'];
            //$_SESSION['id_subgrupo_usu'] = $usuario_datos['id_subgrupo'];
            $_SESSION['nivel_usu'] = $usuario_datos['nivel_usuario'];  //tipo de usuario (administardor, votante, etc
            $_SESSION['usuario_nivel'] = $usuario_datos['nivel_acceso']; //nivel del usuario dentro del tipo
            $_SESSION['imagen'] = $usuario_datos['imagen_pequena']; /////imagen del usuario
            $_SESSION["ultimoAcceso"] = date("Y-n-j H:i:s");  // ponemos la fehca y hora actual para ponerle caducidad a la sesion

            $_SESSION['CKFinder_UserRole'] = 'admin'; // prueba a ver si asi funciona
            ///////////////// miramos el codigo de la provincia para meterlo en la sesion
            $options = "select DISTINCT ID,provincia from $tbn8  where ID = " . $usuario_datos['id_provincia'] . "";
            $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());
            while ($listrows = mysqli_fetch_array($resulta)) {
                $provin = $listrows['ID'];
                $_SESSION['provincia'] = $listrows['provincia'];
            }

            ///////////////////////miramos la comunidad autonoma para meterlo tambien en la sesion

            $options2 = "select DISTINCT ID,ccaa from $tbn3  where ID = " . $usuario_datos['id_ccaa'] . " ";
            $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
            while ($listrows2 = mysqli_fetch_array($resulta2)) {
                $_SESSION['ccaa'] = $listrows2['ccaa'];
            }

            ///////////////////////miramos el municipio para meterlo tambien en la sesion

            $options2 = "select DISTINCT nombre from $tbn18  where id_municipio = " . $usuario_datos['id_municipio'] . " ";
            $resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
            while ($listrows2 = mysqli_fetch_array($resulta2)) {
                $_SESSION['municipio'] = $listrows2['nombre'];
            }


            $fecha = date("Y-m-d H:i:s");
            $insql = "UPDATE $tbn9 SET fecha_ultima =\"$fecha\" WHERE id=" . $usuario_datos['ID'] . " ";
            $inres = @mysqli_query($con, $insql);


            Header("Location:" . $_SERVER['PHP_SELF'] . "?");
            exit;
        }
    }
} else {
    // Header("Location: $url_vot/index.php?error_login=10");
    // exit;
}

// comprueba que las tres principales variables de session existen , si falta alguna, destruimos la session y salimos 
if (!isset($_SESSION['usuario_login']) || !isset($_SESSION['usuario_password']) || !isset($_SESSION['usuario_nivel'])) {
    session_destroy();
    Header("Location: $url_vot/index.php?error_login=6");
    exit;
}
?>
