<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
//mensajes de error si intentas hacer algo ilegal
$error_login_ms[0] = _("No se puede conectar");
$error_login_ms[1] = _("No se puede acceder a la base de datos");
$error_login_ms[2] = _("Usuario o password no existen, por favor vuelva a intentarlo");
$error_login_ms[3] = _("Pasword incorrecto.");
$error_login_ms[4] = _("No es un Usuario registrado");
$error_login_ms[5] = _("No esta autorizado a realizar esa accion");
$error_login_ms[6] = _("No esta autorizado, por favor, registrese");
//$error_login_ms[7]="Su usuario esta temporalmente bloqueado ya que aparece como inscrito en el congreso por lo que va a votar presencialmente";
$error_login_ms[7] = _("Su usuario esta temporalmente bloqueado.");
$error_login_ms[8] = _("Su usuario no ha podido crearse automáticamente.");
$error_login_ms[9] = _("Hay un error con los datos que estaba intentando introducir.");
$error_login_ms[10] = _("Ummmm, parece que es un robot, o al menos asi lo detecta el sistema. ¿Ha marcado la casilla de verificación?");
$error_login_ms[11] =  "Lo sentimos, su sesión ha caducado. <br/> Ha pasasdo mas de ".$tiempo_session/60 ." minutos sin que realice ninguna acción" ;
?>

