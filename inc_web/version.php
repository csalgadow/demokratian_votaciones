<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                         DEMOKRATIAN versión 2.1.2                                                                       ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 2 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###
###############################################################################################################################################################
###############################################################################################################################################################
###################################################################################################################################
############################################## Nombres de las tablas en la BBDD####################################################


$DKversion = "3.1.0";  // verion de esta distribución

if (isset($_SESSION['usuario_nivel'])) {
    if ($_SESSION['usuario_nivel'] == "0") {

        if ($info_versiones == true) {

            function get_content($URL) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $URL);
                $data = curl_exec($ch);
                curl_close($ch);
                return $data;
            }

            if (isset($_SESSION['version'])) {
                $la_ultima_version = $_SESSION['version'];
            } else {
                $la_ultima_version = file_get_contents('https://demokratian.org/version.txt');
                $_SESSION['version'] = $la_ultima_version; // metemos la informacion en la sesion para no tener que hacer continuamente la llamada al doc de versiones
            }

            $ultima_version = explode("##", $la_ultima_version);

            if ($ultima_version[0] > $DKversion) {
                ?>

                <div class="container">
                    <?php
                    // Lista de prioridades que salen de $ultima_version[2]
                    // 1 -- Critica seguridad
                    // 2 -- muy Importante, correccion de bug
                    // 3 -- Importante
                    // 4 -- Mejora
                    ///hacemos una reserva para un desarrollo posterior
                    ?>
                    <div class="row">
                        <div class="col-lg-11 label label-warning">

                            <?php echo "$ultima_version[0]"; ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="<?php echo "$ultima_version[1]"; ?>" target="_blank"><?= _("descargar") ?></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="<?php echo "$ultima_version[3]"; ?>" target="_blank"><?= _("¿Que hay nuevo?") ?></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="../admin/constantes.php#avisos"> <?= _("deshabilitar avisos") ?></a>
                        </div>
                        <span class="col-lg-1 badge pull-right"><?php echo "$ultima_version[0]"; ?> </span>
                    </div>

                </div>
                <?php
            }
        }
    }
}
?>
