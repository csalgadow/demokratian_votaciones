<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                         DEMOKRATIAN versión 2.1.2                                                                       ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 2 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
###################################################################################################################################
############################################## Nombres de las tablas en la BBDD####################################################

$tbn1 = $extension . "votacion";             // nombre votacion y caracteristicas
$tbn2 = $extension . "usuario_x_votacion";  /////tabla que dice quien ha votado en una votacion especifica
$tbn3 = $extension . "ccaa";
$tbn4 = $extension . "grupo_trabajo";
$tbn5 = $extension . "usuario_admin_x_provincia";
$tbn6 = $extension . "usuario_x_g_trabajo";
$tbn7 = $extension . "candidatos ";
$tbn8 = $extension . "provincia";
$tbn9 = $extension . "votantes";
$tbn10 = $extension . "votos";  //
$tbn11 = $extension . "interventores";
$tbn12 = $extension . "debate_comentario";
$tbn13 = $extension . "debate_preguntas";
$tbn14 = $extension . "debate_votos";
$tbn15 = $extension . "elvoto";  //para el VUT  -- sutituye a $tbn10
$tbn16 = $extension . "paginas";
$tbn17 = $extension . "votantes_x_admin";
$tbn18 = $extension . "municipios";
$tbn19 = $extension . "votos_seg";
$tbn20 = $extension . "voto_temporal";
$tbn21 = $extension . "codificadores";
$tbn22 = $extension . "votacion_web";

$con = @mysqli_connect("$host", "$hostu", "$hostp") or die("no se puede conectar");
mysqli_set_charset($con, "utf8");
$db = @mysqli_select_db($con, "$dbn") or die("no se puede acceder a la tabla");
?>
