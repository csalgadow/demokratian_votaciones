<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
///// este scrip se usa en varias paginas

if ($var_carga == true) {
    $contar = 0;

    $result_vot = mysqli_query($con, "SELECT id_provincia, activa,nombre_votacion,tipo_votante, id_grupo_trabajo, demarcacion,id_ccaa,id_municipio 	 FROM $tbn1 where id=$idvot");
    $row_vot = mysqli_fetch_row($result_vot);

    $id_provincia = $row_vot[0];
    $id_ccaa = $row_vot[6];
    $activa = $row_vot[1];
    $tipo_votante = $row_vot[3];
    $id_grupo_trabajo = $row_vot[4];
    $id_municipio = $row_vot[7];
    if (isset($_GET['id_nprov'])) {
        $id_provincia = fn_filtro($con, $_GET['id_nprov']);
    } else {
        $id_provincia = "";
    }
    if ($row_vot[5] == 1) {


        $sql = "SELECT ID, 	nombre_usuario , nif,tipo_votante FROM $tbn9  WHERE id_provincia='$id_provincia' and  tipo_votante <='$tipo_votante' ";
    } else if ($row_vot[5] == 2) {
        $sql = "SELECT ID, 	nombre_usuario , nif,tipo_votante FROM $tbn9  WHERE id_ccaa='$id_ccaa' and tipo_votante <='$tipo_votante' ";
    } else if ($row_vot[5] == 3) {
        $sql = "SELECT ID, 	nombre_usuario , nif,tipo_votante FROM $tbn9  WHERE id_provincia='$id_provincia' and tipo_votante <='$tipo_votante' ";
    } else if ($row_vot[5] == 7) {
        $sql = "SELECT ID, 	nombre_usuario , nif,tipo_votante FROM $tbn9  WHERE id_municipio='$id_municipio' and tipo_votante <='$tipo_votante' ";
    } else {
        ////////////comprobar si funciona bien
        $sql = "SELECT a.ID, a.nombre_usuario , a.nif,a.tipo_votante FROM $tbn9 a,$tbn6 b  WHERE (a.ID= b.id_usuario) and id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' ";
    }


    $result = mysqli_query($con, $sql);
    ?>

    <h1><?= _("Votación de") ?> &quot;<?php echo "$row_vot[2]" ?>&quot;</h1>

    <h3>

        <?= _("Listado del censo") ?> 
        <?php if ($row_vot[5] == 1 and isset($_GET['id_nprov'])) { ?>
            <?= _("para la provincia") ?> <strong> <?php echo $_GET['id_nprov']; ?> </strong> <?= _("y tipo de votación") ?> 
        <?php } else if ($row_vot[5] == 2) { ?>
            <?= _("para la comunidad autonoma") ?> <strong> <?php echo $row_vot[6]; ?> </strong> <?= _("y tipo de votación") ?> 
        <?php } else if ($row_vot[5] == 3) { ?>
            <?= _("para la provincia") ?> <strong> <?php echo $row_vot[0]; ?> </strong> <?= _("y tipo de votación") ?> 
        <?php } else { ?>
            . <?= _("Tipo de votación") ?>  <?php } ?>

        <?php
        if ($row_vot[3] == 1) {
            echo"solo para socios";
        } else if ($row_vot[3] == 2) {
            echo"solo pata socios y simpatizantes";
        } else if ($row_vot[3] == 3) {
            echo " abierta";
        }
        ?> </h3> 



    <?php
    if ($row = mysqli_fetch_array($result)) {
        ?>

        <form name="form1" method="post" action="votantes_listado.php?$idvot=<?php echo "$idvot"; ?>"> 



            <br>
            <table id="tabla1<?php echo $_GET['cen']; ?>" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="5%"><?= _("ID") ?></th>
                        <th width="60%"><?= _("NOMBRE") ?></th>
                        <th width="30%"><?= _("DNI") ?></th>
                        <th width="5%"><?= _("TIPO") ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <tr>
                            <td><?php echo "$contar" ?></td>
                            <td><?php echo "$row[1]" ?></td>
                            <td><?php echo "$row[2]" ?> </td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo _("socios");
                                } else if ($row[3] == 2) {
                                    echo _("simpatizante verificado");
                                } else if ($row[3] == 3) {
                                    echo _("simpatizante");
                                } else if ($row[3] == 5) {
                                    echo _("Nada");
                                }
                                ?></td>
                        </tr>
                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>


                </tbody>
            </table>

        </form>


        <?php
    } else {
        if ($es_municipal == false) {
            if ($id_provincia == "") {
                echo "<div class=\"alert alert-success\">" . _("Escoja la provincia para la que quiere ver el censo") . "</div>";
            } else {
                echo " <div class=\"alert alert-succes\">" . _("¡No se ha encontrado votantes para esta encuesta!") . "</div ";
            }
        }
    }
} else {
    echo _("error acceso");
}
?>
			



