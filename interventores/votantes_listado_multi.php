<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('seguri_inter.php');
$idvot = fn_filtro_numerico($con, $_GET['idvot']);
if (isset($_GET['id_nprov'])) {
    $id_nprov = fn_filtro($con, $_GET['id_nprov']);
} else {
    $id_nprov = "";
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->


        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >

                    <?php include("menu.php"); ?>

                </div>



                <div class="col-md-8">

                    <!--Comiezo-->

                    <?php
                    $var_carga = true;
                    if ($_GET['cen'] == "com") {
                        include("votantes_listado_int.php");
                    } else if ($_GET['cen'] == "fal") {
                        include("votantes_listado_int_no.php");
                    } else if ($_GET['cen'] == "stn") {
                        include("votantes_listado_int_si.php");
                    } else if ($_GET['cen'] == "pres") {
                        include("votantes_listado_int_pres.php");
                    }
                    ?>


                    <!--Final-->
                </div>
                <div class="col-md-2" >

                    <!--Comiezo-->

                    <!--class="glyphicon glyphicon-user"-->

                    <div class="sidebar-nav">
                        <div class="navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <span class="visible-xs navbar-brand">+ <?= _("Censos y provincias") ?></span>
                            </div>

                            <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <?php if ($es_municipal == false) { ?>

                                        <li>
                                            <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&id_nprov=<?php echo $id_nprov; ?>&cen=com&lit=<?php echo $_GET['lit']; ?>" ><?= _("Censo completo") ?></a>
                                        </li>
                                        <li>
                                            <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&id_nprov=<?php echo $id_nprov; ?>&cen=fal&lit=<?php echo $_GET['lit']; ?>" ><?= _("Faltan por votar") ?></a>
                                        </li>
                                        <li>      <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&id_nprov=<?php echo $id_nprov; ?>&cen=stn&lit=<?php echo $_GET['lit']; ?>" ><?= _("Ya ha votado") ?></a>
                                        </li>

                                        <?php
                                        $var_carga = true;
                                        if ($_GET['lit'] == "si") {
                                            include("votantes_listado_provincias.php");
                                        } else if ($_GET['lit'] == "no") {
                                            
                                        }
                                    } else {
                                        ?>

                                        <li>
                                            <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&id_nprov=001&cen=com&lit=<?php echo $_GET['lit']; ?>" ><?= _("Censo completo") ?></a>
                                        </li>
                                        <li>
                                            <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&id_nprov=001&cen=fal&lit=<?php echo $_GET['lit']; ?>" ><?= _("Faltan por vota") ?>r</a>
                                        </li>
                                        <li>      <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&id_nprov=001&cen=stn&lit=<?php echo $_GET['lit']; ?>" ><?= _("Ya ha votado") ?></a>
                                        </li>
                                    <?php } ?>


                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </div>


                </div>

                <!--Final-->




            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php // include("../votacion/ayuda.php");  ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#tabla1').dataTable({
                    "language": {
                        "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                        "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                        "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                        "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                        "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                        "loadingRecords": "<?= _("Cargando") ?>...",
                                                        "processing": "<?= _("Procesando") ?>...",
                                                        "search": "<?= _("Buscar") ?>:",
                                                        "paginate": {
                                                            "first": "<?= _("Primero") ?>",
                                                            "last": "<?= _("Ultimo") ?>",
                                                            "next": "<?= _("Siguiente") ?>",
                                                            "previous": "<?= _("Anterior") ?>"
                                                        },
                                                        "aria": {
                                                            "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                            "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                        }
                                                    },
                                                    "order": [0, "desc"],
                                                    "iDisplayLength": 50
                                                });
                                            });
        </script>
    </body>
</html>
