<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");

//require_once('../modulos/PHPMailer/PHPMailerAutoload.php');
use PHPMailer\PHPMailer\PHPMailer;

require_once '../modulos/PHPMailer/src/PHPMailer.php';
require_once '../modulos/PHPMailer/src/SMTP.php';
require_once '../modulos/PHPMailer/src/Exception.php';
//include("../modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
include ('../basicos_php/basico.php');


if (empty($_POST['name']) ||
        empty($_POST['email']) ||
        empty($_POST['n_votacion']) ||
        !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    echo "Faltan datos";
    return false;
}

$name = fn_filtro_nodb($_POST['name']);
$email = fn_filtro($con, $_POST['email']);
$id_votacion = fn_filtro($con, $_POST['n_votacion']);

$conta = "SELECT id,pass  FROM $tbn11 WHERE correo ='" . $email . "' and id_votacion =" . $id_votacion . " ";


$result_cont = mysqli_query($con, $conta);
$quants = mysqli_num_rows($result_cont);

if ($quants == "") {

//echo "$provincia";
    echo "<div class=\"alert alert-warning\">
    <a href=\"#\" class=\"close\" data-dismiss=\"alert\">x</a>
    <strong>¡¡¡ERROR!!!</strong><br/>" . _("La direccion de correo") . " \" " . $email . " \" " . _(" no la tenemos registrada para la votación numero") . " \" " . $id_votacion . " \", " . _("quizas sea un error de nuestra base de datos , si consideras que tienes derecho acceder contacta con el administrador") . ".
</div>";
//echo "Esta direccion de correo no la tenemos registrada para esta provincia, quizas sea un error de nuestra base de datos , si consideras que tienes derecho a votar haz <a href=\"voto_contacto.php\"> click aqui para enviarnos tus datos a traves de nuestro formulario</a>";
} else {
    $row = mysqli_fetch_array($result_cont);
    $id_votante = $row[0];
    if ($row[1] != "") {
        $ya_clave = "ya_tiene_clave";
    }



////////////////////////////incluimos en la base de datos los datos del envio

    $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $cad = "";
    for ($i = 0; $i < 12; $i++) {
        $cad .= substr($str, rand(0, 62), 1);
    }

    $sSQL = "UPDATE $tbn11 SET codigo_rec=\"$cad\"  WHERE ID='$id_votante'";

    mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

///////////////////enviamos un correo
    function encrypt($string, $key) {
        $result = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result .= $char;
        }
        return str_replace(array('+', '/', '='), array('-', '_', ''), base64_encode($result));
    }

    $cadena_para_encriptar = $id_votacion . "_" . "$id_votante" . "_" . "$cad";
    $cadena_encriptada = encrypt($cadena_para_encriptar, $clave_encriptacion);

    $cadena_para_encriptar2 = "nwt-" . $id_votante;
    $cadena_encriptada2 = encrypt($cadena_para_encriptar2, $clave_encriptacion2);

    $mensaje = _("Hola ") . $name . " \r\n";

    $mensaje .= _("Este mensaje ha sido enviado por el sistema de votaciones") . " " . $nombre_web . " " . _("el") . " " . date('d/m/Y', time());
    if ($ya_clave == "ya_tiene_clave") {
        $mensaje .= "\n" .
                _("Te enviamos un enlace  para recuperar tu contraseña") . "\n";
    } else {
        $mensaje .= "\n" .
                _("Te enviamos un enlace  para finalizar tu registro") . " \n";
    }
    $mensaje .= _("Para completar la operacion es imprescindible acceder a la siguiente direccion web") . " \n
" . $url_vot . "/interventores/rec_contr.php?regedi=" . $cadena_encriptada2 . "&idpr=" . $cadena_encriptada . " \r\n" .
            _("Si  no ha solicitados la contraseña, alguien que conoce su direccion de correo lo ha hecho, pero puedes obviar este correo ya que tu contraseña no ha sido modificada");





    $mensaje = str_replace("\n", "<br>", $mensaje);
    $mensaje = str_replace("\t", "    ", $mensaje);

    if ($correo_smtp == true) {  //comienzo envio smtp
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->ContentType = 'text/plain';
        //$mail->IsHTML(false);
        if ($mail_IsHTML == true) {
            $mail->IsHTML(true);
        } else {
            $mail->IsHTML(false);
        }

        if ($mail_sendmail == true) {
            $mail->IsSendMail();
        } else {
            $mail->IsSMTP();
        }

        //$mail->SMTPAuth = true;
        if ($mail_SMTPAuth == true) {
            $mail->SMTPAuth = true;
        } else {
            $mail->SMTPAuth = false;
        }

        if ($mail_SMTPSecure == false) {
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
        } else if ($mail_SMTPSecure == "SSL") {
            $mail->SMTPSecure = 'ssl';
        } else {
            $mail->SMTPSecure = 'tls';
        }

        if ($mail_SMTPOptions == true) {  //algunos servidores con certificados incorrectos no envian los correos por SMTP por lo que quitamos la validadcion de los certificados, NO SE RECOMIENDA EN ABSOLUTO usar esta opción
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        }



        $mail->Port = $puerto_mail; // Puerto a utilizar

        $mail->Host = $host_smtp;
        $mail->SetFrom($email_env, $nombre_sistema);
        $mail->Subject = $asunto;
        $mail->MsgHTML($mensaje);
        $mail->AddAddress($email, $name);
        $mail->Username = $user_mail;
        $mail->Password = $pass_mail;


        if (!$mail->Send()) {
            echo " Error en el envio " . $mail->ErrorInfo;
        } else {
            // echo "Enviado correctamente!";

            echo "
<div class=\"alert alert-success\">
    <strong>" . _("Se ha enviado su código de activación a su dirección de correo para validar su cuenta y así poder participar en las votaciones") . ".</strong><br/> " .
            _("Si no recibe el correo compruebe su carpeta de spam por si esta allí") . " . <br/>" .
            _("Ahora tiene que mirar su correo y copiar ese código de activación para introducirlo en el sistema") . ".</div>";
        }
    }


    if ($correo_smtp == false) { ///correo mediante mail de php
        //para el envío en formato HTML
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//dirección del remitente
        $headers .= "From: $nombre_sistema <$email_env>\r\n";


//ruta del mensaje desde origen a destino
        $headers .= "Return-path: $email_env\r\n";

//$asunto="$asunto_mens_error";


        mail($email, $asunto, $mensaje, $headers);

        echo _("enviado correo por mail");
    }
}




return true;
?>
