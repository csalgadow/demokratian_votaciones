
<!--class="glyphicon glyphicon-user"-->

<div class="sidebar-nav">
    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="visible-xs navbar-brand">Menu</span>
        </div>

        <div class="navbar-collapse collapse sidebar-navbar-collapse">
            <ul class="nav navbar-nav">
                <li> <a href="votantes_listado_multi.php?idvot=<?php echo $_SESSION['numero_vot']; ?>&cen=com&lit=si"  class="list-group-item"><?= _("Censo de votantes") ?></a></li>
                <li> <a href="censo_urna.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Votantes en urna") ?></a></li>
                <?php if ($_SESSION['tipo'] == 1) { ?>
                    <li> <a href="voto_primarias.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Incluir votos") ?></a></li>
                    <li> <a href="datos_incluidos_primarias.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Ver los datos incluidos manualmente") ?></a></li>

                <?php } ?>
                <?php if ($_SESSION['tipo'] == 2) { ?>
                    <li> <a href="vut.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Incluir otro voto") ?></a></li>
                    <li> <a href="datos_incluidos_vut.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Ver los datos incluidos manualmente") ?></a></li>

                <?php } ?>
                <?php if ($_SESSION['tipo'] == 3) { ?>
                    <li> <a href="vota_encuesta.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Incluir otro voto") ?></a></li>
                    <li> <a href="datos_incluidos_primarias.php?idvot=<?php echo $idvot; ?>"  class="list-group-item"><?= _("Ver los datos incluidos manualmente") ?></a></li>

                <?php } ?>
                <li><a href="log_out.php" class="list-group-item"><?= _("Desconexión") ?></a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>




