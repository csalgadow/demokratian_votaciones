<div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
        <span class="glyphicon glyphicon-globe"></span>  Cambiar idioma
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
        <li><a href="?lang=en_EN">English</a></li>
        <li><a href="?lang=es_ES">Español</a></li>
        <li><a href="?lang=ca_ES">Català</a></li>
    </ul>
</div>