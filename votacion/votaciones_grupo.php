<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require ("../inc_web/verifica.php");
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');


$id_provincia = $_SESSION['localidad'];
$tipo_user = $_SESSION['tipo_votante'];
$idgr = fn_filtro($con, $_GET['idgr']);
$texto1_activo = "";
$texto2_activo = "";
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("../temas/codes/meta.php"); ?>
        <title><?php echo "$nombre_web"; ?></title>
        <meta name="author" content="Carlos Salgado">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png">



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>

        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
                    <div class="list-group">
                        <?php include("../votacion/menu_nav.php"); ?>

                    </div>
                </div>



                <div class="col-md-10">
                    <?php
                    $sql_grupo = mysqli_query($con, "SELECT subgrupo,texto FROM $tbn4 where ID=" . $idgr . " ");
                    $row_grupo = mysqli_fetch_row($sql_grupo);
                    ?>




                    <h3>   <?php echo $row_grupo[0]; ?> </h3>
                    <p>   <?php echo $row_grupo[1]; ?> </p>

                    <p><a data-toggle="modal"  href="../votacion/ver_grupo.php?idgr=<?php echo $idgr ?>" data-target="#apuntarme"  class="btn btn-info pull-right"><?= _("ver miembros de este grupo") ?></a></p>
                    <p>&nbsp;</p><p>&nbsp;</p>
                    <?php
/////hay que sacar datos del grupo
                    $esta_activa = "si";
                    $sql = "SELECT * FROM $tbn1 where id_grupo_trabajo =" . $idgr . " and tipo_votante >=" . $_SESSION['tipo_votante'] . " and activa like '$esta_activa' ORDER BY  ID  DESC ";
                    $result = mysqli_query($con, $sql);
                    if ($row = mysqli_fetch_array($result)) {
                        ?>

                        <div class="panel-group" id="accordion">

                            <?php
                            mysqli_field_seek($result, 0);
                            do {
                                ?>      <?php
                                $hoy = strtotime(date('Y-m-d'));
                                $fecha_ini = strtotime($row[13]);
                                $fecha_fin = strtotime($row[14]);
                                $fecha_ini_ver = date("d-m-Y", strtotime($row[13]));
                                $fecha_fin_ver = date("d-m-Y", strtotime($row[14]));
                                $hoy = strtotime(date('Y-m-d H:i'));
                                $hora_ini_ver = date("H:i", strtotime($row[13]));
                                $hora_fin_ver = date("H:i", strtotime($row[14]));


                                if ($row[2] == "no") {
                                    $activo = _("Votación NO activa");
                                } else if ($fecha_ini <= $hoy && $fecha_fin >= $hoy) {
                                    $id_votante = $_SESSION['ID'];
                                    $id_votacion = $row[0];
                                    $conta_vot = "SELECT id FROM $tbn2 WHERE id_votacion like \"$id_votacion\" and id_votante='$id_votante' ";

                                    $result_cont_vot = mysqli_query($con, $conta_vot);
                                    $quants_vot = mysqli_num_rows($result_cont_vot);

                                    if ($quants_vot != "") {
                                        $activo = _("Ya ha votado");
                                    } else {


                                        if ($row[6] == 1) {
                                            $dir = "../vota_orden/voto_primarias.php";
                                            $texto1_activo = _("Votación ABIERTA");
                                            $texto2_activo = _("Votación CERRADA");
                                            $image_activo = "comments.png";
                                        } else if ($row[6] == 2) {
                                            $dir = "../vota_vut/vut.php";
                                            $texto1_activo = _("Votación ABIERTA");
                                            $texto2_activo = _("Votación CERRADA");
                                            $image_activo = "comments.png";
                                        } else if ($row[6] == 3) {
                                            $dir = "../vota_encuesta/vota_encuesta.php";
                                            $texto1_activo = "DEBATE ABIERTO";
                                            $texto2_activo = _("Votación CERRADA");
                                            $image_activo = "comments.png";
                                        } else if ($row[6] == 4) {
                                            $dir = "../debate/debate.php";
                                            $texto1_activo = _("DEBATE ABIERTO");
                                            $texto2_activo = _("Votación CERRADA");
                                            $image_activo = "comments.png";
                                        }


                                        $activo1 = "$texto1_activo <img src=\"../imagenes/$image_activo\" width=\"20\" height=\"20\" alt=\"Votacion activa\" />";
                                        $activo = "<a href='$dir?idvot=$row[0]'  class=modify>$texto1_activo</a>";
                                    }
                                } else {
                                    if ($row[4] == 1) {
                                        $texto1_activo = _("Votación NO activa");
                                        $texto2_activo = _("Votación NO activa");
                                    } else if ($row[4] == 2) {
                                        $texto1_activo = _("Votación NO activa");
                                        $texto2_activo = _("Votación NO activa");
                                    } else if ($row[4] == 3) {
                                        $texto1_activo = _("Votación NO activa");
                                        $texto2_activo = _("Votación NO activa");
                                    } else if ($row[4] == 4) {
                                        $dir = "../debate/debate.php";
                                        $texto1_activo = _("DEBATE CERRADO") . "<img src=\"../imagenes/comments.png\" width=\"20\" height=\"20\" alt=\"Votación activa\" />";
                                        $texto2_activo = "<a href='$dir?idvot=$row[0]'  class=modify>" . _("DEBATE CERRADO") . "</a>";
                                    }

                                    $activo1 = $texto1_activo;
                                    $activo = $texto2_activo;
                                }
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo "$row[0]" ?>">
                                                <?php echo $row[3] ?>
                                            </a>
                                            <div class="derecha"> <?php echo "$activo"; ?></div>
                                        </h4>
                                    </div>
                                    <div id="<?php echo "$row[0]" ?>" class="panel-collapse collapse ">
                                        <div class="panel-body">

                                            Tipo de votacion :
                                            <?php
                                            if ($row[6] == 1) {
                                                echo _("primarias");
                                            } else if ($row[6] == 2) {
                                                echo _("VUT");
                                            } else if ($row[6] == 3) {
                                                echo _("encuesta");
                                            }
                                            ?>
                                            <div class="derecha">
                                                <?= _("Estado de la votación") ?>:
                                                <?php echo "$activo";
                                                ?></div>

                                            <?php
                                            if ($row[15] == "si") {
                                                if ($row[6] == 1) {
                                                    $dir_a_1 = "../vota_orden/resultados_primarias.php";
                                                } else if ($row[6] == 2) {
                                                    $dir_a_1 = "../vota_vut/dcresults.php";
                                                } else if ($row[6] == 3) {
                                                    $dir_a_1 = "../vota_encuesta/resultados_encuesta.php";
                                                }


                                                $activo_a_1 = "<li><a href='$dir_a_1?idvot=$row[0]'  class=modify>" . _("Resultados") . "</a></li>";
                                            } else {
                                                $activo_a_1 = "&nbsp;";
                                            }
                                            echo "$activo_a_1";
                                            ?>

                                            <br/>
                                            <?= _("Comienzo de la votación :Desde las") ?> <?php echo $hora_ini_ver; ?> <?= _("el dia") ?> <?php echo $fecha_ini_ver; ?>
                                            <br/><?= _("Final de la votación :Hasta las") ?> <?php echo $hora_fin_ver; ?> <?= _("del dia") ?> <?php echo $fecha_fin_ver; ?>
                                            <br/>
                                            <?php echo $row[5] ?>


                                        </div>
                                    </div>
                                </div>


                                <?php
                            } while ($row = mysqli_fetch_array($result));
                            ?>
                        </div>
                        <?php
                    } else {
                        echo _("¡No se ha encontrado ninguna votación!");
                    }
                    ?>


                    <p>&nbsp;</p>
                    <p>&nbsp;</p>

                </div>

            </div>
            <!--
       ===========================  modal para ver quien esta en el grupo
            -->
            <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-body"></div>

                    </div> <!-- /.modal-content -->
                </div> <!-- /.modal-dialog -->
            </div> <!-- /.modal -->

            <!--
           ===========================  FIN modal para ver quien esta en el grupo
            -->

            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>

        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#tabla1').dataTable({
                    "language": {
                        "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                        "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                        "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                        "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                        "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                        "loadingRecords": "<?= _("Cargando") ?>...",
                                                        "processing": "<?= _("Procesando") ?>...",
                                                        "search": "<?= _("Buscar") ?>:",
                                                        "paginate": {
                                                            "first": "<?= _("Primero") ?>",
                                                            "last": "<?= _("Ultimo") ?>",
                                                            "next": "<?= _("Siguiente") ?>",
                                                            "previous": "<?= _("Anterior") ?>"
                                                        },
                                                        "aria": {
                                                            "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                            "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                        }
                                                    },
                                                    "order": [0, "desc"],
                                                    "iDisplayLength": 50
                                                });
                                            });
        </script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#apuntarme').on('hidden.bs.modal', function () {
                $(this).removeData('bs.modal');
            });
        </script>
    </body>
</html>
