<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Fecha en el pasado
require ("../inc_web/verifica.php");
$nivel_acceso = 11;
include('../inc_web/nivel_acceso.php');

if (empty($_GET['idgr'])) {
    echo _("Por favor no altere el fuente");
    exit;
}



$idgr = fn_filtro_numerico($con, $_GET['idgr']);

$sql = "SELECT a.ID,  a.nombre_usuario,  a.correo_usuario,  a.tipo_votante, a.usuario,a.apellido_usuario, a.imagen_pequena, a.perfil FROM $tbn9 a,$tbn6 b where (a.ID= b.id_usuario) and b.id_grupo_trabajo=" . $idgr . " and  b.estado = 1   order by a.nombre_usuario ";

$result = mysqli_query($con, $sql);
?>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
<link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
<link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title"><?= _("Miembros de este grupo") ?></h4>
    </div>

    <div class="modal-body">


        <?php
        if ($row = mysqli_fetch_array($result)) {
            ?>

            <table id="tabla1" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="1%">&nbsp;</th>
                        <th width="20%"><?= _("Nombre") ?></th>
                        <th width="20%"><?= _("Nick") ?></th>
                        <th width="58%">&nbsp;</th>
                        <th width="1%">&nbsp;</th>

                    </tr>
                </thead>

                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);

                    do {
                        ?>
                        <tr>
                            <td><?php if ($row[6] == "peq_usuario.jpg" or $row[6] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/imagenes/avatar_sin_imagen.jpg" width="70" height="70" /><?php } else { ?><img src="<?php echo $upload_user; ?>/<?php echo"$row[6]"; ?>" alt="<?php echo"$row[1]"; ?> <?php echo"$row[4]"; ?>" width="70" height="70"  /> <?php } ?></td>
                            <td><?php echo "$row[1]" ?> <?php echo "$row[5]" ?></td>
                            <td><?php echo "$row[4]" ?></td>
                            <td><?php echo "$row[7]" ?> </td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo _("Afiliado");
                                } else if ($row[3] == 2) {
                                    echo _("Simpatizante Verificado");
                                } else if ($row[3] == 3) {
                                    echo _("Simpatizante");
                                }
                                ?></td>
                        </tr>

                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>


                </tbody>
            </table>

            <?php
        } else {

            echo _("¡No se ha encontrado ningún votante!");
        }
        ?><!---->	



    </div>
    <div class="modal-footer"></div>
</div><!-- /.modal-content -->

<script src="../js/jquery-1.9.0.min.js"></script>
<script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" class="init">

    $(document).ready(function () {
        $('#tabla1').dataTable({
            "language": {
                "lengthMenu": "<?= _("Ver") ?> _MENU_  <?= _("resultados por pagina") ?>",
                                "zeroRecords": "<?= _("No se han encontrado resultados - perdone") ?>",
                                "info": "<?= _("Mostrando") ?> _PAGE_ <?= _("de _PAGES_ paginas") ?>",
                                                "infoEmpty": "<?= _("No se han encitrado resultados") ?>",
                                                "infoFiltered": "(<?= _("filtered from") ?> _MAX_ <?= _("total records") ?>)",
                                                "loadingRecords": "<?= _("Cargando") ?>...",
                                                "processing": "<?= _("Procesando") ?>...",
                                                "search": "<?= _("Buscar") ?>:",
                                                "paginate": {
                                                    "first": "<?= _("Primero") ?>",
                                                    "last": "<?= _("Ultimo") ?>",
                                                    "next": "<?= _("Siguiente") ?>",
                                                    "previous": "<?= _("Anterior") ?>"
                                                },
                                                "aria": {
                                                    "sortAscending": ": <?= _("activate to sort column ascending") ?>",
                                                    "sortDescending": ": <?= _("activate to sort column descending") ?>"
                                                }
                                            },
                                            "order": [0, "desc"],
                                            "iDisplayLength": 50
                                        });
                                    });
</script>
<script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
    $('#apuntarme').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
    });
</script>